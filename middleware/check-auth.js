const jwt = require('jsonwebtoken');
const keys = require('../config/keys');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const cert = keys.JWT_KEY;

       jwt.verify(token, cert, function(err, decoded) {
         if(err){
           return res.status(401).json({
            message: 'Auth failed'
         });  //required fields error
        } else {
 
        req.userData = decoded;
        next();
         }
      });
      } catch (error)
    {
      console.log(error);

        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};
