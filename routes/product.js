var express = require('express');
var router = express.Router();
var Product=require('../models/product');
const checkAuth = require('../middleware/check-auth');
const multer = require('multer');
const path = require('path');
var Unzipper = require("decompress-zip");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/var/www/html/backendAPI/tmp')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

var storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
  cb(null, '/var/www/html/backendAPI/tmp')
 },

filename: function (req, file, cb) {
      let extArray = file.mimetype.split("/");
      let extension = extArray[extArray.length - 1];
      let random_number = req.body.random_number;
      cb(null, 'Project_controller'+ '_' + random_number+ '_upload.zip')
 },

onFileUploadStart: function(file, req, res){
  if (req.file.size > 50000000) {
     res.status(201).json({ "status": "file size exceed from 50MB","upload_url": "1"});  
     return false;
    }
  }
})

var upload = multer({ storage: storage })
var upload1 = multer({ storage: storage1 })

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


//router.get("/getLeadinfo",Lead.contacts_get_all);
router.get("/pro_category", Product.getcategory);  // Get product Category list
router.get("/prod_category", Product.getprocategory);  // Get product Category list
router.get("/products", Product.getProducts); // get product list based on the category id and subcategory id
router.get("/getProduct",Product.getProduct);
router.get("/pro_slider", Product.getCategorySlider); // get product list based on the category id and subcategory id
router.post("/checkAvaiblecomputer", Product.checkAvaiblecomputer); 
router.get("/checkGroup", Product.checkGroup); 
router.get("/taglist", Product.taglist); 
router.post("/updateLifetime", Product.updateLifetime); 



router.get("/related_product", Product.relatedProduct); // get product list based on the category id and subcategory id
router.post("/file_upload",upload.single('file'), Product.project_upload_image); 
router.post("/file_audio",upload.single('file'), Product.project_upload_audio); 
router.post("/project_image_crop", Product.project_image_crop); 
router.post("/project_upload_zip",upload1.single('file'), Product.project_upload_zip);
router.post("/finalApproved", Product.finalApproved); 
router.post("/updaterendertime", Product.updaterendertime); 
router.get("/fonts", Product.fontFamily); // get product list based on the category id and subcategory id
router.post("/updateProject", Product.update_project); 
router.post("/addcat", Product.addsub); 
router.post("/getleadProducts", Product.getleadProducts); 
router.post("/getuserProducts", Product.getuserProducts); 
router.post("/addWorker", Product.addWorker);
router.get("/getWorker", Product.getWorker);
router.get("/getWorkerDetails", Product.getWorkerDetails);
router.post("/addCategory", Product.addCategory);
router.post("/addTags", Product.addTags); 
router.get("/getsubadmincategory", Product.getsubadmincategory); 
router.get("/getProductTags", Product.getProductTags); 
router.post("/addsubCategorys", Product.addsubCategorys); 


router.post("/addgroupproducts", Product.addgroupproducts); 
router.post("/deletegroupproducts", Product.deletegroupproducts); 
router.get("/getGroupproducts", Product.getGroupproducts); 
router.get("/getGroupproductsslider", Product.getGroupproductsslider); 



router.post("/addsliderproducts", Product.addsliderproducts); 
router.post("/deletesliderproducts", Product.deletesliderproducts); 
router.get("/getsliderproducts", Product.getsliderproducts); 
router.get("/getproductsslider", Product.getproductsslider); 




router.post("/add_upload_project", Product.add_upload_project); 
router.get("/getGroupproductsdetails", Product.getGroupproductsdetails); 
router.get("/groupproductspriority", Product.groupproductspriority); 
router.get("/getCompproducts", Product.getCompproducts); 
router.get("/getRenderItem", Product.getRenderItem); 
router.get("/getRenderItemInfo", Product.getRenderItemInfo); 
router.get("/getUploadInfo", Product.uploadRenderItem); 
router.get("/checkRenderStatus", Product.checkRenderStatus); 
router.post("/deleteBuffer", Product.deleteBuffer); 
router.post("/bulkDelete", Product.bulkDelete); 
router.get("/users", Product.users_details); 
router.post("/addProducer", Product.addProducer);
router.get("/getProducer", Product.getProducer);
router.get("/getProducerdetail", Product.getProducerdetails);
router.post("/getProducertrash", Product.getProducertrash);
router.post("/finalProname", Product.finalProname);
router.post("/finalPrice", Product.finalPrice);
router.post("/bulkapproval", Product.bulkapproval); 
router.post("/disapprove", Product.disapprove); 


module.exports = router;


