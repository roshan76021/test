var express = require('express');
var router = express.Router();
var Product=require('../models/postajob');
const checkAuth = require('../middleware/check-auth');
const multer = require('multer');
const path = require('path');
var Unzipper = require("decompress-zip");
var _ = require('underscore');
const keys = require('../config/keys');
const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const s3= new AWS.S3({
                      accessKeyId:keys.AWSaccessKeyId,
                      secretAccessKey:keys.AWSsecretAccessKey
                      });
var db = require('../mdbConnection')
var multerS3 = require('multer-s3')
var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'contributors',
     acl: 'public-read',
   
    key: function (req, file, cb) {
        var freelancer_name = req.query.freelancer;
        var freelancer_name = req.query.freelancer;
        var file_name = req.query.fileName;
        var nooffile = req.query.nooffile;
        var now = req.query.now;

     db.get().collection("freelancer_projects").find().toArray( function(err, result_item_email) {
     if (err) throw err;
     var files_size = 1;
      if(_.size(result_item_email)> 1)
      {
      files_size = result_item_email.length;
      }
      var tempFile = files_size.toString() +'_'+now+'_'+freelancer_name+'_'+file_name;
      var filename = freelancer_name+'/' +tempFile;
     cb(null, filename)
     })
    }
  })
})


var uploadImage = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'contributors',
     acl: 'public-read',
   
    key: function (req, file, cb) {
        var freelancer_name = req.body.freelancer;
        var file_name = req.body.fileName;
           var now = req.body.now;
           var tempFile = freelancer_name+'_'+file_name;
           var filename = freelancer_name+'/' +tempFile;
           cb(null, filename)
   
    }
  })
})





function unixTime(unixtime) {
var theDate = new Date(unixtime);
dateString = theDate.toGMTString();

      return dateString
    };





/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


   router.post("/signup", Product.signup); 
   router.get("/checkEmail", Product.checkEmail); 
   router.get("/checkUsername", Product.checkUsername); 
   router.post("/login", Product.login);
   router.post("/getbio",Product.getbio);
   router.post("/addbio",Product.addbio);
   router.post("/changepassword",Product.changepassword);
   router.post("/getuserInfo",Product.getuserInfo);
   router.post("/adduserInfo",Product.adduserInfo);
   router.post("/getpaymentInfo",Product.getpaymentInfo);
   router.post("/addpaymentInfo",Product.addpaymentInfo);
   router.post("/resetPassword",Product.resetPassword);

   router.post("/uploadProject",upload.single('file'),function(req, res, next){
      var freelancer_name = req.query.freelancer;
      var file_name = req.query.fileName;
      var nooffile = req.query.nooffile;
      var now = req.query.now;

     db.get().collection("freelancer_projects").find().toArray( function(err, result_item_email) {
     if (err) throw err;
     var files_size = 1;
      if(_.size(result_item_email)>1)
      {
      files_size = result_item_email.length;
      }
         var tempFile = files_size.toString() +'_'+now+'_'+freelancer_name+'_'+file_name;
      var filename = freelancer_name+'/' +tempFile;
      res.status(201).json({ "status": "Upload sucessfully","upload_status": "1","file_url":"https://contributors.s3.amazonaws.com/"+ filename}); 
   })
 })


  router.post("/uploadProfileImage",uploadImage.single('file'),function(req, res, next){
      var freelancer_name = req.body.freelancer;
      var file_name = req.body.fileName;
      var now = req.body.now;
      var tempFile = freelancer_name+'_'+file_name;
      var filename = freelancer_name+'/' +tempFile;
      res.status(201).json({ "status": "Upload sucessfully","upload_status": "1","file_url":"https://contributors.s3.amazonaws.com/"+ filename}); 

 })

/* Freelancer Project User Panel*/
  router.post("/addfreelancerProject",Product.addfreelancerProject);
  router.post("/getProectsstatus",Product.getProectsstatus);
  router.post("/updateprojectsStatus",Product.updateproectsStatus);
  router.get("/getProjectsstatus",Product.getProjectsstatus);

  
 /* Freelancer Project User Panel*/


 /* Freelancer Admin Panel */
 router.get("/getFreelanceruser",Product.getFreelanceruser);
 router.get("/getFreelancerpstatus",Product.getFreelancerpstatus);
 router.post("/getfreeLancerProject",Product.getfreeLancerProject);
 
 



 /* Freelancer Admin Panel End */
module.exports = router;
