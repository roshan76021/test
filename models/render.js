const { createClient } = require('@nexrender/api');
const fs = require('fs');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const exec = require('child_process').exec;
const download = require('download');
const keys = require('../config/keys');
var _ = require('underscore');
var request = require('request');
const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const s3= new AWS.S3({
    accessKeyId:keys.AWSaccessKeyId,
    secretAccessKey:keys.AWSsecretAccessKey
});
var db = require('../mdbConnection')
var ObjectId = require('mongodb').ObjectID;

   +function generateUniqueString() {
    var ts = String(new Date().getTime()),
        i = 0,
        out = '';
    for (i = 0; i < ts.length; i += 2) {
        out += Number(ts.substr(i, 2)).toString(36);
     }
     return ('prefix' + out);
     }


    function convertHex(hex){
             hex = hex.replace('#','');
              r = parseInt(hex.substring(0,2), 16);
              g = parseInt(hex.substring(2,4), 16);
              b = parseInt(hex.substring(4,6), 16);
         result = [r/255,g/255,b/255];
         return result;
      }

    function sleep(ms) {
          return new Promise((resolve) => {
          setTimeout(resolve, ms);
          });
        }   

function replaceAll(string, search, replace) {
  return string.split(search).join(replace);
}
    function splitToNumbers(str) {
            str = '0' + str;
            var timeArray = str.match(str.length  % 2 ? /^\d|\d{2}/g : /\d{2}/g).map(Number)
            var hourFrame =  timeArray[0] * 108000;
            var minutesFrame =  timeArray[1] * 1800;
            var secFrame =  timeArray[2] * 30;
            var frame = timeArray[3];
            var totalFrame = hourFrame + minutesFrame + secFrame + frame;
            return totalFrame
        } 




    /* test render for admin panel */
     exports.test_render_preview = (req,res,next) => {
      req.setTimeout(18000000);
          let count = 0;
          let scene = req.body.scene;
          var server_id,port;

          scene.forEach(function(element,i) {
           var file_type = element.fileName;
           var random_scene = element.random_scene;
           var prod_name = element.prod_name;
           var image_frame = splitToNumbers(element.imageFrame);
           var file_duration = splitToNumbers(element.fileDuration);
           var file_str = file_type.split(".");
           var renderModule = element.original.renderSettings;
           var outputModule = element.original.outputModule;
           var switch_on = "1";
           var optionsNodename = element.original.nodeName;
           var optionsOutputext = element.original.outputExt;
           var compositions = element.compositions;
           var render_time = element.render_time;
           var assets_url = keys.utility_url +'/' + file_str[0];
           var texts = element.text;
           var image = element.images;
           var color = element.color_picker;
           var product_id =element.product_id;
           var audio_file = element.audio.url;
           var category = element.category;
           var random_number = (new Date().getTime()).toString(36);
           var asset = [];
           texts.forEach(function(elements) {
             var text_contents = elements.contents;
            text_contents = replaceAll(text_contents,"^", "\\n");
            var text_nodeName = elements.nodeName;
             var text_color = elements.color;
             var text_fontfamily = elements.font;
             var assets_text_content_json = { "type": "data", "layerName":"Text_01","property": "Source Text","expression": "'"+text_contents+"'","composition":text_nodeName+"_composition"};
             asset.push(assets_text_content_json);
             var assets_text_color_json =  {"type": "data","layerName": "Text_01","property":"Effects.Color.Color","value": convertHex(text_color),"composition":text_nodeName+"_composition"};
             asset.push(assets_text_color_json);
             var assets_text_fontFamily_Json =   {
              "type": "data",
              "layerName": "Text_01",
              "property": "Source Text.font",
              "value": text_fontfamily,
              "composition":text_nodeName+"_composition"
              };
            asset.push(assets_text_fontFamily_Json);
             });

 
           image.forEach(function(elementss,l) {
            var image_url = elementss.url;
            var image_compositionNames;
            var image_nodeName = elementss.nodeName;
            var image_compositionName = elementss.name;
            var image_composition = elementss.image_composition;
            if(image_compositionName === 'Logo 1'){
              image_compositionNames = image_compositionName+"_composition_"+image_composition;
            } else {
              image_compositionNames = image_compositionName+"_composition_1920*1080";
           }
             var assets_image_json = {"src": image_url,"type": 'image',"layerName" : image_nodeName,"composition": image_compositionNames};
             asset.push(assets_image_json);
           });

           color.forEach(function(elementcolors) {
            var color_picker_color = elementcolors.color;
            var color_picker_nodeName = elementcolors.nodename;
            var color_picker_json =  {"type": "data","layerName": "color_picker","property": "Effects.Color_"+color_picker_nodeName+".Color", "value": convertHex(color_picker_color),"composition": "Project_controller"};
            asset.push(color_picker_json);
            });
            
           var assets_options = {"type": "data","layerName": optionsNodename,"property": "Effects.on/off.ADBE Checkbox Control-0001","value": switch_on,"composition": "Watermark"};
           asset.push(assets_options);



           var assets_opocity = {"type": "data","layerName": "Watermark","property": "Opacity","value": [20],"composition":compositions};
          
           asset.push(assets_opocity);




           var watermark_url = keys.data_url+"/Watermarks/studiolancers/Watermark.png";
           var assets_watermark_json = {"src": watermark_url,"type": 'image',"layerName" : "Watermark.png","composition": "Watermark"};
           asset.push(assets_watermark_json);
          
          
          var fileUtl = assets_url + '/' + file_str[1] +'.aep';

          if(category === "5f3127b59df15d658b9cfcff"){
           var assets_audio_json = {"src": audio_file, "type": 'audio',"layerName" : "Music.mp3","composition": "Music"};
           asset.push(assets_audio_json);
           fileUtl= assets_url + '/' + file_str[1] +'.aep';

           } 

           
            var now = Date.now();

            var render_server_state_collection = db.get().collection("render_server_state");
            var port_filter = {};

            var query = {
              "port": {
                  "$ne": 6041
              },
              "render_type": "Project Uploader",
              "status": "Online",
              };
            // var sort = [ ["priority", 1] ];
    
            render_server_state_collection.find(query).toArray(function(err,result_port_select) {
            if (err) throw err;
            if(_.size(result_port_select) > 0){
              if(result_port_select[0]['available_slots'] > 0 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var avaiable_slots = Math.abs(result_port_select[0]['available_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {available_slots: parseInt(avaiable_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                 } else if(result_port_select[0]['available_slots'] < 1 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var queue_slots = Math.abs(result_port_select[0]['queue_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {queue_slots: parseInt(queue_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                } else {
                 port = '6041';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }
                 } else {
                 port = '6041';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }
             var dynamic_render_client = createClient({host: keys.render_url+port,secret:"sdsecret"});
             var final_aftereffect = {"final_video":"render_"+random_number+".mp4","image_frame":image_frame,"random_number":random_number,"product_id":product_id,"compositions":compositions,"now":now,"audio_file":audio_file,"file_duration":file_duration,"port":port,"server_id":server_id,"random_scene":random_scene}
             const main =  async () => {
             const result = await dynamic_render_client.addJob({
             template: {
                  src: fileUtl,
                  composition: compositions,
                  frameStart: 1,
                  frameEnd:file_duration,
                  frameIncrement: 1,
                  continueOnMissing: false,
                  settingsTemplate: renderModule,
                  outputModule: outputModule,
                  outputExt: "mp4",
                  multiFrames:true,
                  addLicense:false,
                  reuse:true,
                  
                  },
                  assets: asset,
                  actions: {
                   prerender: [],
                   postrender: [
                   {
                     "module": "@nexrender/action-upload",
                     "input": "result.mp4",
                     "provider": "s3",
                     "params": {
                     "region": "us-east-1",
                     "bucket": "projectsfiless",
                     "key": "uploaddata/"+random_scene+"/test/render_"+random_number+".mp4",
                     "acl": "public-read"
                     }
                    }
                   ],
                   }
                   })
                     result.on('created', job => module.exports.updateCreatedState(product_id,prod_name,port,server_id,compositions,final_aftereffect,random_number,random_scene,"render_"+random_number+".mp4",res))
                     result.on('queued', job =>  module.exports.updateQueueState(product_id,port,server_id,compositions))
                     result.on('picked', job => module.exports.updatePickedState(product_id,port,server_id,compositions))
                     result.on('started', job => module.exports.render_test_preview_started(product_id,port,server_id,compositions))
                     result.on('progress', (job, percents) => module.exports.updatePercentageUpdate(percents,product_id,prod_name,port,server_id,compositions,final_aftereffect,random_number,random_scene,"render_"+random_number+".mp4"))
                     result.on('finished',job => module.exports.render_test_preview_completed("render_"+random_number+".mp4",image_frame,random_number,product_id,compositions,now,audio_file,file_duration,port,server_id,random_scene))
                     result.on('error',err => module.exports.render_test_preview_stopped(compositions,err,port,server_id,product_id))
                     }
                        main().catch(err => {
                        console.log(err);
                        res.status(500).json({
                        error: err
                        });
                      })
                     })
                    count++;
                   });
                 }


      /* test render for admin panel */
     exports.test_image_preview = (req,res,next) => {
      req.setTimeout(18000000);
          let count = 0;
          let scene = req.body.scene;
          var server_id,port;

          scene.forEach(function(element,i) {
           var file_type = element.fileName;
           var random_scene = element.random_scene;
           var prod_name = element.prod_name;
           var image_frame = splitToNumbers(element.imageFrame);
           var file_duration = splitToNumbers(element.fileDuration);
           var file_str = file_type.split(".");
           var renderModule = element.original.renderSettings;
           var outputModule = element.original.outputModule;
           var switch_on = "1";
           var optionsNodename = element.original.nodeName;
           var optionsOutputext = element.original.outputExt;
           var compositions = element.compositions;
           var render_time = element.render_time;
           
               var input,output,outputExt,file_start_duration,file_end_duration;
               input = "result_00000.jpg";
               output = "result_" + Math.floor(new Date().valueOf() * Math.random())+".jpg";
               outputExt = "jpg";
               file_start_duration = image_frame;
               file_end_duration = image_frame;

           var assets_url = keys.utility_url +'/' + file_str[0];
           var texts = element.text;
           var image = element.images;
           var color = element.color_picker;
           var product_id =element.product_id;
           var audio_file = element.audio.url;
           var category = element.category;
           var random_number = (new Date().getTime()).toString(36);
           var asset = [];
           texts.forEach(function(elements) {
             var text_contents = elements.contents;
            text_contents = replaceAll(text_contents,"^", "\\n");
            var text_nodeName = elements.nodeName;
             var text_color = elements.color;
             var text_fontfamily = elements.font;
             var assets_text_content_json = { "type": "data", "layerName":"Text_01","property": "Source Text","expression": "'"+text_contents+"'","composition":text_nodeName+"_composition"};
             asset.push(assets_text_content_json);
             var assets_text_color_json =  {"type": "data","layerName": "Text_01","property":"Effects.Color.Color","value": convertHex(text_color),"composition":text_nodeName+"_composition"};
             asset.push(assets_text_color_json);
             var assets_text_fontFamily_Json =   {
              "type": "data",
              "layerName": "Text_01",
              "property": "Source Text.font",
              "value": text_fontfamily,
              "composition":text_nodeName+"_composition"
              };
            asset.push(assets_text_fontFamily_Json);
             });

 
           image.forEach(function(elementss,l) {
            var image_url = elementss.url;
            var image_compositionNames;
            var image_nodeName = elementss.nodeName;
            var image_compositionName = elementss.name;
            var image_composition = elementss.image_composition;
            if(image_compositionName === 'Logo 1'){
              image_compositionNames = image_compositionName+"_composition_"+image_composition;
            } else {
              image_compositionNames = image_compositionName+"_composition_1920*1080";
           }
             var assets_image_json = {"src": image_url,"type": 'image',"layerName" : image_nodeName,"composition": image_compositionNames};
             asset.push(assets_image_json);
           });

           color.forEach(function(elementcolors) {
            var color_picker_color = elementcolors.color;
            var color_picker_nodeName = elementcolors.nodename;
            var color_picker_json =  {"type": "data","layerName": "color_picker","property": "Effects.Color_"+color_picker_nodeName+".Color", "value": convertHex(color_picker_color),"composition": "Project_controller"};
            asset.push(color_picker_json);
            });
            
           var assets_options = {"type": "data","layerName": optionsNodename,"property": "Effects.on/off.ADBE Checkbox Control-0001","value": switch_on,"composition": "Watermark"};
           asset.push(assets_options);
           var assets_opocity = {"type": "data","layerName": "Watermark","property": "Opacity","value": [20],"composition":compositions};
           asset.push(assets_opocity);

           var watermark_url = keys.data_url+"/Watermarks/studiolancers/Watermark.png";
           var assets_watermark_json = {"src": watermark_url,"type": 'image',"layerName" : "Watermark.png","composition": "Watermark"};
           asset.push(assets_watermark_json);
           var fileUtl = assets_url + '/' + file_str[1] +'.aep';

          if(category === "5f3127b59df15d658b9cfcff"){
           var assets_audio_json = {"src": audio_file, "type": 'audio',"layerName" : "Music.mp3","composition": "Music"};
           asset.push(assets_audio_json);
           fileUtl= assets_url + '/' + file_str[1] +'.aep';
           } 

           
            var now = Date.now();
            var render_server_state_collection = db.get().collection("render_server_state");
            var port_filter = {};
            var query = {
              "port": {
                  "$ne": 6041
              },
              "render_type": "Project Uploader",
              "status": "Online",
              };
            // var sort = [ ["priority", 1] ];
    
            render_server_state_collection.find(query).toArray(function(err,result_port_select) {
            if (err) throw err;
            if(_.size(result_port_select) > 0){
              if(result_port_select[0]['available_slots'] > 0 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var avaiable_slots = Math.abs(result_port_select[0]['available_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {available_slots: parseInt(avaiable_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                 } else if(result_port_select[0]['available_slots'] < 1 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var queue_slots = Math.abs(result_port_select[0]['queue_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {queue_slots: parseInt(queue_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                } else {
                 port = '4000';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }
                 } else {
                 port = '4000';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }


             var dynamic_render_client = createClient({host: keys.render_url+port,secret:"imagesecret"});
             
             const main =  async () => {
             const result = await dynamic_render_client.addJob({
             template: {
                  src: fileUtl,
                  composition: compositions,
                  frameStart: file_start_duration,
                  frameEnd: file_end_duration,
                  frameIncrement: 1,
                  continueOnMissing: false,
                  settingsTemplate:'Image preview',
                  outputModule:'Image preview',
                  outputExt: outputExt,
                  addLicense:false,
                  
                  },
                  assets: asset,
                  actions: {
                   prerender: [],
                   postrender: [
                   {
                     "module": "@nexrender/action-upload",
                     "input": "result_00000.jpg",
                     "provider": "s3",
                     "params": {
                     "region": "us-east-1",
                     "bucket": "projectsfiless",
                     "key": "imagepreview/result_preview_image_"+output,
                     "acl": "public-read"
                     }
                    }
                   ],
                   }
                   })
                     result.on('created', job => console.log("Image preview created"))
                     result.on('started', job => module.exports.image_preview_started(product_id))
                     result.on('finished',job => module.exports.image_preview_completed(product_id,res))
               
                     }
                        main().catch(err => {
                        console.log(err);
                        res.status(500).json({
                        error: err
                        });
                      })
                     })
                    count++;
                   });
                 }
                

                exports.image_preview_started = (product_id) => {
                var prod_comp_collection = db.get().collection("product");
                var now = new Date();
                 var myquery = { _id:ObjectId(product_id)};
                 var newvalues = { $set: {image_renderCreated:now} };
                  prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                 if (err) throw err;
                })
                }

                  exports.image_preview_completed = (product_id,res) => {
                var prod_comp_collection = db.get().collection("product");
                var now = new Date();
                  var myquery = { _id:ObjectId(product_id)};
              prod_comp_collection.find(myquery).toArray( function(err, result_comp_select) {
               if (err) throw err;
               if(_.size(result_comp_select) > 0){
               var final_completed = now - result_comp_select[0]['image_renderCreated'];
               var newvalues = { $set: {image_renderFinished:now,image_render_time:final_completed}};
                  prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                 if (err) throw err;
                   res.status(200).json({ "status": "sucess","render_image_time":final_completed});
                })
               }
             })
           }


                 


      exports.testDebugPrview = (req,res,next) => {
      req.setTimeout(18000000);
           req.setTimeout(18000000);
          let count = 0;
          let scene = req.body.scene;
          var server_id,port;

          scene.forEach(function(element,i) {
           var file_type = element.fileName;
           var random_scene = element.random_scene;
           var prod_name = element.prod_name;
           var image_frame = splitToNumbers(element.imageFrame);
           var file_duration = splitToNumbers(element.fileDuration);
           var file_str = file_type.split(".");
           var renderModule = element.original.renderSettings;
           var outputModule = element.original.outputModule;
           var switch_on = "1";
           var optionsNodename = element.original.nodeName;
           var optionsOutputext = element.original.outputExt;
           var compositions = element.compositions;
           var render_time = element.render_time;
           var assets_url = keys.utility_url +'/' + file_str[0];
           var texts = element.text;
           var image = element.images;
           var color = element.color_picker;
           var product_id =element.product_id;
           var audio_file = element.audio.url;
           var category = element.category;
           var random_number = (new Date().getTime()).toString(36);
           var asset = [];
           texts.forEach(function(elements) {
             var text_contents = elements.contents;
            text_contents = replaceAll(text_contents,"^", "\\n");
            var text_nodeName = elements.nodeName;
             var text_color = elements.color;
             var text_fontfamily = elements.font;
             var assets_text_content_json = { "type": "data", "layerName":"Text_01","property": "Source Text","expression": "'"+text_contents+"'","composition":text_nodeName+"_composition"};
             asset.push(assets_text_content_json);
             var assets_text_color_json =  {"type": "data","layerName": "Text_01","property":"Effects.Color.Color","value": convertHex(text_color),"composition":text_nodeName+"_composition"};
             asset.push(assets_text_color_json);
             var assets_text_fontFamily_Json =   {
              "type": "data",
              "layerName": "Text_01",
              "property": "Source Text.font",
              "value": text_fontfamily,
              "composition":text_nodeName+"_composition"
              };
            asset.push(assets_text_fontFamily_Json);
             });

 
           image.forEach(function(elementss,l) {
            var image_url = elementss.url;
            var image_compositionNames;
            var image_nodeName = elementss.nodeName;
            var image_compositionName = elementss.name;
            var image_composition = elementss.image_composition;
            if(image_compositionName === 'Logo 1'){
              image_compositionNames = image_compositionName+"_composition_"+image_composition;
            } else {
              image_compositionNames = image_compositionName+"_composition_1920*1080";
           }
             var assets_image_json = {"src": image_url,"type": 'image',"layerName" : image_nodeName,"composition": image_compositionNames};
             asset.push(assets_image_json);
           });

           color.forEach(function(elementcolors) {
            var color_picker_color = elementcolors.color;
            var color_picker_nodeName = elementcolors.nodename;
            var color_picker_json =  {"type": "data","layerName": "color_picker","property": "Effects.Color_"+color_picker_nodeName+".Color", "value": convertHex(color_picker_color),"composition": "Project_controller"};
            asset.push(color_picker_json);
            });
            
           var assets_options = {"type": "data","layerName": optionsNodename,"property": "Effects.on/off.ADBE Checkbox Control-0001","value": switch_on,"composition": "Watermark"};
           asset.push(assets_options);



           var assets_opocity = {"type": "data","layerName": "Watermark","property": "Opacity","value": [20],"composition":compositions};
          
           asset.push(assets_opocity);




           var watermark_url = keys.data_url+"/Watermarks/studiolancers/Watermark.png";
           var assets_watermark_json = {"src": watermark_url,"type": 'image',"layerName" : "Watermark.png","composition": "Watermark"};
           asset.push(assets_watermark_json);
          
          
          var fileUtl = assets_url + '/' + file_str[1] +'.aep';

          if(category === "5f3127b59df15d658b9cfcff"){
           var assets_audio_json = {"src": audio_file, "type": 'audio',"layerName" : "Music.mp3","composition": "Music"};
           asset.push(assets_audio_json);
           fileUtl= assets_url + '/' + file_str[1] +'.aep';

           } 

           
            var now = Date.now();

            var render_server_state_collection = db.get().collection("render_server_state");
            var port_filter = {};

            var query = {
              "port": {
                  "$ne": 6041
              },
              "render_type": "Project Uploader",
              "status": "Online",
              };
            // var sort = [ ["priority", 1] ];
    
            render_server_state_collection.find(query).toArray(function(err,result_port_select) {
            if (err) throw err;
            if(_.size(result_port_select) > 0){
              if(result_port_select[0]['available_slots'] > 0 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var avaiable_slots = Math.abs(result_port_select[0]['available_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {available_slots: parseInt(avaiable_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                 } else if(result_port_select[0]['available_slots'] < 1 && result_port_select[0]['queue_slots'] > 0){
                 port = result_port_select[0]['port'];
                 server_id = result_port_select[0]['server_id'];
                 var queue_slots = Math.abs(result_port_select[0]['queue_slots']-1);
                 var myquery = { port: parseInt(port)};
                 var newvalues = { $set: {queue_slots: parseInt(queue_slots)} };
                 render_server_state_collection.updateOne(myquery,newvalues, function(err, result) {});
                } else {
                 port = '6041';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }
                 } else {
                 port = '6041';
                 server_id = '5f3a0967fe08752239f8e0d3';
                 }
             var dynamic_render_client = createClient({host: keys.render_url+"6001",secret:"sdsecret"});
             var final_aftereffect = {"final_video":"render_"+random_number+".mp4","image_frame":image_frame,"random_number":random_number,"product_id":product_id,"compositions":compositions,"now":now,"audio_file":audio_file,"file_duration":file_duration,"port":port,"server_id":server_id,"random_scene":random_scene}
             const main =  async () => {
             const result = await dynamic_render_client.addJob({
             template: {
                  src: fileUtl,
                  composition: compositions,
                  frameStart: 1,
                  frameEnd:file_duration,
                  frameIncrement: 1,
                  continueOnMissing: false,
                  settingsTemplate: renderModule,
                  outputModule: outputModule,
                  outputExt: "mp4",
                  multiFrames:true,
                  addLicense:false,
                  reuse:true,
                 
                  },
                  assets: asset,
                  actions: {
                   prerender: [],
                   postrender: [
                   {
                     "module": "@nexrender/action-upload",
                     "input": "result.mp4",
                     "provider": "s3",
                     "params": {
                     "region": "us-east-1",
                     "bucket": "projectsfiless",
                     "key": "uploaddata/"+random_scene+"/test/render_"+random_number+".mp4",
                     "acl": "public-read"
                     }
                    }
                   ],
                   }
                   })
                     result.on('created', job => console.log())
                     result.on('queued', job =>  console.log())
                     result.on('picked', job => console.log())
                     result.on('started', job => console.log())
                     result.on('progress', (job, percents) => console.log(percents))
                     result.on('finished',job => console.log())
                     result.on('error',err => console.log(err))
                     }
                        main().catch(err => {
                        console.log(err);
                        res.status(500).json({
                        error: err
                        });
                      })
                     })
                    count++;
                   });
                 }

               exports.updatePercentageUpdate = (percents,product_id,prod_name,port,server_id,compositions,final_aftereffect,random_number,random_scene,afterurl) => {
               var prod_comp_collection = db.get().collection("prod_comp");
                 var myquery = { comp_name:compositions,prod_id:ObjectId(product_id)};
               
                var newvalues = { $set: {uploadPercentage:percents} };
                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
               if (err) throw err;
               
                })
                }
        




            exports.updateCreatedState = (product_id,prod_name,port,server_id,compositions,final_aftereffect,random_number,random_scene,afterurl,res) => {
              var render_server_state_collection = db.get().collection("render_server_state");
              var uploader_queue_collection = db.get().collection("uploader_queue");
               var prod_comp_collection = db.get().collection("prod_comp");
              uploader_queue_collection.find({'product_id':ObjectId(product_id),'compositions':compositions}).toArray( function(err, result_comp_select) {
               if (err) throw err;
             
               if(_.size(result_comp_select) > 0){


                 var myquery = { product_id:ObjectId(product_id),compositions:compositions};
                 var newvalues = { $set: {created_status: 1, prod_name:prod_name,picked_status:0,rendering_status:0,finalized_status:0,failure_status:0,port_number:parseInt(port),server_id:server_id}};
                 uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                var umyquery = { prod_id:parseInt(product_id),comp_name:parseInt(compositions)};
                var unewvalues = { $set: {render_status: 1} };
                uploader_queue_collection.updateOne(umyquery,unewvalues, function(err, item_update_result) {
                 if (err) throw err; 
                
                 var myquery = { comp_name:compositions,prod_id:ObjectId(product_id)};
                var final_upload_url = keys.upload_url+random_scene+'/modified_'+afterurl;
                var aftereffect_url = keys.upload_url+random_scene+'/test/'+afterurl;
                var newvalues = { $set: {aftereffect_url:aftereffect_url,final_upload_url:final_upload_url,final_aftereffect:final_aftereffect} };
                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
               if (err) throw err;
               res.status(200).json({ "status": "success","comment": "Render has been created", "result_url":"","result_image_url":""});
                })

               });
                 });
                 } else {
               var state_data = {product_id:ObjectId(product_id),
                                server_id:server_id,
                                prod_name:prod_name,
                                port_number:parseInt(port),
                                created_status:1,
                                picked_status:0,
                                rendering_status:0,
                                finalized_status:0,
                                failure_status:0,
                                port_number:parseInt(port),
                                compositions:compositions};
              uploader_queue_collection.insertOne(state_data, function(err, result_order_item) { 
              if (err) throw err;
              var umyquery = { prod_id:parseInt(product_id),comp_name:parseInt(compositions)};
              var unewvalues = { $set: {render_status: 1} };
              uploader_queue_collection.updateOne(umyquery,unewvalues, function(err, item_update_result) {
                if (err) throw err; 
           
                var myquery = { comp_name:compositions,prod_id:ObjectId(product_id)};
                var final_upload_url = keys.upload_url+random_scene+'/modified_'+afterurl;
                var aftereffect_url = keys.upload_url+random_scene+'/test/'+afterurl;
                var newvalues = { $set: {aftereffect_url:aftereffect_url,final_upload_url:final_upload_url,final_aftereffect:final_aftereffect} };
                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
               if (err) throw err;
               res.status(200).json({ "status": "success","comment": "Render has been created", "result_url":"","result_image_url":""});
                })
                });
                });
                }
                });
               }


           exports.updatePickedState = (product_id,port,server_id,compositions) => {
             var uploader_queue_collection = db.get().collection("uploader_queue");
              uploader_queue_collection.find({'product_id':product_id,'compositions':compositions}).toArray( function(err, result_comp_select) {
               if (err) throw err;
               if(_.size(result_comp_select) > 0){
                var myquery = { product_id:ObjectId(product_id),compositions:compositions};
                var newvalues = { $set: {picked_status: 1,port_number:parseInt(port)} };
                uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                });
                 } else {
                }
                });
              }
           

            exports.updateQueueState = (product_id,port,server_id,compositions) => {
              var uploader_queue_collection = db.get().collection("uploader_queue");
              uploader_queue_collection.find({'product_id':ObjectId(product_id),'compositions':compositions}).toArray( function(err, result_comp_select) {
               if (err) throw err;
               if(_.size(result_comp_select) > 0){
                var myquery = { product_id:ObjectId(product_id),compositions:compositions};
                var newvalues = { $set: {queue_status: 1,port_number:port} };
                uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                });
               } else {
                          }
           });
           }


           exports.render_test_preview_started = (product_id,port,server_id,compositions) => {
              var now = Date.now();
              var prod_comp_collection = db.get().collection("prod_comp");
              var myquery = { comp_name:compositions,prod_id:ObjectId(product_id)};
              var newvalues = { $set: {render_start_time: parseInt(now),render_status:2} };
              prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
              if (err) throw err;
              });

               var uploader_queue_collection = db.get().collection("uploader_queue");
                uploader_queue_collection.find({'product_id':ObjectId(product_id),'compositions':compositions}).toArray( function(err, result_comp_select) {
                 if (err) throw err;
                 if(_.size(result_comp_select) > 0){
                 var myquery = { product_id:ObjectId(product_id),compositions:compositions};
                 var newvalues = { $set: {rendering_status: 1,port_number:parseInt(port)} };
                 uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                 if (err) throw err;
                 });
                 } else {
                 } 
                });
               }

            exports.render_test_preview_stopped = (comp,err,port,server_id,product_id,res) => {
              console.log(err)
              var uploader_queue_collection = db.get().collection("uploader_queue");
              var prod_comp_collection = db.get().collection("prod_comp");
              var render_server_state_collection = db.get().collection("render_server_state");
              render_server_state_collection.find({'port':parseInt(port)}).toArray( function(err, result_port_select) {
              if (err) throw err;
               var available_slots = result_port_select[0]['available_slots'] + 1;
               var myquery = { port:parseInt(port)};
               var newvalues = { $set: {available_slots:available_slots} };
               render_server_state_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
               if (err) throw err;
                });
              });

              var myquery = { comp_name:comp,prod_id:ObjectId(product_id)};
              var newvalues = { $set: {render_status: 4,port_number:parseInt(port),nexrender_status:err} };
              prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
              if (err) throw err;
              });

              var myquery = { product_id:ObjectId(product_id),compositions:comp};
              var newvalues = { $set: {failure_status: 1,port_number:parseInt(port)} };
              uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
              if (err) throw err;
              });

              var product_link = "https://staging.onemaker.io/adminpanel/apps/user/user-view/"+product_id;

                 let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });

                      var mailOptions = {
                      to:'vivekrana46@gmail.com',
                      from: 'no-reply@onemaker.io',
                      subject: 'Project Uploader Stopped Via staging server',
                      text: 'Hello,\n\n' +
                     'Your render is Stopped Due to some technical issue for product_id : '+product_id+' , Project Link : '+product_link
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

                      var mailOptions = {
                      to:"srivastava.kushagra7@gmail.com",
                     from: 'no-reply@onemaker.io',
                      subject: 'Project Render Stopped Via staging server',
                      text: 'Hello,\n\n' +
                      'Your render is Stopped Due to some technical issue for product_id : '+product_id+' , Project Link : '+product_link
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });


            }


     
           exports.render_test_preview_completed = (product_id,image_frame,random_number,id,comp,prestnt_time,audio_file,file_duration,port,server_id,random_scene,res) => {
              var now = Date.now();
              var uploader_queue_collection = db.get().collection("uploader_queue");
              var prod_comp_collection = db.get().collection("prod_comp");
              var render_server_state_collection = db.get().collection("render_server_state");
              var prod_collection = db.get().collection("product");
              var myquery = { product_id:ObjectId(id),compositions:comp};
              var newvalues = { $set: {finalized_status: 1} };
              uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
              if (err) throw err;
              });              

               render_server_state_collection.find({'port':parseInt(port)}).toArray( function(err, result_port_select) {
                if (err) throw err;
                if(parseInt(port) === 6001){
                  if(parseInt(result_port_select[0]['available_slots']) <  4){
                    available_slots = Math.floor(parseInt(result_port_select[0]['available_slots']) + 1);
                    queue_slots = result_port_select[0]['queue_slots'];
                  } else {
                    available_slots = 4;
                    queue_slots = Math.floor(result_port_select[0]['queue_slots'] + 1);
                  }
                } else {
                  var available_slots = 1;
                  var queue_slots = 1;
                }
                var myquery = { port:parseInt(port)};
                var newvalues = { $set: {available_slots: parseInt(available_slots),queue_slots:parseInt(queue_slots)} };
                render_server_state_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                });
              })

                
               prod_comp_collection.find({'prod_id':ObjectId(id),'comp_name':comp}).toArray( function(err, item_select_result) {
                if (err) throw err;
              var diff = Math.floor((now - item_select_result[0]['render_start_time']) / 1000);
               diff = Math.floor(diff - 20);
                var myquery = { comp_name:comp,prod_id:ObjectId(id)};
                var newvalues = { $set: {render_end_time:now,render_time:diff,nexrender_status:"render is complete"} };
                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                });
                });
          var download = function(uri, filename, callback){
                   request.head(uri, function(err, res, body){
                    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
                   });
                };
            var imageFrame = Math.floor(image_frame/30);
            download(keys.upload_url+random_scene+'/test/'+product_id, product_id, function(){
            download(audio_file,"audio_"+random_number+"_.mp3", function(){
              const child = exec('ffmpeg -i '+product_id+' -ss '+imageFrame+' -vframes 1 -q:v 2 render_' + random_number +'.jpeg', (error, stdout, stderr) => {
              if (error) {throw error;}
              fs.readFile('render_' + random_number +'.jpeg', function (err, data) {
                if (err) { throw err; }
                   const s3Render = async () => {
                    await s3.putObject({
                      Body: data,
                      Bucket: "projectsfiless",
                      Key: "uploaddata/"+random_scene+"/render_"+ random_number +"_modified.jpeg",
                      ACL: 'public-read',
                      ContentType: 'binary',
                      CacheControl: 'max-age=172800'
                      }
                    ,(err, data) => {
                     if (err) {console.log(err);
                      } else {
                      fs.unlink('render_' + random_number +'.jpeg', (err) => {
                      if (err) {console.log("failed to delete local image:"+err);}
                       });
                      }
                    });
                   }

                    s3Render().catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });

                  });

                  });

                var endkeyframe = Math.floor(file_duration/30);
                var endTime = endkeyframe -2;
                const mp3_child = exec('ffmpeg -i  '+product_id+' -stream_loop -1 -i audio_'+random_number+'_.mp3 -af "apad,afade=type=out:start_time="'+endTime+'":duration=2" -shortest -map 0:v:0 -map 1:a:0 -y modified_'+product_id, (error, stdout, stderr) => {
                if (error) {throw error;}
                fs.readFile('modified_'+product_id, function (err, data) {
                if (err) { throw err; }
                const s3Render = async () => {
                    await s3.putObject({
                      Body: data,
                      Bucket: "projectsfiless",
                      Key: "uploaddata/"+random_scene+"/modified_"+product_id,
                      ACL: 'public-read',
                      CacheControl: 'public, max-age=50',
                      }
                     ,(err, data) => {
                      if (err) {console.log(err);
                       } else {
                        setTimeout(function() {                        
                        fs.unlink("modified_"+product_id, (err) => {
                         if (err) {
                           console.log("failed to delete local image:"+err);
                          } else {
                            try {
                                  fs.unlinkSync('audio_'+random_number+'_.mp3')
                                  fs.unlinkSync(product_id)
                                  //file removed
                                   } catch(err) {
                                   console.error(err)
                                }
                                var image_url = keys.upload_url+random_scene+ "/render_" + random_number + "_modified.jpeg";
                               var myquery = { comp_name:comp,prod_id:ObjectId(id)};
                                var newvalues = { $set: {preview_video: keys.upload_url+random_scene+'/modified_'+product_id,render_status:3,preview_image: image_url,fileUpload:1,render_finalized:now} };
                                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                                if (err) throw err;
                                var myquery = { _id:ObjectId(id)};
                                    var newvalues = { $set: {product_preview:image_url}};
                                    prod_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                                    if (err) throw err;
                                    });
                                });
                                }
                              })
                            ,15000})
                              }
                            });
                           }
                  s3Render().catch(err => {
                       console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });

                  });
                })  //=== end of mp3


               });
               });
                  
            }



          /* Start the CRON RENDER*/
           exports.cronrender = (req,res,next) => {
            var order_item_collection = db.get().collection("order_item");
            var render_ID = req.body.rerenderItem;
            if(_.size(render_ID)>0){
            render_ID.forEach(function(elementss,j) {
            var item_buffer_id = elementss[j];
            order_item_collection.find({item_buffer_id:item_buffer_id,image_render:1,render_status:{"$in" : [0,1,2]}}).toArray( function(err, result) {
            if(_.size(result)>0){
            let render_request = result[0]['render_request'];
            let render_type =  result[0]['render_type'];
            if(render_request && render_type==="SD"){
            module.exports.update_get_render(render_request,"4041","5f38e56112cd84601687e841",req,res,next);    
            } else if(render_request && render_type==="HD"){
            var item_parent_id = result[0]['item_parent_id'];
            module.exports.get_item_hd_render(item_buffer_id,"5041","5f38e56112cd84601687e841",req,res,next);    
            } else {
            console.log("ID is not found ")
            }
            }
            })
            if(j === render_ID.length-1){
            res.status(200).json({ "status": "sucess","comment":"sucess"});
            }
            })
            } else {
            res.status(200).json({ "status": "sucess","comment":"Item ID is not found"});
            }
           }

          /* END CRON RE_RENDER */
          
          /* Start CRON for SD Render */

          exports.update_get_render = (render_request,port,server_id,req, res, next) => {

          var user_id = req.body.user_id;
          var lead_id = req.body.lead_id;
          var item_buffer_id = req.body.item_id;
          var order_id = req.body.order_id;
          var product_id = req.body.product_id;
          var comp_method = req.body.comp_method;
          var state_id = req.body.state_id;
          var randomString = md5(Math.random()).substr(0, 5);
          var x = 0;
          var y = 0;
          var port,server_id;
          var Videoresult = [];
          var video_preview = {};
          if(order_id == "" || order_id == null || order_id == undefined || order_id === '0'){
            order_id = "000000000000000000000000";
          }
          if(user_id == "" || user_id == null || user_id == undefined || user_id === '0'){
            user_id = "000000000000000000000000";
          }
          if(lead_id == "" || lead_id == null || lead_id == undefined || lead_id === '0'){
            lead_id = "000000000000000000000000";
          }
          if(state_id === "" || state_id === null || state_id === undefined || state_id === '0'){
            state_id = "000000000000000000000000";
          }
          db.get().collection("product").find(
            { 
                "pro_buffer_id" : product_id
            }
        ).toArray( function(err, result_item_pro) {
        if (err) throw err;
        var pro_price = result_item_pro[0]['pro_price'];
 
 
        db.get().collection("render_server_state").find(
          { 
              "available_slots" : { 
                  "$gt" : 1
              }, 
              "render_type" : "SD", 
              "port" : { 
                  "$ne" : "4041"
              }, 
              "status" : "Online", 
              "server_status" : "1"
          }, 
          { 
              "port" : "$port", 
              "available_slots" : "$available_slots", 
              "id" : "$id", 
              
          }
      ).sort(
          { 
              "priority" : 1
          }
      ).toArray( function(err, result_port_select) {
        if (err) throw err;
         // if(_.size(result_port_select) > 0){
                    port = '4041';
                   server_id = '5f3a0967fe08752239f8e0d3';
                  // var avaiable_slots = Math.abs(result_port_select[0]['available_slots']-1);
                  // var update_item_percentage_sql =  "UPDATE `render_server_state` SET `available_slots` = '"+avaiable_slots+"' WHERE `port` = '"+port+"'";
                  //  db.query(update_item_percentage_sql, function (err, result_comp_text) {});
                  //  } else {
                  //  port = '4041';
                  //  server_id = '2';
                  //  }
           db.get().collection("order_item").find(
             { 
              "item_buffer_id" : item_buffer_id
             }
            ).toArray( function(err, result_item_order_select) {
            if (err) throw err;
            var item_id = result_item_order_select[0]['_id']; 
            var order_item_buffer = item_buffer_id; 
            var new_time_stamp = Date.now();
            var audio_json = req.body.scenes[0]['audio'];
            var audio_edit = 0;
            var audio_status = audio_json.enabled;
              if(audio_status===true){
                audio_edit = 1
              } else {
                audio_edit = 0;
              }

            var audio_url = audio_json.url;

            var update_download_product = {$set:{item_buffer_id:order_item_buffer,
                                                 final_upload_status:0,
                                                 item_video_preview:"",
                                                 item_image_preview:"",
                                                 frame_set:0,
                                                 render_1:0,
                                                 render_2:0,
                                                 render_3:0,
                                                 render_4:0,
                                                 render_5:0,
                                                 render_percentage:0,
                                                 image_render:0,
                                                 sd_gif_url:'',
                                                 hd_gf_url:'',
                                                 render_created:new_time_stamp,
                                                 audio_url:audio_url,
                                                 audio_image:audio_json['image'],
                                                 audio_name:audio_json['name'],
                                                 audio_status:audio_edit,
                                                 render_status:0,
                                                 item_status:0,
                                                 image_preview:"",
                                                 state_id:state_id}};
             var download_query = {_id:ObjectId(item_id)};
             db.get().collection("order_item").updateOne(download_query,update_download_product, function(err, result) {});
             var product_temp_id = result_item_pro[0]['_id'];

             db.get().collection("prod_comp").find(
              { 
                  "prod_id" : ObjectId(product_temp_id), 
                  "comp_method":comp_method
              }
            ).toArray( function(err, result_comp1) {
             if (err) throw err;
             var ip = JSON.stringify(req.ip)
             var order_item_tracking = {order_item:item_id,
                                        lead_id:lead_id,
                                        user_id:user_id.toString(),
                                        ip_address:ip,
                                        sd_download:1,
                                        render_created:Date.now(),
                                        hd_download:0};
            db.get().collection("lead_user_download").insertOne(order_item_tracking, function(err, result_order_tracking_insert) {});
            db.get().collection("order_item_comp").find(
              { 
                  "item_id" : ObjectId(item_id), 
                  
              }
            ).toArray( function(err, result_item_comp) {
            if (err) throw err;
            if(_.size(result_item_comp) > 0) {
              var order_item_udate_comp = {$set:{comp_name:result_comp1[0]['comp_name'],
                                          comp_method:result_comp1[0]['comp_method'],
                                          original_width:result_comp1[0]['original_width'],
                                          original_height:result_comp1[0]['original_height'],
                                          preview_video:result_comp1[0]['preview_video'],
                                          preview_image:result_comp1[0]['preview_image'],
                                          preview_width:result_comp1[0]['preview_width'],
                                          preview_heigh:result_comp1[0]['preview_heigh'],
                                          key_time:result_comp1[0]['key_time'],
                                          status:result_comp1[0]['status'],
                                          render_time:result_comp1[0]['render_time']}};
            var download_query = {item_id:ObjectId(item_id)};
            db.get().collection("order_item_comp").updateOne(download_query,order_item_udate_comp, function(err, result) {});

            var update_download_product = {$set:{
                                          item_video_preview:result_comp1[0]['preview_video'],
                                          item_image_preview:result_comp1[0]['preview_image']
                                        }};
            var download_query = {_id:ObjectId(item_id)};
           db.get().collection("order_item").updateOne(download_query,update_download_product, function(err, result) {});


               let image_result =  req.body.scenes[0]['images'];
               for (let i =0; i < image_result.length; i++){
               let visible_status,original_cropper_url,scale;
               if(image_result[i]['visible'] && image_result[i]['visible'] === "true"){
                 visible_status = '1';              
                 } else {
                 visible_status = '0';
                }
              if(image_result[i]['original_cropper_url'] && image_result[i]['original_cropper_url'].length > 0){
                 original_cropper_url = image_result[i]['original_cropper_url'];
                 scale = image_result[i]['scale'];
                 } else {
                 original_cropper_url = 0;
                 scale = 0;
                 };
               if(image_result[i]['position']){
                   x = image_result[i]['position']['x'].toString();
                   y = image_result[i]['position']['y'].toString();
                   } else {
                   x = 0;
                   y = 0;
                   };
                  var order_item_image = {$set:{image_url:image_result[i]['url'],file_url:image_result[i]['url'],visible:visible_status,original_cropper_url:original_cropper_url,scale:scale,x:x,y:y}};
                   var download_query = {item_id:ObjectId(item_id),name:image_result[i]['name']};
                  db.get().collection("order_item_image").updateOne(download_query,order_item_image, function(err, result_comp_image_update_order) {});
                  }
                  
                  let text_result =  req.body.scenes[0]['text'];
                  let result_comp_item_text_length = text_result.length;
                  for (let i =0; i < result_comp_item_text_length; i++){

                  var text_contents = replaceAll(text_result[i]['contents'],"\n", "^");
                  var order_item_text = {$set:{display_content:text_contents,color:text_result[i]['color'],font_family:text_result[i]['font'], max_lines:text_result[i]['contents'].search("\n") ? text_result[i]['contents'].split("\n").length - 1:0,allow_multiline:text_result[i]['contents'].search("\n") > 0 ? 1:0}};
                  var download_query = {item_id:ObjectId(item_id),name:text_result[i]['nodeName']};
                  db.get().collection("order_item_text").updateOne(download_query,order_item_text, function(err, result_comp_text_update_order) {});
                  }

                    let color_result =  req.body.scenes[0]['color_picker'];
                     let result_comp_item_color_length = color_result.length;
                  
                     for (var i =0; i < result_comp_item_color_length; i++){
                      var order_item_color = {$set:{color:color_result[i]['color']}};
                      var download_query = {item_id:ObjectId(item_id),name:color_result[i]['name']};
                      db.get().collection("order_item_color").updateOne(download_query,order_item_color, function(err, result_comp_color_update_order) {});
                      }
                      // }
                      // });

                      var watermark = '0';
                      db.get().collection("order").find(
                        { 
                          "user_id" : ObjectId(user_id), 
                          "order_type" : { 
                                "$in" : [
                                    1, 
                                    2, 
                                    3, 
                                    5
                                ]
                            }
                         }
                         ).toArray( function(err, result) {
                         if (err) throw err;
                         if(_.size(result) > 0){
                          watermark = '0'
                         } else {
                           if(pro_price === 0){
                           watermark ='0';
                           } else {
                            watermark ='1';
                           }
                         }   

                    
                      var scene = req.body.scenes;
                      var scene_length = scene.length;
                      fs.mkdir(item_id.toString(),function(e){
                      if(!e || (e && e.code === 'EEXIST')){
                      //do something with contents
                       } else {
                       //debug
                       console.log(e);
                       }
                       });
                         
                     
            var duration = scene[0]['fileDuration'];
            var comp_name = result_comp1[0]['comp_name'];
            if(result_item_pro[0]['category']==="5f3127b59df15d658b9cfcff"){
              var download = function(uri, filename, callback){
              request.head(uri, function(err, res, body){
              request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
              });
              };
             var random_audio_key = generateUniqueString();
              download(audio_json['url'], "./"+random_audio_key+".mp3", function(){
               fs.readFile("./"+random_audio_key+".mp3", function (err, data) {
                   if (err) { throw err; }
                   var duration_temp = Math.floor(getMP3Duration(data)/1000)
                    duration = Math.floor(duration_temp * 30);
                    module.exports.test_item_render(comp_name,result_item_pro[0]['filePath'],result_item_pro[0]['_id'].toString(),result_item_pro[0]['pro_buffer_id'],result_item_pro[0]['image_frame'],scene,item_id,order_item_buffer,user_id,result_item_pro[0]['category'],port,server_id,watermark,randomString,duration,req,res,next);    
                    })
                   })
                 } else {
                  duration = duration;
                  module.exports.test_item_render(comp_name,result_item_pro[0]['filePath'],result_item_pro[0]['_id'].toString(),result_item_pro[0]['pro_buffer_id'],result_item_pro[0]['image_frame'],scene,item_id,order_item_buffer,user_id,result_item_pro[0]['category'],port,server_id,watermark,randomString,duration,req,res,next);    
                 }
                     //module.exports.test_item_render(result_comp1[0]['comp_name'],result_item_pro[0]['filePath'],result_item_pro[0]['image_frame'],scene,item_id,order_item_buffer,user_id,result_item_pro[0]['category'],port,server_id,watermark,randomString,req,res,next);    
                  })
                 res.status(201).json({ "status": "sucess","comment":"sucess","item_video_preview": result_url + item_id+'/preview_'+randomString+'_file.mp4',"item_image_preview": result_url + item_id +'/preview_'+randomString+'_file.jpeg',"item_id":order_item_buffer,"product_id":product_id,"state_id":state_id});
                     }
                     });
                     });
                     });
                     });
                     });

  
            };





   exports.test_item_render = (compositions_name,filepath,product_url,react_url,image_frame,scene,itemID,item_buffer_id,user_id,category,port,server_id,watermark,randomString,duration,req,res,next) => {
        let count = 0;
        //let count = 0;
        scene.forEach(function(element,i) {
          var file_type = filepath;
          var file_duration = category === "5f3127b59df15d658b9cfcff" ? duration : splitToNumbers(duration);
          image_frame = splitToNumbers(image_frame);
          var file_str = file_type.split(".");
          var renderModule = element.original.renderSettings;
          var outputModule = element.original.outputModule;
          var switch_on = watermark;
          var optionsNodename = element.original.nodeName;
          var optionsOutputext = element.original.outputExt;
          var compositions = compositions_name;
          var render_time = element.render_time;
          var assets_url = utility_url +'/' + file_str[0];
          var texts = element.text;
          var image = element.images;
          var color = element.color_picker;
          var audio_file
          var audio_status = element.audio.enabled;
          if(audio_status === true){
           audio_file = element.audio.url;
         } else {
           audio_file = keys.asset_url+"/demo_data/mute.mp3";
         }
         if(parseInt(file_duration) < 900 && category === "5f3127b59df15d658b9cfcff"){
           file_duration = file_duration;
         } else if(parseInt(file_duration) > 900 && category === "5f3127b59df15d658b9cfcff"){
           file_duration = "900";
         } else {
           file_duration = file_duration;
         }
         if(audio_status === true){
            audio_file = element.audio.url;
          } else {
            audio_file = keys.asset_url+"/demo_data/mute.mp3";
          }
         var asset = [];
         texts.forEach(function(elements) {
             var text_contents = elements.contents;
                text_contents = replaceAll(text_contents,"\n", "\\n");
             var text_nodeName = elements.nodeName;
             var text_color = elements.color;
             var text_fontfamily = elements.font;
             var assets_text_content_json = { "type": "data", "layerName":"Text_01","property": "Source Text","expression": "'"+text_contents+"'","composition":text_nodeName+"_composition"};
             asset.push(assets_text_content_json);
             var assets_text_color_json =  {"type": "data","layerName": "Text_01","property":"Effects.Color.Color","value": convertHex(text_color),"composition":text_nodeName+"_composition"};
            asset.push(assets_text_color_json);


          var assets_text_fontFamily_Json =   {
            "type": "data",
               "layerName": "Text_01",
            "property": "Source Text.font",
            "value": text_fontfamily,
            "composition":text_nodeName+"_composition"
          };

           asset.push(assets_text_fontFamily_Json);
             });

           image.forEach(function(elementss,l) {
              var image_url = "";
             var image_compositionNames;
             var image_nodeName = elementss.nodeName;
             var image_compositionName = elementss.name;
             var image_composition = elementss.image_composition;

             

             if(image_compositionName === 'Logo 1'){
              image_compositionNames = image_compositionName+"_composition_"+image_composition;
             } else {
              image_compositionNames = image_compositionName+"_composition_1920*1080";
             }

             if(elementss.visible && elementss.visible === "true"){
              image_url = elementss.url;
                } else {
               image_url = keys.data_url+"Watermarks/transparent.png";
              }

          
            var assets_image_json = {"src": image_url,"type": 'image',"layerName" : image_nodeName,"composition": image_compositionNames};
            asset.push(assets_image_json);
            
          });

    
              color.forEach(function(elementcolors) {
             var color_picker_color = elementcolors.color;
             var color_picker_nodeName = elementcolors.nodename;
              var color_picker_json =  {"type": "data","layerName": "color_picker","property": "Effects.Color_"+color_picker_nodeName+".Color", "value":convertHex(color_picker_color),"composition": "Project_controller"};
              asset.push(color_picker_json);
            
             });

            if(category === "5f3127b59df15d658b9cfcff"){
             var assets_audio_json = {"src": audio_file, "type": 'audio',"layerName" : "Music.mp3","composition": "Music"};
             asset.push(assets_audio_json);

             } 
            

            var assets_options = {"type": "data","layerName": optionsNodename,"property": "Effects.on/off.ADBE Checkbox Control-0001","value": switch_on,"composition": "Watermark"};
             asset.push(assets_options);

             var watermark_url = keys.data_url+"/Watermarks/studiolancers/Watermark.png";

             var assets_watermark_json = {"src": watermark_url,"type": 'image',"layerName" : "Watermark.png","composition": "Watermark"};
            asset.push(assets_watermark_json);


            var division_percentage = 100;
            if(render_time > 180){
             division_percentage = 50;
              } else if(render_time > 120 && render_time < 180){
              division_percentage = 50;
              } else if(render_time > 60 && render_time < 120){
            division_percentage = 50;
              } else {
              division_percentage = 50;
            }
                                        

                       
               var check = 0;
               var ids=[];

           
               var render_time = 100;
               
               var frameSet = render_time / division_percentage;
               var remainingframeSet = render_time % division_percentage;
      

               if(remainingframeSet > 0 ){
                  frameSet = frameSet + 1;
                  var avarageFileDuration = file_duration / frameSet;
                  var update_avarageFileDuration =  Math.floor(avarageFileDuration);
                  var remaining_fileDuration = avarageFileDuration - update_avarageFileDuration;

               } else {

               var avarageFileDuration = file_duration / frameSet;
               var update_avarageFileDuration =  Math.floor(avarageFileDuration);
               var remaining_fileDuration = avarageFileDuration - update_avarageFileDuration;

               }

               var avarageFileDuration = file_duration / frameSet;
               var avarageFileDuration =  Math.floor(avarageFileDuration);
               var startFrame = 2;
               var endFrame;
               var j= 0;

              for(let k=0; k < frameSet; k++){
                if(k > 0 && k != Math.floor(frameSet -1)){
                    startFrame = endFrame + 1;
                    endFrame = startFrame + avarageFileDuration;
                  } else if(k == Math.floor(frameSet -1)) {
                    startFrame = endFrame + 1;
                    endFrame = Math.floor(startFrame + avarageFileDuration + remaining_fileDuration);
                      if(endFrame === file_duration){
                        endFrame = endFrame;
                       } else {
                      temp_duration = file_duration - endFrame;
                      endFrame = temp_duration + endFrame;
                    }
                  } else {
                  
                    startFrame = startFrame;
                    endFrame = startFrame + avarageFileDuration;
                  }
                  var dynamic_render_client = createClient({host:keys.render_url+port,secret:"sdsecret"});
                  const main =  async () => {
                    const result = await dynamic_render_client.addJob({
                          template: {
                            src: assets_url + '/' + file_str[1] +'.aep',
                            composition: compositions,
                            frameStart: startFrame,
                            frameEnd: endFrame,
                            continueOnMissing: false,
                            settingsTemplate: renderModule,
                            outputModule: outputModule,
                            outputExt: "mp4",
                            addLicense:false
                            },
                        assets: asset,
                        actions: {
                            prerender: [
                               // { module: '@inlife/crop-images' },
                            ],
                            postrender: [
                             {
                              "module": "@nexrender/action-upload",
                              "input": "result.mp4",
                              "provider": "s3",
                              
                              "params": {
                              "region": "us-east-1",
                              "bucket": "consumerdatas",
                              "key": "uploaddata/"+itemID+"/result_preview_image_"+i+"_"+k+"_"+itemID+'_'+randomString+".mp4",
                              "acl": "public-read"
                              }
                              }
                             ]
                           }
                        })

                        result.on('created', job => module.exports.updateSDCreatedState(itemID,k,port,server_id,frameSet))
                        result.on('queued', job =>  module.exports.updateSDQueueState(itemID,k,port,server_id,frameSet))
                        result.on('picked', job => module.exports.updateSDPickedState(itemID,k,port,server_id,frameSet))
                        result.on('progress', (job, percents) => module.exports.SDPercentageUpdate(itemID,percents,k,frameSet,port))
                        result.on('error', err => module.exports.email_render_err(itemID,user_id,err,port,product_url,react_url,req,res,next))
                        result.on('finished', job =>  module.exports.sd_render_item_scene_finished(i,k,itemID,frameSet,ids,item_buffer_id,audio_file,user_id,image_frame,endFrame,port,res,randomString,file_duration,category,next))
                        }
                            
                       main().catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });
                     }
                  count++;
           
                });
            }
         

  exports.sd_render_item_scene_finished = (i,k,itemId,frameSet,ids,item_buffer_id,audio_file,user_id,image_frame,endFrame,port,res,randomStrings) => {
    var randomString = md5(Math.random()).substr(0, 5);
    var download = function(uri, filename, callback){
      request.head(uri, function(err, res, body){
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
         });
       }
      
    download(keys.consumer_url+itemId.toString()+"/result_preview_image_"+i+"_"+k+"_"+itemId.toString()+"_"+randomStrings+".mp4", "./"+itemId.toString()+"/result_preview_image_"+i+"_"+k+"_"+itemId+".mp4", function(){
    ids.push("result_preview_image_"+i+"_"+k+"_"+itemId+".mp4");
      var percentage = Math.floor(ids.length*100/frameSet);
      var now = Date.now();
      if(ids.length === frameSet){
      if(percentage > 100){
        percentage = 100;
       } else {
        percentage = percentage;
       }
       var state_data = {$set:{render_status:3,render_finalized:now}};
       var state_data_query = {_id:ObjectId(itemId)};
       db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
       } else {
       var state_data = {$set:{render_status:2,item_status:2}};
       var state_data_query = {_id:ObjectId(itemId)};
       db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
       }
      if(ids.length === frameSet){
         var sort_array = ids.sort();
         var fileList = sort_array; // files to merge
         // files to merge
         var listFileName = 'list.txt', fileNames = '';
         // ffmpeg -f concat -i mylist.txt -c copy output
         fileList.forEach(function(fileName, index){
           fileNames = fileNames + 'file ' + ' ' + fileName + '\n';
         });
       fs.writeFile(itemId.toString()+"/"+listFileName, fileNames, function(err) {
        if(err) {
        return console.log(err);
        }
       let merge = ffmpeg();
       var video =  merge.addInput(itemId.toString()+"/"+listFileName)
         .inputOptions(['-f concat', '-safe 0'])
         .outputOptions('-c copy')
         .on('progress', (progress) => {
           console.log(progress);
         })
         .on('error', (err) => {
           console.log(err);
         })
         .on('end', () => {
         console.log('finished merging');
         var imagekeyframe = Math.floor(image_frame/30);
         download(audio_file, "audio_"+itemId.toString()+".mp3", function(){
         console.log("download audio file")
       const child = exec('ffmpeg -i final_' + itemId +'.mp4  -ss '+imagekeyframe+' -vframes 1 -q:v 2 final_' + itemId +'.jpeg', (error, stdout, stderr) => {
       if (error) {
        console.error('stderr: =============================', stderr);
        throw error;
        }
        fs.readFile('final_' + itemId +'.jpeg', function (err, data) {
        if (err) { throw err; }
        const s3Render = async () => {
             await s3.putObject({
               Body: data,
               Bucket: "consumerdatas",
               Key: 'uploaddata/'+itemId+'/'+randomString+'/preview_'+randomStrings+'_file.jpeg',
               ACL: 'public-read'
           
           }, (err, data) => {
            if (err) {
            console.log(err);
            } else {
            console.log("Successfully uploaded data ");

            var state_data = {$set:{item_image_preview:keys.consumer_url + itemId +'/'+randomString+'/preview_'+randomStrings+'_file.jpeg'}};
            var state_data_query = {_id:ObjectId(itemId)};
            db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
            fs.unlink('final_' + itemId +'.jpeg', (err) => {
              if (err) {
                 console.log("failed to delete local image:"+err);
               } else {
                  console.log('successfully deleted local image');                                
                   }
                 });
               }
             });
            }

             s3Render().catch(err => {
                 console.log(err);
                 res.status(500).json({
                   error: err
                 });
               });

           });
           });
             /* Gif start */

             

             // const child_gif = exec('ffmpeg -i final_' + itemId +'.mp4 -vf "fps=4,scale=268:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 final_' + itemId +'.gif', (error, stdout, stderr) => {
             //  if (error) {
             //   console.error('stderr: =============================', stderr);
             //   throw error;
             //   }
             //   fs.readFile('final_' + itemId +'.gif', function (err, data) {
             //   if (err) { throw err; }
             //   const s3Render = async () => {
             //        await s3.putObject({
             //          Body: data,
             //          Bucket: "consumerdatas",
             //          Key: 'uploaddata/'+itemId+'/'+randomString+'/preview.gif',
             //          ACL: 'public-read'
                  
             //      }, (err, data) => {
             //       if (err) {
             //       console.log(err);
             //       } else {
             //       console.log("Successfully uploaded data ");

             //       var state_data = {$set:{sd_gif_url:keys.consumer_url + itemId +'/'+randomString+'/preview.gif'}};
             //       var state_data_query = {_id:ObjectId(itemId)};
             //       db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
             //       fs.unlink('final_' + itemId +'.gif', (err) => {
             //         if (err) {
             //            console.log("failed to delete local image:"+err);
             //          } else {
             //             console.log('successfully deleted local image');                                
             //              }
             //            });
             //          }
             //        });
             //       }

             //        s3Render().catch(err => {
             //            console.log(err);
             //            res.status(500).json({
             //              error: err
             //            });
             //          });

             //      });
             //      });

                  /* Gif end */

           
   
     var endtestFrame = Math.floor(endFrame/30);
     var endTime =  endtestFrame -3;
     const mp3_child = exec('ffmpeg -i  final_' + itemId +'.mp4 -stream_loop -1 -i audio_'+itemId.toString()+'.mp3 -map 0:v -map 1:a -c:v copy -shortest -af "apad,afade=type=out:start_time="'+endTime+'":duration=3" final_modified' + itemId +'.mp4', (error, stdout, stderr) => {
         if (error) {
           console.error('stderr: =============================', stderr);
           throw error;
          }
   fs.readFile('final_modified' + itemId +'.mp4', function (err, data) {
             if (err) { throw err; }
            
            const s3Render = async () => {
             await s3.putObject({
               Body: data,
               Bucket: "consumerdatas",
               Key: 'uploaddata/'+ itemId+'/'+randomString+'//preview_'+randomStrings+'_file.mp4',
               ACL: 'public-read'                   
             }
             , (err, data) => {
               if (err) {
                  console.log(err);
               } else {
                 console.log("Successfully uploaded data ");


                if(user_id > 0){
                 //  module.exports.email_render_created(itemId,user_id,"finished")
                  }

                   var stats = fs.statSync('final_' + itemId +'.mp4');

                   if(port === '4041'){
                     console.log("4041")
                  
                    } else {
                     db.get().collection("render_server_state").find(
                       { 
                           "port" :port
                       }).toArray( function(err, result_port_select) {
                      if (err){ throw err;}
                     var available_slots = result_port_select[0]['available_slots'] + 1;
                     var state_data = {$set:{available_slots:available_slots}};
                     var state_data_query = {port:port};
                     db.get().collection("render_server_state").updateOne(state_data_query,state_data, function(err, result_order_item) {});
                     });
                     }
                             
                  var state_data = {$set:{render_percentage:100,final_upload_status:1,fileSize:stats['size'],item_video_preview:keys.consumer_url + itemId +'/'+randomString+'//preview_'+randomStrings+'_file.mp4'}};
                  var state_data_query = {_id:ObjectId(itemId)};
                  db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
                   let dirPath =  "./"+itemId.toString();
                   fs.unlink('final_' + itemId +'.mp4', (err) => {
                  if (err) {
                  console.log("failed to delete local image:"+err);
                   } else {
                  rimraf(itemId.toString()+'/*', function () { console.log('done');
                  fs.rmdirSync(itemId.toString());
                   try {
                        fs.unlinkSync('final_modified' + itemId +'.mp4')
                        fs.unlinkSync('audio_'+itemId.toString()+'.mp3')
                        } catch(err) {
                        console.error(err)
                         }
                      })
                     console.log('successfully deleted local Video');  
                            
                   }
                 });
               }
             });
            }

             s3Render().catch(err => {
                 console.log(err);
                 res.status(500).json({
                   error: err
                 });
               });

           });
         });

        if(!res.headersSent){
             console.log("response send");
             return res.json('/uploads/finalOutput.mov');
           }
      })
         })
         .save('final_' + itemId +'.mp4');
          //video.kill('SIGKILL')
         });                                                    
        }
         return ids;
      });
    }


    exports.updateSDQueueState = (itemID,k,port,server_id,frameSet) => {
      var state_data = {$set:{server_id:ObjectId(server_id),port_number:port,queue_status:1}};
      var order_item_queue = db.get().collection("order_item_queue");
      var state_data_query = {order_item:ObjectId(itemID),job_id:k};
      order_item_queue.updateOne(state_data_query,state_data, function(err, result_order_item) {});
      }



    exports.updateSDPickedState = (itemID,k,port,server_id,frameSet) => {
      var order_item_queue = db.get().collection("order_item_queue");
      order_item_queue.find({ 
        "item_id" :ObjectId(itemID), 
        "job_id" : k            
       }).toArray( function(err, result_comp_select) {
       if (err){ throw err;}
       if(_.size(result_comp_select) > 0){
        var state_data = {$set:{picked_status:1}};
        var state_data_query = {order_item:ObjectId(itemID),job_id:k};
        order_item_queue.updateOne(state_data_query,state_data, function(err, result_order_item) {});
       } else {
       }
       });
    }


exports.updateSDCreatedState = (itemID,k,port,server_id,frameSet) => {
  console.log("SD render via cron")
  var order_item_queue = db.get().collection("order_item_queue");
  order_item_queue.find({ 
    "item_id" :ObjectId(itemID), 
    "job_id" : k            
 }).toArray( function(err, result_comp_select) {
  if (err){ throw err;}
  if(_.size(result_comp_select) > 0){
    var state_data = {$set:{picked_status:1}};
    var state_data_query = {order_item:ObjectId(itemID),job_id:k};
    order_item_queue.updateOne(state_data_query,state_data, function(err, result_order_item) {});
;          } else {
   var state_data = {order_item:ObjectId(itemID),
                   job_id:k,
                   server_id:ObjectId(server_id),
                   port_number:port,
                   created_status:1};
   order_item_queue.insertOne(state_data, function(err, result_order_item) {});
   }
   });
   }


   exports.SDPercentageUpdate = (itemID,percents,k,frameSet,port) => {
   var update_item_percentage_sql,select_item_percentage_sql;
   var percentage = 0;
   var j = k+1;
    if(percents > 100){
        percents = 100;
      } else {
        percents = percents;
     }
      var now = Math.round(new Date()/1000);
      db.get().collection("order_item").find(
        { 
            "_id" : ObjectId(itemID)
        }
      ).toArray( function(err, result_comp_select) {
      if (err){ throw err;}
      if(result_comp_select[0]['render_'+j] === 100){
      var state_data = {$set:{['render_'+j]:percents,frame_set:frameSet,render_status:3,render_updated:now,item_status:2,port_number:port}};
      var state_data_query = {_id:ObjectId(itemID)};
      db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});

      var state_data = {$set:{finalized_status:1}};
      var state_data_query = {order_item:ObjectId(itemID),job_id:k};
      db.get().collection("order_item_queue").updateOne(state_data_query,state_data, function(err, result_order_item) {});
       } else {
        var state_data = {$set:{['render_'+j]:percents,frame_set:frameSet,render_status:2,render_updated:now,item_status:2}};
        var state_data_query = {_id:ObjectId(itemID)};
        db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
        var state_data = {$set:{finalized_status:1}};
        var state_data_query = {order_item:ObjectId(itemID),job_id:k};
        db.get().collection("order_item_queue").updateOne(state_data_query,state_data, function(err, result_order_item) {});
       }
      });
 
     }


/* END CRON FOR SD RENDER */





exports.get_hd_scene = (scene,itemID,item_buffer_id,user_id,comp_method,pro_name,port,server_id,res,next) => {
  let count = 0;
  //let count = 0;
  scene.forEach(function(element,i) {
       var file_type = element.fileName;
     var file_duration = splitToNumbers(element.fileDuration);
     var file_str = file_type.split(".");
     var renderModule = element.original.renderSettings;
     var outputModule = element.original.outputModule;
     var switch_on = element.original.switch;
     var optionsNodename = element.original.nodeName;
     var optionsOutputext = element.original.outputExt;
     var compositions = element.compositions;
     var render_time = element.render_time;
     var assets_url = utility_url +'/' + file_str[0];
     var texts = element.text;
     var image = element.images;
     var color = element.color_picker;
     var audio_file
     var audio_status = element.audio.enabled;
      var category = element.category;

     if(audio_status === true){
      audio_file = element.audio.url;
    } else {
      audio_file = keys.asset_url+"/demo_data/mute.mp3";
    }
  
     var asset = [];


     texts.forEach(function(elements) {
       var text_contents = elements.contents;
       var text_nodeName = elements.nodeName;
       var text_color = elements.color;
       var text_fontfamily = elements.font;
       var assets_text_content_json = { "type": "data", "layerName":text_nodeName,"property": "Source Text","expression": "'"+text_contents+"'","composition":text_nodeName+"_composition"};
       asset.push(assets_text_content_json);
       var assets_text_color_json =  {"type": "data","layerName": text_nodeName,"property":"Effects.Color.Color","value": convertHex(text_color),"composition":text_nodeName+"_composition"};
      asset.push(assets_text_color_json);


       var assets_text_fontFamily_Json =   {
      "type": "data",
      "layerName": text_nodeName,
      "property": "Source Text.font",
      "value": text_fontfamily,
      "composition":text_nodeName+"_composition"
  };

       
      asset.push(assets_text_fontFamily_Json);
       });

     image.forEach(function(elementss,l) {
       var image_url;
       if(elementss.visible && (elementss.visible === 1 || elementss.visible === '1')){
        image_url = elementss.url;
          } else {
         image_url = keys.data_url+ "Watermarks/transparent.png";
        }
     var image_compositionNames;
       var image_nodeName = elementss.nodeName;
       var image_compositionName = elementss.name;
       var image_composition = elementss.image_composition;

       if(image_compositionName === 'Logo 1'){
        image_compositionNames = image_compositionName+"_composition_"+image_composition;
       } else {
        image_compositionNames = image_compositionName+"_composition_1920*1080";
       }
      var assets_image_json = {"src": image_url,"type": 'image',"layerName" : image_nodeName,"composition": image_compositionNames};
      asset.push(assets_image_json);
      
    });


        color.forEach(function(elementcolors) {
       var color_picker_color = elementcolors.color;
       var color_picker_nodeName = elementcolors.nodename;
        var color_picker_json =  {"type": "data","layerName": "color_picker","property": "Effects.Color_"+color_picker_nodeName+".Color", "value":convertHex(color_picker_color),"composition": "Project_controller"};
        asset.push(color_picker_json);
      
       });
      
      var assets_options = {"type": "data","layerName": optionsNodename,"property": "Effects.on/off.ADBE Checkbox Control-0001","value": switch_on,"composition": "Watermark"};
       asset.push(assets_options);

       var watermark_url = keys.data_url+"/Watermarks/studiolancers/Watermark.png";

       var assets_watermark_json = {"src": watermark_url,"type": 'image',"layerName" : "Watermark.png","composition": "Watermark"};
      asset.push(assets_watermark_json);
      

      if(category === 14){
       var assets_audio_json = {"src": audio_file, "type": 'audio',"layerName" : "Music.mp3","composition": "Music"};
       asset.push(assets_audio_json);

       } 

    
       var hd_render_client = createClient({host: keys.render_url+port});
       const main =  async () => {
           const result = await hd_render_client.addJob({
                    template: {
                      src: assets_url + '/' + file_str[1] +'.aep',
                      composition: compositions,
                      frameStart: 1,
                      frameEnd: file_duration,
                      frameIncrement: 1,
                      continueOnMissing: false,
                      settingsTemplate: "HD Render",
                      outputModule: "HD Render",
                      outputExt: "mp4",
                      addLicense:false
                      },
                  assets: asset,
                  actions: {
                      prerender: [
                         // { module: '@inlife/crop-images' },
                      ],
                   postrender: [
                        {
                        "module": "@nexrender/action-upload",
                        "input": "result.mp4",
                        "provider": "s3",
                        "params": {
                            "region": "us-east-1",
                            "bucket": "consumerdatas",
                            "key": "uploaddata/"+itemID+"/result_preview_HD_"+itemID+".mp4",
                            "acl": "public-read"
                          }
                         }
                       ],
                   
                     }
                  })
                  result.on('created', job => console.log('project has been created'))
                  result.on('picked', job => module.exports.render_hd_started(itemID,item_buffer_id))
                  result.on('queue', job => module.exports.render_hd_started(itemID,item_buffer_id))
                  result.on('created', job => module.exports.render_hd_createState(itemID,item_buffer_id,port,server_id))
                //  result.on('created', job => module.exports.email_render_created(itemID,user_id,"HD Created"))
                  result.on('created', job => module.exports.render_hd_started(itemID,item_buffer_id))
                  result.on('progress', (job, percents) => module.exports.render_hd_proceed(itemID,item_buffer_id,percents))
                  result.on('started', job => module.exports.render_hd_startedState(itemID,item_buffer_id,port,server_id))
                  //result.on('started', job => module.exports.email_render_created(itemID,user_id,"HD Started"))
                  result.on('finished',job => module.exports.render_hd_completed(itemID,item_buffer_id,audio_file,user_id,comp_method,pro_name,file_duration,port,server_id,res,next))
                   //module.exports.test(i,k,frameSet,ids,res))  
                }                
                  
                  main().catch(err => {
                  console.log(err);
                  res.status(500).json({
                  error: err
                  });
                  });
                 count++;
                 });
           }
        




          exports.render_hd_started = (itemID,itembufferid) => {
          var update_download_product = {$set:{hd_percentage:1}};
            var download_query = {item_buffer_id:itembufferid,render_type:'SD'};
            db.get().collection("order_item").updateOne(download_query,update_download_product, function(err, result) {});
          }



        exports.render_hd_proceed = (itemID,itembufferid,percentage) => {
          console.log("HD render proceed")
          if(percentage > 90){
            percentage = 90;
          }
        var update_download_product = {$set:{hd_percentage:percentage}};
          var download_query = {item_buffer_id:itembufferid,render_type:'SD'};
        
          db.get().collection("order_item").updateOne(download_query,update_download_product, function(err, result) {});
      
        }


        
        exports.render_hd_startedState = (itemID,k,port,server_id) => {

          db.get().collection("order_item_queue").find(
            { 
                "item_id" : ObjectId(itemID)
            }, 
            { 
                "_id" : "$id", 
                
            }
        ).toArray( function(err, result_comp_select) {
          if (err){ throw err;}
         if(_.size(result_comp_select) > 0){
          var order_item_product = {$set:{picked_status:1}};
          var order_item_query = {order_item:ObjectId(itemID)};
          db.get().collection("order_item_queue").updateOne(order_item_query,order_item_product, function(err, result_order_item) {});

         } else {
         }
        });
        }


    exports.render_hd_createState = (itemID,k,port,server_id) => {
      db.get().collection("order_item_queue").find(
        { 
            "item_id" : ObjectId(itemID)
        }, 
        { 
            "_id" : "$id", 
            
        }
    ).toArray( function(err, result_comp_select) {
      if (err){ throw err;}
     if(_.size(result_comp_select) > 0){
      var order_item_product = {$set:{created_status:1}};
      var order_item_query = {order_item:ObjectId(itemID)};
      db.get().collection("order_item_queue").updateOne(order_item_query,order_item_product, function(err, result_order_item) {});
      } else {
      var order_item_product = {order_item:ObjectId(itemID),server_id:ObjectId(server_id),port_number:port,created_status:1};
      db.get().collection("order_item_queue").insertOne(order_item_product, function(err, result_order_item) {});
       }
       });
      }



      
      exports.render_hd_completed = (itemID,itembufferid,audio_file,user_id,comp_method,pro_name,endFrame,port,server_id,res,next) => {
        var download = function(uri, filename, callback){
                   request.head(uri, function(err, res, body){
                     console.log('content-type:', res.headers['content-type']);
                     console.log('content-length:', res.headers['content-length']);
                     request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
                  });
               };
             

           download(keys.consumer_url+itemID.toString()+"/result_preview_HD_"+itemID+".mp4", "./result_preview_HD_"+itemID+".mp4", function(){
           download(audio_file, "result_preview_HD_"+itemID+".mp3", function(){
            var endsFrame = Math.floor(endFrame/30);
            var endTime = endsFrame -3;

                                 /* Gif start */
                const child_gif = exec('ffmpeg -i result_preview_HD_'+itemID+'.mp4 -vf "fps=4,scale=268:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 result_preview_HD_modified'+itemID+'.gif', (error, stdout, stderr) => {
                                   if (error) {
                                    console.error('stderr: =============================', stderr);
                                    throw error;
                                    }
                                    fs.readFile('result_preview_HD_modified'+itemID+'.gif', function (err, data) {
                                    if (err) { throw err; }
                                    const s3Render = async () => {
                                         await s3.putObject({
                                           Body: data,
                                           Bucket: "consumerdatas",
                                           Key: 'uploaddata/'+itemID+'/HD_preview.gif',
                                           ACL: 'public-read'
                                       
                                       }, (err, data) => {
                                        if (err) {
                                        console.log(err);
                                        } else {
                                        console.log("Successfully uploaded data ");
                     
                                        var state_data = {$set:{hd_gif_url:keys.consumer_url + itemID +'/HD_preview.gif'}};
                                        var state_data_query = {item_buffer_id:itembufferid,render_type:'SD'};
                                        db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
                                        fs.unlink('result_preview_HD_modified'+itemID+'.gif', (err) => {
                                          if (err) {
                                             console.log("failed to delete local image:"+err);
                                           } else {
                                              console.log('successfully deleted local image');                                
                                               }
                                             });
                                           }
                                         });
                                        }
                     
                                         s3Render().catch(err => {
                                             console.log(err);
                                             res.status(500).json({
                                               error: err
                                             });
                                           });
                     
                                       });
                                       });
             
                                       /* Gif end */
             
            const mp3_child = exec('ffmpeg -i  result_preview_HD_'+itemID+'.mp4 -stream_loop -1 -i result_preview_HD_'+itemID+'.mp3 -map 0:v -map 1:a -c:v copy -shortest -af "apad,afade=type=out:start_time="'+endTime+'":duration=3" result_preview_HD_modified'+itemID+'.mp4', (error, stdout, stderr) => {
                     if (error) {
                         console.error('stderr: =============================', stderr);
                         throw error;
                     }
                           
          fs.readFile('result_preview_HD_modified'+itemID+'.mp4', function (err, data) {
             if (err) { throw err; }
                  const s3Render = async () => {
                   await s3.putObject({
                     Body: data,
                     Bucket: "consumerdatas",
                     Key: 'uploaddata/'+itemID+"/"+pro_name +"_"+comp_method+"_HD_FILE.mp4",
                     ACL: 'public-read',
                     ContentType: 'binary',
                     CacheControl: 'max-age=172800'
                    }, (err, data) => {
                      if (err) {
                        console.log(err);
                        } else {
                       console.log("Successfully uploaded data ");
                       var update_item_percentage_sql;
                       var render_finalized = Date.now();
                       module.exports.email_HD_render_created(itemID,user_id,keys.consumeer_hd_url + itemID+"/"+pro_name +"_"+ comp_method+"_HD_FILE.mp4",keys.consumer_url + itemID +'/HD_preview.gif')

                       var update_download_product = {$set:{finalized_status:1}};
                       var download_query = {order_item:ObjectId(itemID)};
                       db.get().collection("order_item_queue").updateOne(download_query,update_download_product, function(err, result_order_item) {});
                       if(port===5001 || port===5002){
                       db.get().collection("render_server_state").find(
                         { 
                             "port" :port
                         }                        
                       ).toArray( function(err, result_port_select) {
                       if (err){ throw err;}
                        var available_slots = result_port_select[0]['available_slots'] + 1;
                        var update_download_product = {$set:{available_slots:available_slots}};
                       var download_query = {port:port};

                       db.get().collection("render_server_state").updateOne(download_query,update_download_product, function(err, result_order_item) {});
                        });
                        }
                        var order_item_product = {$set:{render_percentage:100,final_upload_status:1,render_status:3,item_status:2,render_finalized,render_finalized,item_video_preview:keys.consumeer_hd_url + itemID+"/"+pro_name +"_"+ comp_method+"_HD_FILE.mp4"}};
                       var order_item_query = {_id:ObjectId(itemID)};
                      db.get().collection("order_item").updateOne(order_item_query,order_item_product, function(err, result_order_item) {});
                       var order_item_product = {$set:{hd_url:keys.consumeer_hd_url + itemID+"/"+pro_name +"_"+ comp_method+"_HD_FILE.mp4",hd_percentage:100}};
                       var order_item_query = {item_buffer_id:itembufferid,render_type:'SD'};
                       db.get().collection("order_item").updateOne(order_item_query,order_item_product, function(err, result_order_item) {});
                      }
                     });
                     }

                   s3Render().catch(err => {
                       console.log(err);
                       res.status(500).json({
                         error: err
                       });
                     });


                 });
               })  //=== end of mp3
              });
            });
          }


          exports.get_item_hd_render = (item_buffer_id,port,server_id,req, res, next) => {
            var render_server_state_collection = db.get().collection("render_server_state");
              var item_buffer_id = req.body.itemID;
              var text = [];
              var images=[];
              var color=[];
              var port,server_id;
              db.get().collection("order_item").find(
                { 
                    "item_buffer_id" :item_buffer_id
                }
            ).toArray( function(err, result_pro) {
              if (err){ throw err;}
               db.get().collection("product").find(
                        { 
                            "pro_buffer_id" :result_pro[0]['product_id']
                        }, 
                        { 
                            "category" : "$category", 
        
                        }
                    ).toArray( function(err, result_item_pro) {
                    if (err) {
                      return res.send();
                    }
  
                 var item_id = result_pro[0]['_id'];
                 var render_created = Date.now();
                 var order_item = {order_id:ObjectId(result_pro[0]['order_id']),
                                  item_buffer_id:result_pro[0]['item_buffer_id'],
                                  item_parent_id:ObjectId(item_id),
                                  user_id:ObjectId(result_pro[0]['user_id']),
                                  lead_id:ObjectId(result_pro[0]['lead_id']),
                                  previous_parent_item_id:ObjectId(0),
                                  product_id:result_pro[0]['product_id'],
                                  comp_id:ObjectId(result_pro[0]['comp_id']),
                                  render_created:render_created,
                                  render_finalized:'',
                                  render_updated:'',
                                  created_at:render_created,
                                  filePath:result_pro[0]['filePath'],
                                  item_image_preview:result_pro[0]['item_image_preview'],
                                  item_video_preview:result_pro[0]['item_video_preview'],
                                  audio_url:result_pro[0]['audio_url'],
                                  state_id:ObjectId(result_pro[0]['state_id']),
                                  audio_status:result_pro[0]['audio_status'],
                                  audio_image:result_pro[0]['audio_image'],
                                  audio_name:result_pro[0]['audio_name'],
                                  render_type:'HD',
                                  render_status:0,
                                  fileSize:0,
                                  hd_url:'',
                                  hd_mail_status:0,
                                  hd_status:0,
                                  hd_percentage:0,
                                  hd_download:0,
                                  revision_start:'',
                                  revision_end:'',
                                  revision:0,
                                  render_request:'',
                                  jobs_failure:0,
                                  render_queue:0,
                                  caption:'',
                                  item_status:0,
                                  final_upload_status:0,
                                  front_percentage:0,
                                  port_number:parseInt(port),
                                  created_at:render_created,
                                  updated_at:render_created,
                                  frame_set:0,
                                  render_1:0,
                                  render_2:0,
                                  render_3:0,
                                  render_4:0,
                                  render_5:0,
                                  render_request:result_pro[0]['render_request']};
                  db.get().collection("order_item").insertOne(order_item, function(err, result_order_item) {
                  if (err) throw err;
                  var itemID = result_order_item.insertedId;
  
                  var update_download_product = {$set:{hd_status:1}};
                   var download_query = {item_buffer_id:result_pro[0]['item_buffer_id'],render_type:'SD'};
                   db.get().collection("render_server_state").updateOne(download_query,update_download_product, function(err, result_comp_text) {});
                    
                   db.get().collection("order_item_comp").find(
                    { 
                        "item_id" : ObjectId(item_id)
                    }
                ).toArray( function(err, result_comp) {
                  if (err){ throw err;}
                    var ip = JSON.stringify(req.ip)
                    var order_item_tracking = {order_item:itemID,
                                              lead_id:result_pro[0]['lead_id'],
                                              user_id:result_pro[0]['user_id'],
                                              ip_address:ip,
                                              sd_download:0,
                                              render_created:render_created,
                                              hd_download:1};
                    db.get().collection("lead_user_download").insertOne(order_item_tracking, function(err, result_order_tracking_insert) {});
                    var order_item_comp = {item_id:ObjectId(itemID),
                                           comp_name:result_comp[0]['comp_name'],
                                           comp_method:result_comp[0]['comp_method'],
                                           original_width:result_comp[0]['original_width'],
                                           original_height:result_comp[0]['original_height'],
                                           preview_video:result_comp[0]['preview_video'],
                                           preview_image:result_comp[0]['preview_image'],
                                           preview_width:result_comp[0]['preview_width'],
                                           preview_heigh:result_comp[0]['preview_heigh'],
                                           key_time:result_comp[0]['key_time'],
                                           render_time:result_comp[0]['render_time']};
                    db.get().collection("order_item_comp").insertOne(order_item_comp, function(err, result_comp_comp_insert_order) {});
                    var comp_pixel = result_comp[0]['original_width'] + "*" +  result_comp[0]['original_height'] ;
                      
                    db.get().collection("order_item_image").find(
                      { 
                          "item_id" : ObjectId(item_id)
                      }
                    ).toArray( function(err, result_comp_image) {
                    if (err) throw err;
                    if(_.size(result_comp_image) > 0) {
                    let result_comp_image_length = result_comp_image.length;
                    for (let i =0; i < result_comp_image_length; i++){
                    let order_item_image = {item_id:ObjectId(itemID),
                                           name:result_comp_image[i]['name'],
                                           image_url:result_comp_image[i]['image_url'],
                                           width:result_comp_image[i]['width'],
                                           height:result_comp_image[i]['height'],
                                           nodename:result_comp_image[i]['nodename'],
                                           file_url:result_comp_image[i]['file_url'],
                                           required:result_comp_image[i]['required'],
                                           visible:result_comp_image[i]['visible'],
                                           keyFrame:result_comp_image[i]['keyFrame']};
                   db.get().collection("order_item_image").insertOne(order_item_image, function(err, result_comp_image_insert_order) {});
                   let image_data = {"nodeName": result_comp_image[i]['nodename'] ,"name":result_comp_image[i]['name'],"image_composition":  result_comp_image[i]['width'] + "*" + result_comp_image[i]['height'],"url": result_comp_image[i]['image_url'],visible:result_comp_image[i]['visible']};
                   images.push(image_data);
       
                   }
                   }
  
                   db.get().collection("order_item_text").find(
                    { 
                      "item_id" : ObjectId(item_id)
                    }
                  ).toArray( function(err, result_comp_text) {
                  if (err) throw err;
                  if(_.size(result_comp_text) > 0) {
                 let result_comp_text_length = result_comp_text.length;
                 for (let i =0; i < result_comp_text_length; i++){
                  var order_item_text = {item_id:ObjectId(itemID),
                                         name:result_comp_text[i]['name'],
                                         display_content:result_comp_text[i]['display_content'],
                                         color:result_comp_text[i]['color'],
                                         font_family:result_comp_text[i]['font_family'],
                                         key_frame:result_comp_text[i]['key_frame'],
                                         visible:result_comp_text[i]['visible'],
                                         maxCharacter:result_comp_text[i]['maxCharacter'],
                                         nodename:result_comp_text[i]['nodename'],
                                         exclude:result_comp_text[i]['exclude']};
                    var text_data = {"contents": result_comp_text[i]['display_content'],"color": result_comp_text[i]['color'],"font": result_comp_text[i]['font_family'],"nodeName": result_comp_text[i]['name']};
                    text.push(text_data);
                    db.get().collection("order_item_text").insertOne(order_item_text, function(err, result_comp_text_insert_order) {});
                    }
                    }
  
                    db.get().collection("order_item_colors").find(
                      { 
                          "item_id" : ObjectId(item_id)
                      }
                    ).toArray( function(err, result_comp_color) {
                    if (err) throw err;
                    if(_.size(result_comp_color) > 0) {
                    let result_comp_color_length = result_comp_color.length;
                     for (let i =0; i < result_comp_color_length; i++){
                     var order_item_color = {
                                            name:result_comp_color[i]['name'],
                                            limited_color:result_comp_color[i]['limited_color'],
                                            keyframe:result_comp_color[i]['keyframe'],
                                            color:result_comp_color[i]['color'],
                                            nodeName:result_comp_color[i]['nodeName'],
                                            item_id:ObjectId(itemID)};
                      db.get().collection("order_item_colors").insertOne(order_item_color, function(err, result_comp_color_insert_order) {});
                      var color_data = {"nodename":result_comp_color[i]['nodeName'],"color":result_comp_color[i]['color']};
                      color.push(color_data);
                       }
                       }
                          var scene = [{                               
                                      "fileName": result_pro[0]['filePath'],
                                      "fileDuration": result_comp[0]['key_time'],
                                      "render_time":result_comp[0]['render_time'],
                                      "compositions": result_comp[0]['comp_name'],
                                      "color_picker":color,
                                      "images":images,
                                      "text":text,
                                      "category":result_item_pro[0]['category'],
                                      "audio": {
                                        "nodeName": "Project_music.mp3",
                                        "url": result_pro[0]['audio_url'],
                                        "enabled":true
                                      },
                                      "original": {
                                        "nodeName": "Watermark.png",
                                        "outputModule": "HD Render",
                                        "renderSettings": "HD Render",
                                        "switch": "0",
                                        "outputExt": "0"
                                      }
                                    }];
                           var scene_length = scene.length;
                           
                           module.exports.get_hd_scene(scene,itemID,item_buffer_id,result_pro[0]['user_id'],comp_pixel,result_pro[0]['product_id'],port,server_id,res,next);
                          // 
                        });
                        });
                        });
                        });
                        });
                        });
                    
                        });
                      };

   exports.email_render_err = (item_id,user_id,content,port,product_url,react_url,req,res,next) => {
              db.get().collection("order_item").find(
                { 
                    "render_type" : "SD", 
                    "_id" : ObjectId(item_id),
                  
                }
               ).toArray( function(err, cron_data_select) { 
               if (err) throw err;
              //  if(_.size(cron_data_select)>0){
              //       req.body.cron_data = cron_data_select[0]['render_request'];
              //       req.body.item_id = cron_data_select[0]['item_buffer_id'];
              //       module.exports.update_cron_render(req,res,next);
              //      }

                // db.get().collection("render_server_state").find(
                //     { 
                //         "port" : port
                //     }, 
                //     { 
                //         "available_slots" : "$available_slots", 
    
                //     }
                //  ).toArray( function(err, result_port_select) {
                //  var available_slots = result_port_select[0]['available_slots'] + 1;

                 var state_data = {$set:{render_status:4}};
                 var state_data_query = { 
                  "render_type" : "SD", 
                  "_id" : ObjectId(item_id),
                
              };
                 db.get().collection("order_item").updateOne(state_data_query,state_data, function(err, result_order_item) {});
                  

                  db.get().collection("users").find(
                    { 
                        "_id" :ObjectId(user_id)
                    }
                ).toArray( function(err, result_user_select) {
                   if (err) throw err;
                   let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });
   


                    // var smtpTransport = nodemailer.createTransport(mg(auth));

                    //   var mailOptions = {
                    //   to:result_user_select[0]['email'],
                    //   from: 'admin@onemaker.io',
                    //   subject: 'Render Stopped',
                    //   text: 'Hello,\n\n' +
                    //   'Your render is Stopped Due to some technical issue'
                    //    };
                    //  smtpTransport.sendMail(mailOptions, function(err) {
                    //  });
                    var pro_url = "https://staging.onemaker.io/adminpanel/apps/user/user-view/"+product_url;
                    var react_ur = "https://staging.onemaker.io/i/editor/product/"+react_url; 

                      var mailOptions = {
                      to:'vivekrana46@gmail.com',
                      from: 'admin@onemaker.io',
                      subject: 'SD Render Stopped' +item_id,
                      text: 'Hello,\n\n' +
                     'Your render is Stopped Item ID:'+item_id+'due to some render issues of user email id = '+ result_user_select[0]['email'] + ', Project Uploader Url : '+pro_url +' ,Project Live URL :'+react_ur
                     };
                     smtpTransport.sendMail(mailOptions, function(err) {
                      console.log(err);
                     });
                    });
                  });
                  }
