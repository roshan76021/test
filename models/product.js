var db = require('../mdbConnection')
var _ = require('underscore');
var keys = require('../config/keys');
const fs = require('fs');
var fsc = require("fs-extra");
var FormData = require('form-data');
var moment =  require('moment');
const fetch = require('node-fetch');
var request = require('request');
var ObjectId = require('mongodb').ObjectID;
const exec = require('child_process').exec;
const imagemin = require("imagemin");
var potrace = require('potrace')
const imageminPngquant = require("imagemin-pngquant");
const AWS = require('aws-sdk');
const wasabiEndpoint = new AWS.Endpoint('s3.us-west-1.wasabisys.com');
const rimraf = require('rimraf');
const upload_url = "https://s3.us-west-1.wasabisys.com/projectsfiles/uploaddata/";
const upload_image_url = "https://projectdatas.s3.amazonaws.com/uploaddata/";
const sharp = require('sharp')
var sNode3 = require('s3-node-client');
let jsonData = require('./../render.json');
let groupData = require('./../group_product.json');
const consumer_url = "https://s3.us-west-1.wasabisys.com/consumerdata/";
const getMP3Duration = require('get-mp3-duration')
const cron = require('node-cron');
const s3= new AWS.S3({
      accessKeyId:keys.AWSaccessKeyId,
      secretAccessKey:keys.AWSsecretAccessKey
 });
const path = require('path');
const fstream = require('fstream');
const { object } = require('underscore');
const { ObjectID } = require('mongodb');

  /* get category API start*/
  exports.getcategory = (req, res, next) => {
    var collection = db.get().collection("product_category");
    collection.find({status:{$in:[1,3]}}).sort({'slug':1}).collation( { locale: "en_US", numericOrdering: true }).toArray( function(err, result) {
      if (err) throw err;
      res.status(200).json({ "status": "sucess","category":result});
    });
  };


    // function splitToNumbers(str) {
    //         str = '0' + str;
    //         var timeArray = str.match(str.length  % 2 ? /^\d|\d{2}/g : /\d{2}/g).map(Number)
    //         var hourFrame =  timeArray[0] * 108000;
    //         var minutesFrame =  timeArray[1] * 1800;
    //         var secFrame =  timeArray[2] * 30;
    //         var frame = timeArray[3];
    //         var totalFrame = hourFrame + minutesFrame + secFrame + frame;
    //         return totalFrame
    //     } 



   function splitToNumbers(currentFrame) {
    var fps = 30;
    var h = Math.floor(currentFrame/(60*60*fps));
    var m = (Math.floor(currentFrame/(60*fps))) % 60;
    var s = (Math.floor(currentFrame/fps)) % 60;
    var f = currentFrame % fps;
    return showTwoDigits(h) + showTwoDigits(m)+ showTwoDigits(s) +  showTwoDigits(f); 
    }


function showTwoDigits(number) {
    return ("00" + number).slice(-2);
}



        function datediff(first, second) {
         // Take the difference between the dates and divide by milliseconds per day.
         // Round to nearest whole number to deal with DST.
            return Math.round((second-first)/(1000*60*60*24));
        }


function getISTTime(time){
  let d = new Date(time)
  return d.getTime() + ( 5.5 * 60 * 60 * 1000 )
}
  

  /* get category API END*/
  exports.taglist = (req, res, next) => {
    var collection = db.get().collection("tags");
    collection.find().sort({'tag_slug':1}).collation( { locale: "en_US", numericOrdering: true }).toArray( function(err, result) {
      if (err) throw err;
      res.status(200).json({ "status": "sucess","tags":result});
    });
  };

/* get category nameAPI Start*/
  exports.getprocategory = (req, res) => {
  var cat_id = req.query.cat_id;
  var collection = db.get().collection("product_category");
  collection.find({'_id':ObjectId(cat_id)}).toArray( function(err, result) {
  if (err) throw err;
  res.status(200).json({ "status": "200","cat_name":result[0]['slug']});     
    });
}
  /* get category name API END*/



  /* Delete group product  */


  exports.deletegroupproducts = (req, res) => {
  var group_id = req.body.group_id;
  var collection = db.get().collection("category_slider");
  var slider_product = db.get().collection("slider_product");
  var group_del_query = {'_id':ObjectId(group_id)};
  collection.deleteOne(group_del_query, function(err, result_del_tag) {
  if (err) throw err;
  console.log(err)
    var product_del_query = {'group_id':ObjectId(group_id)};
    slider_product.deleteMany(product_del_query, function(err, result_del_tag) {
    if (err) throw err;

  res.status(200).json({ "status": "200"});     
    });
  })
}




  exports.deletesliderproducts = (req, res) => {
  var group_id = req.body.group_id;
  var collection = db.get().collection("sliders");
  var slider_product = db.get().collection("sliders_url");
  var group_del_query = {'_id':ObjectId(group_id)};
  collection.deleteOne(group_del_query, function(err, result_del_tag) {
  if (err) throw err;
 
    var product_del_query = {'group_id':ObjectId(group_id)};
    slider_product.deleteMany(product_del_query, function(err, result_del_tag) {
    if (err) throw err;

  res.status(200).json({ "status": "200"});     
    });
  })
}

/* End group oriduct delte */

  /* Get productrs Start */
  exports.getProducts = (req, res, next) => {
    var category_id = req.query.cat_id;
    var sub_category_id = req.query.sub_cat_id;
    var collection = db.get().collection("product");
    collection.find({'category':parseInt(category_id), 'status':1,'subcategory':parseInt(sub_category_id)}).toArray( function(err, result) {
    res.status(200).json({ "status": "sucess","product":result});
    });
  };

/* Get productrs End */


    /* Get users*/


      exports.users_details = (req, res, next) => {
        var user_id = req.query.user_id;
        var collection = db.get().collection("users");
        var order_collection = db.get().collection("order");
          if(user_id === '' || user_id === undefined || user_id === null){
            collection.find().toArray( function(err, result) {
              var users = [];
              if(_.size(result)>0){
                result.forEach(function(elementss,j) {
                  let user_id = elementss._id;
                  let subscription = "free";
                  order_collection.find({'user_id':ObjectId(user_id),'order_type':{ 
                "$in" : [
                    1, 
                    2, 
                    3, 
                    5,
                    9
                ]
            }}).toArray( function(err, order_result) {
                  if(_.size(order_result)>0){
                    if(order_result[0]['lifetime_status']===1){
                      subscription = "Life Time";
                    } else if(order_result[0]['lifetime_status']===0 && order_result[0]['order_type']===3){
                        subscription = "Yearly User";
                    } else if(order_result[0]['lifetime_status']===0 && order_result[0]['order_type']===9){
                        subscription = "15 days Trial";
                    } else {
                       subscription = "Monthly User";
                    }

                  }

                  var today = new Date();
                  let subscription_expiry = new Date(order_result[0]['subscription_end']);
                  var remaining_days  = parseInt(datediff(today,subscription_expiry));
                  if(remaining_days < 0){
                    remaining_days = 0;
                  }
                
             

                 var user_array =  {
                                  display_name: elementss.display_name,
                                  _id:user_id,
                                  email: elementss.email,
                                  profile_pic: elementss.profile_pic,
                                  social_id: elementss.social_id,
                                  social_provider: elementss.social_provider,
                                  token:elementss.token,
                                  username: elementss.username,
                                  usr_created:elementss.loginTime,
                                  subscription:subscription,
                                  remaining_days:remaining_days,
                                  id:j+1
                  }
                  users.push(user_array);
                  if(j===result.length-1){
                    res.status(200).json({ "status": "sucess","users":users});
                  }   
 
                  });                  
               });
              } else {
                res.status(200).json({ "status": "sucess","users":[]});
              }

            
            });
          } else {
           collection.find({"_id":ObjectId(user_id)}).toArray( function(err, result) {
           let subscription = "free";
           order_collection.find({'user_id':ObjectId(user_id),'order_type':{ 
                "$in" : [
                    1, 
                    2, 
                    3, 
                    5,
                    9
                ]
            }}).toArray( function(err, order_result) {
                  if(_.size(order_result)>0){
                    if(order_result[0]['lifetime_status']===1){
                      subscription = "Life Time";
                    } else if(order_result[0]['lifetime_status']===0 && order_result[0]['order_type']===3){
                        subscription = "Yearly User";
                    } else if(order_result[0]['lifetime_status']===0 && order_result[0]['order_type']===9){
                        subscription = "15 days Trial";
                    } else {
                       subscription = "Monthly User";
                    }

                  }

               res.status(200).json({ "status": "sucess","users":result,subscription:subscription});
              })
            
              });
          }
       }
     /* */



  /* GET Category Slider Start */
  exports.getCategorySlider = (req, res, next) => {
  var items = [];
  var slider = [];
  var tag_name =[];
  var product_id = req.query.product_id;
if(product_id === '' || product_id === undefined || product_id === null){
    res.status(200).json({ "status": "sucess","comment":"sucess","slider_data":jsonData});
    } else {
    res.status(200).json({ "status": "sucess","comment":"sucess","slider_data":jsonData});
    }
  }

/* GET Category Slider END*/
 /* Add Image on Project with audio file */  
function getFileExtension3(filename) {
   return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
  }

  exports.project_upload_image = (req, res) => {
      var file = 'project_uploads' + '/' + req.file.originalname;
      var url_name ='prefix_'+ (new Date().getTime()).toString(36)+"_t.mp3";
      var png_name ='prefix_'+ (new Date().getTime()).toString(36)+".png";
      var svg_name ='prefix_'+ (new Date().getTime()).toString(36)+".svg";
      var test_number = (new Date().getTime()).toString(36);
      var file = 'project_uploads' + '/' + url_name;
      var random_number = req.body.random_number;
      var file_name = req.body.file_name;
      var fineName = req.file.originalname
      var extpart = getFileExtension3(fineName);
      var duration;
      fs.rename(req.file.path, file, function(err) {
      if (err) {
        res.status(500);
        } else {
        fs.readFile('project_uploads' + '/' + url_name, function (err, data) {
        if (err) { throw err; }
        if(extpart === "mp3"){
        var duration_temp = Math.floor(getMP3Duration(data)/1000)
        duration = Math.floor(duration_temp * 30);
        console.log(duration)
        duration= splitToNumbers(duration);
        duration = duration.substring(1);

        } else {
        duration = 0;
        }
        const s3Render = async () => {
        await s3.putObject({
              Body: data,
              Bucket: "projectsfiless",
              Key: 'uploaddata/'+random_number+'/audio/'+url_name,
              ACL: 'public-read',
              CacheControl: 'public, max-age=50',
              },(err, data) => {
              if (err) {
              console.log(err);
              } else {

            fs.readFile('project_uploads' + '/' + url_name, function (err, data) {
             if (err) { throw err; }
             const child = exec('ffmpeg -i ./project_uploads/'+ url_name+' -lavfi showwavespic=s=180x10:colors=B8B8B8 ./project_uploads/'+png_name, (error, stdout, stderr) => {
                if (error) {
                   console.error('stderr: =============================', stderr);
                    throw error;
                   }
                 console.log('stdout: ==========================', stdout);
            potrace.posterize('./project_uploads/'+png_name,{ steps: [40, 85, 135, 180] }, function(err, svg) {
           if (err) throw err;
           fs.writeFile('./project_uploads/'+svg_name, svg, function (err,data) {
           if (err) {
           return console.log(err);
           }
           fs.readFile('./project_uploads/'+svg_name, function (err, data) {
           if (err) { throw err; }
           const s3Render = async () => {
                  await s3.putObject({
                  Body: data,
                  Bucket: "projectsfiless",
                  Key: 'uploaddata/'+random_number+'/audio/'+test_number+'/'+svg_name,
                  ACL: 'public-read',
                  CacheControl: 'public, max-age=50',
                    }
                    , (err, data) => {
                      if (err) {
                         console.log(err);
                      } else {
                        console.log("Successfully uploaded data ");
          
                        res.status(200).json({ "status": "data found","upload_url": keys.upload_url + random_number +'/audio/'+url_name,"nodeName":req.body.nodeName,"duration":duration,"waveForm_url":keys.upload_url + random_number +'/audio/'+test_number+'/'+svg_name,"nodeName":req.body.nodeName});
                        fs.unlink('project_uploads' + '/' + req.file.originalname, (err) => {
                          if (err) {
                              console.log("failed to delete local mp3:"+err);
                          } else {
                          fs.unlink('project_uploads' + '/' + file_name+'.png', (err) => {
                          if (err) {
                              console.log("failed to delete local image:"+err);
                          } else {

                              fs.unlink('project_uploads' + '/' + req.file.originalname, (err) => {
                              if (err) {
                              console.log("failed to delete local image:"+err);
                              } else {
             
                              console.log('successfully deleted local image');  
                              }
                              });                              
                           }
                           });
                                                   
                          }
                        });
                      }
                    });
                   }

                    s3Render().catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });
                  });
                });
                });
                 });
                });
                }
              });
              }
             s3Render().catch(err => {
                   console.log(err);
                    res.status(500).json({
                    error: err
                    });
                    });
                   });
                   }
                 });
                }

        /* Add Image on Project with audio file END */  

       /* Add image crop*/
        exports.project_image_crop = (req, res) => {
        var file_url = req.body.file_url;
        var file_name = req.body.file_name;
        var random_number = req.body.random_number;
        var now = Date.now();
        var download = function(uri, filename, callback){
        request.head(uri, function(err, res, body){
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
         });
        };
        download(file_url,'project_uploads' + '/' + random_number+ '_' + file_name+'.jpeg', function(){
        sharp.cache(false);
        sharp('project_uploads' + '/' + random_number+ '_' + file_name+'.jpeg').png({  compressionLevel: 9, adaptiveFiltering: true, progressive:true, force: false ,quality:50 }).withMetadata().resize(parseInt(req.body.width),parseInt(req.body.height)).toFile("./project_uploads/" + random_number + "_" + file_name +".png").then((ImageResult) => {
        fs.readFile("./project_uploads/" + random_number+ "_"+ file_name +".png", function (err, data) {
        if (err) { throw err; }
           (async () => {
            const files = await imagemin(["./project_uploads/" + random_number+ "_"+ file_name +".png"], {
            destination: "./project_uploads/compress",
            plugins: [
            imageminPngquant({
            quality: [0.5, 0.6]
            })
            ]
           });

         fs.readFile("./project_uploads/compress/" + random_number+ "_"+ file_name +".png", function (err, data) {
         const s3Render = async () => {
                await s3.putObject({
                 Body: data,
                 Bucket: "projectsfiless",
                 Key: 'uploaddata/'+random_number+'/image/'+now+'/'+file_name +".png",
                 ACL: 'public-read',
                 CacheControl: 'public, max-age=50',
                 },(err, data) => {
                 if (err) {
                 console.log(err);
                 } else {
                 fs.unlink('project_uploads' + '/'  + random_number+ '_' + file_name+'.jpeg', (err) => {
                 if (err) {
                 console.log("failed to delete local image:"+err);
                 } else {
                 fs.unlink("./project_uploads/compress/" + random_number+ "_"+ file_name +".png", (err) => {
                 if (err) {
                 console.log("failed to delete local image:"+err);
                 } else {
                 fs.unlink("./project_uploads/" + random_number+ "_"+ file_name +".png", (err) => {
                 if (err) {
                 console.log("failed to delete local image:"+err);
                 } else {
                 res.status(200).json({ "status": "data found","upload_url": keys.upload_url +random_number +'/image/'+now+'/'+file_name +".png"});
                 }
                 });
                }
                });
                }
                });
                }
                });
                }
                s3Render().catch(err => {
                console.log(err);
                res.status(500).json({
                 error: err
                 });
                 });
                 })
                 })();
                 });  
                 }); 
                 });
                 }
             /* end image crop*/

     /* Add upload project ZIP file*/
    exports.project_upload_zip = (req, res) => {
      var filezize = req.file.size
      req.setTimeout(18000000);
      if (filezize > 50000000){
      res.status(201).json({ "status": "file size exceed from 50MB","upload_status": "1"});  
      return false;
      } else {
      var rnNumber = req.body.random_number;
      var readStream = fs.createReadStream('tmp/Project_controller_' + rnNumber+ '_upload.zip');
      var form = new FormData();
       form.append('file', readStream);
       form.append('rnNumber', rnNumber);
      fetch('http://3.237.3.243:3000/users/', { method: 'POST', body: form })
      .then(response => response.json())
      .then(data => {
      res.status(201).json({ "status": "file size exceed from 50MB","upload_status": "2"});
      if(data.status === "success"){
      res.status(201).json({ "status": "file size exceed from 50MB","upload_status": "2"});
      fs.unlink('tmp/Project_controller_' + rnNumber+ '_upload.zip', (err) => {
      if (err) {
       console.log("failed to delete local image:"+err);
        } else {
       res.status(200).json({ "status": "Project Files upload sucessfully","upload_url": rnNumber+".Project_controller","upload_status":"3"});           
        }
        });
        } else {
        res.status(201).json({ "status": "file size exceed from 50MB","upload_status": "2"});
         }
        })
      }
  }
     /* END upload project ZIP file*/

   /* Add final Approved */

   exports.finalApproved = (req, res) => {
   var product_id = req.body.product_id;
   var price = req.body.price;
   var producer_id = req.body.producer_id;
   var producer_product_id = req.body.producer_project
   var render_time = req.body.final_render_time;
   if(render_time < 0){
    render_time = 0;
   }
   var collection = db.get().collection("product");
   var myquery = { _id: ObjectID(product_id)};
   var newvalues = { $set: {status: 1,render_time:parseInt(render_time)}};
   var tag_collection = db.get().collection("product_tags");
   var tag_query = { product_id: product_id.toString()};
   var tag_newvalues = { $set: {status: 1}};
   tag_collection.updateOne(tag_query,tag_newvalues, function(err, result) {});
   collection.updateOne(myquery,newvalues, function(err, result) {
    if (err) throw err;
    if(producer_product_id===null || producer_product_id === "" || producer_product_id === undefined){
       res.status(200).json({ "status": "200"});   
    } else {
    var relase_query = { _id: ObjectId(producer_product_id)};
    var project_collection = db.get(relase_query).collection("freelancer_projects");
    project_collection.find().toArray( function(err, result_projectss) {
     if (err) throw err;
     if(_.size(result_projectss)>0){
     var relase_newvalues = { $set: {status: 2}};
     project_collection.updateOne(relase_query,relase_newvalues, function(err, result) {
     if (err) throw err;
    var user_query = { _id: ObjectId(producer_id)};
    var pu_collection = db.get(relase_query).collection("freelancer_users");
    pu_collection.find().toArray( function(err, result_user) {
     if(_.size(result_user)>0){
      var userEmail = result_user[0]['email'];
                    let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });


        var mailOptions = {
        to:userEmail,
        from: 'no-reply@onemaker.io',
        subject: 'Hey '+result_user[0]['profileName']+', your template '+result_projectss[0]['name']+' has been release.',
        html: message
        };
       smtpTransport.sendMail(mailOptions, function(err) {
         res.status(200).json({ "status": "200"});  
        });
               
       } else {
      res.status(200).json({ "status": "200"});   
      }
      })
      })       
     } else {
      res.status(200).json({ "status": "200"});      
      }
      })
      }
    });
    }

   /* END final Approved */

    /* update render time add */ 
    exports.updaterendertime = (req, res) => {
    var product_id = req.body.product_id;
    var render_time = req.body.render_time;
    var collection = db.get().collection("prod_comp");
    var myquery = { pro_id: parseInt(product_id)};
    var newvalues = { $set: {render_time: parseInt(render_time)} };
    collection.updateOne(myquery,newvalues, function(err, result) {
    if (err) throw err;
     res.status(200).json({ "status": "200"});                           
     });
    }
    /* update render time END */ 

    /* GET font Family Add*/
    exports.fontFamily = (req, res) => {
    var collection = db.get().collection("fonts");
    collection.find().toArray( function(err, result_fonts) {
    if (err) throw err;
     res.status(200).json({ "status": "200","fonts":result_fonts});     
     });
    }
   /* GET font Family END*/
   
   /* update project */
   exports.update_project = (req, res, next) => {
    var product_id = req.body.product_id;
    var prod_info = req.body;
    var image_data = req.body.comp_data[0]['images'];
    var image_length = image_data.length;
    var text_data = req.body.comp_data[0]['text'];
    var color_picker = req.body.comp_data[0]['color'];
    var color_length = color_picker.length;
    var text_length = text_data.length;
    var subcategory = prod_info.subcategory;

   if(product_id === "" || product_id === "undefined" || product_id === null){
     res.status(401).json({ "status": "error","comment":"product not found","product_details": []});
     } else {

    var pro_collection = db.get().collection("product");
     pro_collection.find({"_id":ObjectId(product_id)}).toArray( function(err, result_pro) {
     if (err) throw err;
     if(_.size(result_pro) > 0) {
      var pro_query={ _id: ObjectId(product_id)};
      var version = prod_info.version + 0.1;

      var update_product = { $set: {pro_name: prod_info.pro_name,
                                    pro_price:parseInt(prod_info.pro_price),
                                    category:prod_info.category,
                                    producer_name:prod_info.producer_name,
                                    producer_fname:prod_info.producer_fname,
                                    producer_status:prod_info.producer_status,
                                    filePath:prod_info.filePath,
                                    default_audio:prod_info.default_audio,
                                     default_audio_url:prod_info.default_audio_url,
                                    image_frame:prod_info.image_frame,
                                    producer_p_id:prod_info.producer_p_id,
                                    status:0,
                                    version:version,
                                    image_render_time:0,
                                    filename:prod_info.filename,
                                    updated_at:getDateTime()}};
      pro_collection.updateOne(pro_query,update_product, function(err, result) {
      if (err) throw err;
      });

      var sub_cat_collection = db.get().collection("product_subcategory");
      var sub_del_query = { product_id: product_id };
      sub_cat_collection.deleteMany(sub_del_query, function(err, result_del_tag) {
        if (err) throw err;
        
      if(_.size(subcategory) > 0) {
      for(var i=0;i<subcategory.length;i++){
        var sub_cat_data = {product_id:product_id.toString(), 
                             subcategory_id: subcategory[i],
                             status: 1}
        sub_cat_collection.insertOne(sub_cat_data, function(err, insert_sub) {
        });
        }
        }
      });

       var tag_data = req.body.tag_id;
       var tag_collection = db.get().collection("product_tags");
       var tag_query = { product_id: product_id};
       tag_collection.deleteMany(tag_query, function(err, result_del_tag) {
       if (err) throw err;
       if(_.size(tag_data) > 0) {
       for(var i = 0;i<tag_data.length;i++){
        var tags_data = { product_id:product_id.toString(), 
                          tag_id: tag_data[i],
                          status:0}
        tag_collection.insertOne(tags_data, function(err, insert_tag) {
          });
          }
          }
         });

          var image_collection = db.get().collection("prod_images");
          var images_query = {product_id: ObjectId(product_id)};
          image_collection.deleteMany(images_query, function(err, result_del_color) {
          if (err) throw err;
          var text_collection = db.get().collection("prod_text");
          var text_query = {product_id: ObjectId(product_id)};
          text_collection.deleteMany(text_query, function(err, result_del_color) {
          if (err) throw err;
          var color_collection = db.get().collection("prod_colors");
          color_collection.deleteMany(text_query, function(err, result_del_color) {
         if (err) throw err;
          var pro_comp_collection = db.get().collection("prod_comp");
          pro_comp_collection.find({"prod_id":ObjectId(product_id)}).toArray( function(err, result_comp) {
          if (err) throw err;
          let comp_length = result_comp.length;

         if(image_length > 0){
          let transparent = "false";
          for(var i =0;  i < image_length; i++){
            if(image_data[i]['name'] === "Logo 1"){
              transparent = "true";
            }

           var image_query = {product_id: product_id,nodename:image_data[i]['nodename']};
           var image_data_insert = {product_id: ObjectId(product_id),
                                      name:image_data[i]['name'],
                                      image_url: image_data[i]['image_url'],
                                      width:parseInt(image_data[i]['width']),
                                      height: parseInt(image_data[i]['height']),
                                      nodename: image_data[i]['nodename'],
                                      preview_width: parseInt(image_data[i]['preview_width']),
                                      preview_height: parseInt(image_data[i]['preview_height']),
                                      file_url: image_data[i]['file_url'],
                                      required: image_data[i]['required'],
                                      visible: image_data[i]['visible'],
                                      keyframe: image_data[i]['keyframe'],
                                      transparent:transparent,
                                      cropper_width:0,
                                      cropper_height:0,
                                      cropper_url:'',
                                      original_cropper_url:'',
                                      scale:'',
                                      x:'',
                                      y:'',                                                
                                      status:0};
          image_collection.insertOne(image_data_insert, function(err, insert_images) {
          if (err) throw err;
           });
           }
          }


          if(color_length > 0){
               for(var k = 0; k < color_length;k++){
               var color_data_insert = {product_id: ObjectId(product_id),
                                     name:color_picker[k]['name'],
                                     color:color_picker[k]['color'],
                                     limited_color:color_picker[k]['limited_color'],
                                     nodeName:color_picker[k]['nodeName'],
                                     colorpick:color_picker[k]['colorpick'],
                                     keyframe:color_picker[k]['keyframe']};
             color_collection.insertOne(color_data_insert, function(err, update_color) {
             });
             }
             }


             
             if(text_length > 0){
              for(var l =0; l < text_length;l++){
               var pro_text_collection = db.get().collection("prod_text");                    
               var text_data_insert = {product_id: ObjectId(product_id),
                                       name:text_data[l]['name'],
                                       display_content: text_data[l]['display_content'],
                                       color:text_data[l]['color'],
                                       font_family: text_data[l]['font_family'],
                                       key_frame: text_data[l]['key_frame'],
                                       visible:text_data[l]['visible'],
                                       maxcharacter: parseInt(text_data[l]['maxcharacter']),
                                       nodename: text_data[l]['nodename'],
                                       visible: text_data[l]['exclude'],
                                       layer_number: parseInt(text_data[l]['layer_number']),
                                       max_lines:parseInt(text_data[l]['maxlines']),
                                       allow_multiline:0,
                                       capitalization:0,
                                       colorpick:text_data[l]['colorpick'],
                                       status:0};
              pro_text_collection.insertOne(text_data_insert, function(err, insert_text) {                                                                             
              if (err) throw err;
              });
              }
             }
             



             

         if(_.size(result_comp) > 0) {
         result_comp.forEach(function(elementss,j) {
         var update_product_comp = {$set:{key_time:prod_info['comp_data'][j]['key_time'],
                                   render_time:prod_info['comp_data'][j]['render_time'],
                                   status:parseInt(prod_info['comp_data'][j]['status']),
                                   preview_image:prod_info['comp_data'][j]['preview_image'],
                                   preview_video:prod_info['comp_data'][j]['preview_video'],
                                   original_width:parseInt(prod_info['comp_data'][j]['original_width']),
                                          original_height:parseInt(prod_info['comp_data'][j]['original_height']),
                                          preview_width:parseInt(prod_info['comp_data'][j]['preview_width']),
                                          preview_heigh:parseInt(prod_info['comp_data'][j]['preview_heigh']),
                                          key_time:prod_info['comp_data'][j]['key_time'],
                                          render_start_time:0,
                                          port_number:0,
                                          render_time: 0,
                                           nexrender_status:"",
                                          status:parseInt(prod_info['comp_data'][j]['status']),
                                          preview_image:prod_info['comp_data'][j]['preview_image'],
                                          preview_video:prod_info['comp_data'][j]['preview_video'],
                                          priority:parseInt(prod_info['comp_data'][j]['priority']),
                                           render_end_time:0,
                                          aftereffect_url:"",
                                          final_upload_url:"",
                                          final_aftereffect:"",
                                          fileUpload:0,
                                          uploadPercentage:0,
                                          render_finalized:0,
                                          render_status:0}};
                                         
         var pro_comp_query = {prod_id:ObjectId(product_id),comp_method:prod_info['comp_data'][j]['comp_method']}
         pro_comp_collection.updateOne(pro_comp_query,update_product_comp, function(err, result) {
         if (err) throw err;
          if(j === 3){
           res.status(200).json({ "status": "sucess"});
            } else {
            }
            });
            });
          
            } else {
            res.status(401).json({ "status": "error","comment":"product not found","product_details": []});
            }
            });
             })
            });
            });
            } else {
            res.status(401).json({ "status": "error","comment":"product not found","product_details": []});
            }
           });
          }
         };

         /* End of Update Project*/

        /* Add Upload project */
          exports.add_upload_project = (req, res, next) => {
           var prod_info = req.body;
           var product_result = [];
           var product_id;
           var image_data = req.body.comp_data[0]['images'];
           var image_length = image_data.length;
           var text_data = req.body.comp_data[0]['text'];
           var color_picker = req.body.comp_data[0]['color'];
           var tag_data = req.body.tag_id;
           var color_length = color_picker.length;
           var text_length = text_data.length;
           var result_comp = req.body.comp_data;
           var subcategory = prod_info.subcategory
           var pro_collection = db.get().collection("product");
           var tags_collection = db.get().collection("tags"); 
           var product_tags_collection = db.get().collection("product_tags");
           var insert_product = {pro_name:prod_info.pro_name,
                                  pro_price:prod_info.pro_price,
                                  pro_buffer_id:prod_info.product_buffer_id,
                                  producer_name:prod_info.producer_name,
                                  producer_fname:prod_info.producer_fname,
                                  producer_status:prod_info.producer_status,
                                  sd_download:0,
                                  wp_status:prod_info.wp_status,
                                  producer_p_id:prod_info.producer_p_id,
                                  random_number:prod_info.random_number,
                                  category:prod_info.category,
                                  filePath:prod_info.filePath,
                                  render_time:0,
                                  default_audio:prod_info.default_audio,
                                  default_audio_url:prod_info.default_audio_url,
                                  image_frame:prod_info.image_frame,
                                  status:0,
                                  product_preview:'',
                                  filename:prod_info.filename,
                                  version:0.1,
                                  image_render_time:0,
                                  created_at:getDateTime(),
                                  updated_at:getDateTime()};

             pro_collection.insertOne(insert_product, function(err, result) {
             if (err) throw err;
             product_id = result.insertedId;
             
             var prod_name_array = prod_info.pro_name.trim().split(" ");
            for(let u=0;u<prod_name_array.length;u++){
            let product_tag_name = prod_name_array[u];
            let tag_query = {"tag_slug":product_tag_name.toString()};
            tags_collection.find(tag_query).toArray(function(err, result_select_tag) {
             if (err) throw err;           
             if(_.size(result_select_tag)> 0){
                tag_sid = result_select_tag[0]['_id'];
                var tags_pro_data = {product_id:product_id.toString(), 
                                      tag_id:tag_sid.toString(), 
                                      status:1}    
                product_tags_collection.insertOne(tags_pro_data, function(err, insert_pro_tag) {
                 });
                 } else {
                let tags_data = {tag_slug:product_tag_name,
                                  tag_name:product_tag_name}
                                  console.log(tags_data)
                tags_collection.insertOne(tags_data, function(err, insert_tags) {
                let tag_sid = insert_tags.insertedId;
                let tags_pro_data = {product_id: product_id.toString(),
                                     tag_id: tag_sid.toString(), 
                                     status: 1}
                product_tags_collection.insertOne(tags_pro_data, function(err, insert_pro_tag) {
                 });
                 });
                 }
                 });
                }


               if(_.size(subcategory) > 0) {
               for(var i=0;i<subcategory.length;i++){
                var product_subcat_collection = db.get().collection("product_subcategory");
               var subcat_data = {product_id: product_id.toString(),
                                  subcategory_id: subcategory[i],
                                  status: 1}
                product_subcat_collection.insertOne(subcat_data, function(err, insert_subcat) {
                });
                }
               }
              
               if(_.size(tag_data) > 0) {
                for(var i=0;i<tag_data.length;i++){
                var tags_data = {product_id:product_id.toString(), 
                                tag_id: tag_data[i],
                                status: 1}
                product_tags_collection.insertOne(tags_data, function(err, insert_tag) {
                });
                }
               }

               if(text_length > 0){
                for(var l =0; l < text_length;l++){
                 var pro_text_collection = db.get().collection("prod_text");                    
                 var text_data_insert = {product_id: product_id,
                                         name:text_data[l]['name'],
                                         display_content: text_data[l]['display_content'],
                                         color:text_data[l]['color'],
                                         font_family: text_data[l]['font_family'],
                                         key_frame: text_data[l]['key_frame'],
                                         visible:text_data[l]['visible'],
                                         maxcharacter: parseInt(text_data[l]['maxcharacter']),
                                         nodename: text_data[l]['nodename'],
                                         visible: text_data[l]['exclude'],
                                         layer_number: parseInt(text_data[l]['layer_number']),
                                         max_lines:parseInt(text_data[l]['maxlines']),
                                         allow_multiline:0,
                                         colorpick:text_data[l]['colorpick'],
                                         capitalization:0,
                                         status:0};
                pro_text_collection.insertOne(text_data_insert, function(err, insert_text) {                                                                             
                if (err) throw err;
                });
                }
               }
               
               if(image_length > 0){
                let transparent = "false";
                 for(var i =0;  i < image_length; i++){
                  if(image_data[i]['name'] === "Logo 1"){
                    transparent = "true";
                  }
                 var image_collection = db.get().collection("prod_images");            
                 var image_data_insert = {product_id: product_id,
                                            name:image_data[i]['name'],
                                            image_url: image_data[i]['image_url'],
                                            width:parseInt(image_data[i]['width']),
                                            height: parseInt(image_data[i]['height']),
                                            nodename: image_data[i]['nodename'],
                                            preview_width: parseInt(image_data[i]['preview_width']),
                                            preview_height: parseInt(image_data[i]['preview_height']),
                                            file_url: image_data[i]['file_url'],
                                            required: image_data[i]['required'],
                                            visible: image_data[i]['visible'],
                                            keyframe: image_data[i]['keyframe'],
                                            cropper_width:0,
                                            cropper_height:0,
                                            cropper_url:'',
                                            original_cropper_url:'',
                                            transparent:transparent,
                                            scale:'',
                                            x:'',
                                            y:'',                                                
                                            status:0};
                image_collection.insertOne(image_data_insert, function(err, insert_images) {
                 if (err) throw err;
                 
                  });
                  }
                 }

                if(color_length > 0){
                 for(var k = 0; k < color_length;k++){
                   var color_collection = db.get().collection("prod_colors");                    
                   var color_data_insert = {product_id: product_id,
                                            name:color_picker[k]['name'],
                                            color:color_picker[k]['color'],
                                            limited_color:color_picker[k]['limited_color'],
                                            nodeName:color_picker[k]['nodeName'],
                                            colorpick:color_picker[k]['colorpick'],
                                            keyframe:color_picker[k]['keyframe']};
                    color_collection.insertOne(color_data_insert, function(err, insert_color) {
                    });
                    }
                   }

              if(_.size(result_comp) > 0) {
               result_comp.forEach(function(elementss,j) {
               var insert_product_comp = {comp_name:prod_info['comp_data'][j]['comp_name'],
                                          comp_method:prod_info['comp_data'][j]['comp_method'],
                                          prod_id:product_id,
                                          original_width:parseInt(prod_info['comp_data'][j]['original_width']),
                                          original_height:parseInt(prod_info['comp_data'][j]['original_height']),
                                          preview_width:parseInt(prod_info['comp_data'][j]['preview_width']),
                                          preview_heigh:parseInt(prod_info['comp_data'][j]['preview_heigh']),
                                          key_time:prod_info['comp_data'][j]['key_time'],
                                          render_start_time:0,
                                          render_status:0,
                                          port_number:0,
                                          render_end_time:0,
                                          render_time: prod_info['comp_data'][j]['render_time'],
                                          status:parseInt(prod_info['comp_data'][j]['status']),
                                          preview_image:prod_info['comp_data'][j]['preview_image'],
                                          preview_video:prod_info['comp_data'][j]['preview_video'],
                                          nexrender_status:"",
                                          aftereffect_url:"",
                                          final_upload_url:"",
                                          final_aftereffect:"",
                                          fileUpload:0,
                                          uploadPercentage:0,
                                          render_finalized:0,

                                          priority:parseInt(prod_info['comp_data'][j]['priority'])};
                                         
              var pro_comp_collection = db.get().collection("prod_comp");                    
               pro_comp_collection.insertOne(insert_product_comp, function(err, insert_comp) {                                                                             
               if (err) throw err;
             
               });
              if(j === 3){
                console.log(product_id)
               res.status(200).json({ "status": "sucess","product_id":product_id});
               } else {
               }
               });
               } else {
               res.status(201).json({ "status": "comp_data not found","product_id":""});
               }
              });
             }

             /* End of Add project*/

             exports.addsub = (req,res,next) => {
              var cat_id = req.body.cat_id;
              var cat_result = [];
              var collection = db.get().collection("category_subcategory");
              collection.find({cat_id:cat_id}).toArray( function(err, result_cat) {
              if (err) throw err;
              if(_.size(result_cat) > 0){
              for(var i=0;i<result_cat.length;i++){
              getsubcatdata(req,result_cat,cat_result,i, function(cat_final_result,i){ 
              if(i === result_cat.length -1){
              res.status(201).json({ "status": "sucess","sub_cat":cat_final_result});
               }
               });
               }
               }
               });
             }

            function getsubcatdata(req,result_cat,cat_result,i,next){ 
              var collection = db.get().collection("subcategory");
              console.log(result_cat[i]['sub_cat_id'])
              collection.find({_id:ObjectId(result_cat[i]['sub_cat_id'])}).toArray( function(err, result__sub_cat) {
              if (err) throw err;
              var subCat_reult = {_id:result__sub_cat[0]['_id'],cat_name:result__sub_cat[0]['cat_name']}
              cat_result.push(subCat_reult);
              next(cat_result,i);
              });
              }


            exports.getuserProducts = (req,res,next) => {
            var user_id = req.body.user_id;
            var collection = db.get().collection("order_item");
            collection.find({user_id:parseInt(user_id)}).toArray( function(err, result_cat) {
            if (err) throw err;
             res.status(201).json({ "status": "sucess","result":result_cat});
             });
            }

             exports.getleadProducts = (req,res,next) => {
             var lead_id = req.body.lead_id;
             var collection = db.get().collection("order_item");
             collection.find({lead_id:parseInt(lead_id)}).toArray( function(err, result_cat) {
             if (err) throw err;
             res.status(201).json({ "status": "sucess","result":result_cat});
             });
            }

             exports.getsubadmincategory = (req, res, next) => {
              var cat_id = req.query.cat_id;
              var collection = db.get().collection("subcategory");
              if(cat_id === ''|| cat_id === null || cat_id === undefined){
              collection.find().toArray( function(err, result) {
              if (err) throw err;
               res.status(200).json({ "status": "sucess","subcategory":result});
                });
               } else {
               collection.find({id:parseInt(cat_id)}).toArray( function(err, result) {
               if (err) throw err;
               res.status(200).json({ "status": "sucess","subcategory":result});
                });
               }
              }

             exports.addCategory = (req,res,next) => {
              var cat_name = req.body.cat_name;
              var cat_slug = req.body.cat_slug;
              var cat_url = req.body.cat_url;
              var cat_id =req.body.cat_id;
              var subcategory = req.body.subcategory;
              var cat_data = {name:cat_name,
                              slug:cat_slug,
                              url:cat_url,
                              status:1};
              var pro_cat_collection = db.get().collection("product_category"); 
              var cat_sub_collection = db.get().collection("category_subcategory"); 
              if(cat_id === "" || cat_id === null || cat_id === undefined){
                pro_cat_collection.insertOne(cat_data, function(err, insert_pro_cat) {
                if (err) throw err;
                for(var i=0;i<subcategory.length;i++){
                var subcat_data = {cat_id:parseInt(insert_pro_cat.insertedId),
                                   sub_cat_id:parseInt(subcategory[i]),
                                   status:1};
                cat_sub_collection.insertOne(subcat_data, function(err, result_sub_cat) {
                if (err) throw err;
                res.status(201).json({ "status": "sucess"});
                 });
                 }
                });
               } else {
               var myquery = { id: parseInt(cat_id)};
               var newvalues = { $set: {name:cat_name,slug:cat_slug,url:cat_url,status:1}};
               pro_cat_collection.updateOne(myquery,newvalues, function(err, result) {
               for(var i=0;i<subcategory.length;i++){
               var subcat_data = { $set:{sub_cat_id:parseInt(subcategory[i]),status:1}};
               cat_sub_collection.updateOne(myquery,subcat_data, function(err, result) {
               if (err) throw err;
               res.status(201).json({ "status": "sucess"});
                 });
                 }
               })
              }
            }

            exports.addsubCategorys = (req,res,next) => {
              var cat_name = req.body.name;
              var cat_slug = req.body.slug;
              var cat_id =req.body.cat_id;
              var sub_collection = db.get().collection("subcategory"); 
              var cat_data = {cat_name:cat_name,cat_slug:cat_slug,status:1};
              if(cat_id === "" || cat_id === null || cat_id === undefined){
               sub_collection.insertOne(cat_data, function(err, insert_subcat) {
                  if (err) throw err;
                  res.status(201).json({ "status": "sucess"});
               })
          
               } else {
                var myquery = { id: parseInt(cat_id)};
                var subcat_data = { $set:{cat_name:cat_name,cat_slug:cat_slug,status:1}};
                sub_collection.updateOne(myquery,subcat_data, function(err, result) {
                if (err) throw err;
                res.status(201).json({ "status": "sucess"});
                  });
                 }
              }

             exports.addTags = (req,res,next) => {
                  var tag_name = req.body.tag_name;
                  var tag_slug = req.body.tag_slug;
                  var tag_id =req.body.tag_id;
                  var tags_collection = db.get().collection("tags"); 
                  var tag_data = {tag_name:tag_name,tag_slug:tag_slug};
                  if(tag_id === "" || tag_id === null || tag_id === undefined){
                  tags_collection.insertOne(tag_data, function(err, insert_tag) {
                  if (err) throw err;
                  res.status(201).json({ "status": "sucess"});
                  })
                  } else {
                var myquery = { id: parseInt(tag_id)};
                var subcat_data = { $set:{tag_name:tag_name,tag_slug:tag_slug}};
                tags_collection.updateOne(myquery,subcat_data, function(err, result) {
                if (err) throw err;
                res.status(201).json({ "status": "sucess"});
                });
                }
                }

           exports.addgroupproducts = (req,res,next) => {
                  var group_name = req.body.groupname;
                  var group_url = req.body.group_url;
                  var tag_id = req.body.productTag;
                  
                  var comp_method =req.body.wp_status;

                  

                  var priority =req.body.priority;
                  var url = req.body.url;
                  var group_id = req.body.id;
                  var category_slider_collection = db.get().collection("category_slider"); 
                  var slider_tag_collection = db.get().collection("slider_tag"); 
                  var slider_product_collection = db.get().collection("slider_product");
                  var group_data = {group_name:group_name,group_url:group_url,priority:parseInt(priority),comp_method:comp_method};
                  var update_group_data = {$set:{group_name:group_name,group_url:group_url,priority:parseInt(priority),comp_method:comp_method}};
                  if(group_id === "" || group_id === null || group_id === undefined){
                  category_slider_collection.insertOne(group_data, function(err, result_cat) {
                  if (err) throw err;
                  group_id = result_cat.insertedId;

                 if(_.size(url) >0){
                 for (var i =0; i < url.length; i++){
                  var product_name = url[i]['timestamp'];
                  var respio = product_name.split("/");
                  var last_word = respio[respio.length-1];
                  var priority = i+1;
                  var tag_data = {product_name:last_word,group_id:ObjectId(group_id),priority:priority}
                  slider_product_collection.insertOne(tag_data, function(err, result_cat) {
                  if (err) throw err;
                  });
                  }
                  }
                  });
                  } else {
               console.log(update_group_data)
                var myquery = { group_id: ObjectId(group_id)};
                var group_ery = {_id: ObjectId(group_id)};
                category_slider_collection.deleteMany(myquery, function(err, result) {
                if (err) throw err;
                slider_product_collection.deleteMany(myquery, function(err, result) {
                if (err) throw err;
                category_slider_collection.updateOne(group_ery,update_group_data, function(err, result) {
                if (err) throw err;
                               
                 if(_.size(url) >0){
                 for (var i =0; i < url.length; i++){
                  var product_name = url[i]['product_url'];
                  var respio = product_name.split("/");
                  var last_word = respio[respio.length-1];
                  var priority = i+1;
                  var tag_data = {product_name:last_word,group_id:ObjectId(group_id),priority:priority}
                  slider_product_collection.insertOne(tag_data, function(err, result_cat) {
                  if (err) throw err;
                  });
                  }
                  }
                 });
                 })
                  });
                 }
              
                 res.status(201).json({ "status": "sucess"});
                 } 




               exports.addsliderproducts = (req,res,next) => {
                  var group_name = req.body.groupname;
                  //var group_url = req.body.group_url;
                  // var tag_id = req.body.productTag;
                  // var comp_method =req.body.wp_status;
                  //var priority =req.body.priority;
                  var url = req.body.url;
                  var group_id = req.body.id;
                 
                  var sliders_collection = db.get().collection("sliders"); 
                  var slider_url_collection = db.get().collection("sliders_url"); 
                  
                  var group_data = {group_name:group_name};
                  var update_group_data = {$set:{group_name:group_name}};
                  
                  if(group_id === "" || group_id === null || group_id === undefined){
                  sliders_collection.insertOne(group_data, function(err, result_cat) {
                  if (err) throw err;
                  group_id = result_cat.insertedId;
                  if(_.size(url) >0){
                  for (var i =0; i < url.length; i++){
                  var product_name = url[i]['timestamp'];
                  var respio = product_name.split("/");
                  var last_word = respio[respio.length-1];
                  var comp_method= url[i]['composition']['value'];
                  var priority = i+1;
                  var tag_data = {product_name:last_word,group_id:ObjectId(group_id),priority:priority,comp_method:comp_method}
                  slider_url_collection.insertOne(tag_data, function(err, result_cat) {
                  if (err) throw err;
                  });
                  }
                  }
                  });
                  } else {
                var myquery = { group_id: ObjectId(group_id)};
                var group_ery = {_id: ObjectId(group_id)};
               
                slider_url_collection.deleteMany(myquery, function(err, result) {
                if (err) throw err;
             
                sliders_collection.updateOne(group_ery,update_group_data, function(err, result) {
                if (err) throw err;
                if(_.size(url) >0){
                 for (var i =0; i < url.length; i++){
                  var product_name = url[i]['product_url'];
                  var respio = product_name.split("/");
                  var last_word = respio[respio.length-1];
                     var comp_method= url[i]['composition']['value'];
                  var priority = i+1;
                  var tag_data = {product_name:last_word,group_id:ObjectId(group_id),priority:priority,comp_method:comp_method}
                  slider_url_collection.insertOne(tag_data, function(err, result_cat) {
                  if (err) throw err;
                  });
                  }
                  }
                 });
                 })
                 }
                 res.status(201).json({ "status": "sucess"});
                 } 




               exports.getGroupproducts = (req,res,next) => {
               var group_id = req.query.id;
               var category_slider_collection = db.get().collection("category_slider"); 
               if(group_id === "" || group_id === null || group_id === undefined){
               category_slider_collection.find().toArray( function(err, result_cat) {
               if (err) throw err;
               res.status(201).json({ "status": "sucess","groupData": result_cat});
                });
               } else {
               var options = {
                    allowDiskUse: true
                };
                var pipeline = [
                    {
                        "$project": {
                            "_id": 0,
                            "category_slider": "$$ROOT"
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "category_slider.id",
                            "from": "slider_product",
                            "foreignField": "group_id",
                            "as": "slider_product"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$slider_product",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "slider_product.product_name",
                            "from": "product",
                            "foreignField": "pro_buffer_id",
                            "as": "product"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$product",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "product._id",
                            "from": "prod_comp",
                            "foreignField": "prod_id",
                            "as": "prod_comp"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$prod_comp",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$match": {
                            "category_slider.group_id": parseInt(group_id),
                            "category_slider.comp_method": "prod_comp.comp_method"
                        }
                    }
                ];
                
               category_slider_collection.aggregate(pipeline, options).toArray( function(err, result_cat) {
                if (err) throw err;

              res.status(201).json({ "status": "sucess","groupData": result_cat});
               });
                 }
                }


               exports.getsliderproducts = (req,res,next) => {
               var group_id = req.query.id;
               var slider_collection = db.get().collection("sliders"); 
               if(group_id === "" || group_id === null || group_id === undefined){
               slider_collection.find().toArray( function(err, result_cat) {
               if (err) throw err;
               res.status(201).json({ "status": "sucess","groupData": result_cat});
                });
               } else {
               var options = {
                    allowDiskUse: true
                };
                var pipeline = [
                    {
                        "$project": {
                            "_id": 0,
                            "sliders": "$$ROOT"
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "sliders._id",
                            "from": "sliders",
                            "foreignField": "group_id",
                            "as": "sliders_url"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$sliders_url",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "sliders_url.product_name",
                            "from": "product",
                            "foreignField": "pro_buffer_id",
                            "as": "product"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$product",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "product._id",
                            "from": "prod_comp",
                            "foreignField": "prod_id",
                            "as": "prod_comp"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$prod_comp",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$match": {
                            "sliders.group_id": parseInt(group_id),
                            "sliders_url.comp_method": "prod_comp.comp_method"
                        }
                    }
                ];
                
               category_slider_collection.aggregate(pipeline, options).toArray( function(err, result_cat) {
                if (err) throw err;

              res.status(201).json({ "status": "sucess","groupData": result_cat});
               });
                 }
                }





            exports.getGroupproductsdetails = (req,res,next) => {
                var group_id = req.query.id;
                var url = [];
                var category_slider_collection = db.get().collection("category_slider"); 
                var slider_product_collection = db.get().collection("slider_product"); 
                var slider_tag_collection = db.get().collection("slider_tag"); 
                                
                category_slider_collection.find({'_id':ObjectId(group_id)}).toArray( function(err, result_cat_slider) {
                if (err) throw err;
                if(_.size(result_cat_slider)> 0){
                var comp_method = result_cat_slider[0]['comp_method'];
                var options = {
                    allowDiskUse: true
                };
                
                var pipeline = [
                    {
                        "$project": {
                            "_id": 0,
                            "slider_product": "$$ROOT"
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "slider_product.product_name",
                            "from": "product",
                            "foreignField": "pro_buffer_id",
                            "as": "product"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$product",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$lookup": {
                            "localField": "product._id",
                            "from": "prod_comp",
                            "foreignField": "prod_id",
                            "as": "prod_comp"
                        }
                    }, 
                    {
                        "$unwind": {
                            "path": "$prod_comp",
                            "preserveNullAndEmptyArrays": true
                        }
                    }, 
                    {
                        "$match": {
                            "slider_product.group_id":ObjectId(group_id),
                            "prod_comp.comp_method": comp_method
                        }
                    }
                ];
                
                slider_product_collection.aggregate(pipeline, options).toArray( function(err, result_cat_product) {
                if (err) throw err;
                console.log(result_cat_product)

                result_cat_product.forEach(function (officer) {
                var url_data = {product_name:officer.product.pro_name,preview_image:officer.product.product_preview,timestamp:officer.product.pro_name,product_url:"https://www.onemaker.io/i/editor/product/"+officer.product.pro_buffer_id}
                url.push(url_data);
                });

                 //  slider_tag_collection.find({'group_id':parseInt(group_id)}).toArray( function(err, result_tag_product) {
                 // if (err) throw err;
                 // var officersIds = [];
                 //  result_tag_product.forEach(function (officer) {
                 //  officersIds.push(officer.tag_id);
                 //  });


                 var groupDetails = {groupname:result_cat_slider[0]['group_name'],group_url:result_cat_slider[0]['group_url'],wp_status:comp_method,priority:result_cat_slider[0]['priority'],url:url}
                 res.status(201).json({ "status": "sucess","groupDetails": groupDetails});
                  });
                  //});
                  } else {
                  res.status(201).json({ "status": "sucess","groupDetails": []});
                }
                });
                }
                

              exports.getCompproducts = (req,res,next) => {
              var comp_method = req.query.comp_method;
              var collection = db.get().collection("prod_comp");
              if(comp_method === "" || comp_method === null || comp_method === undefined){
                collection.find().toArray( function(err, result_fonts) {
              if (err) throw err;
               res.status(200).json({ "status": "200","compData":result_fonts});     
               });
              } else {
              collection.find({"comp_method":comp_method}).toArray( function(err, result_fonts) {
               if (err) throw err;
               res.status(200).json({ "status": "200","compData":result_fonts});     
              });
              }
             }
 
 
             exports.getProductTags = (req,res,next) => {
              var product_buffer_id = req.query.product_id;
              var tag_name = [];
              var pro_collection = db.get().collection("product");
              var pro_tag_collection = db.get().collection("product_tags");
              pro_collection.find({"pro_buffer_id":product_buffer_id}).toArray( function(err, result_product_id) {
              if (err) throw err;
              if(_.size(result_product_id) > 0){
              let product_id = result_product_id[0]['pro_id'];
              pro_tag_collection.find({"product_id":parseInt(product_id)}).toArray( function(err, result_tag_id) {
              if (err) throw err;
              if (err) throw err;
              if(_.size(result_tag_id)>0){
              for(var i=0;i<result_tag_id.length;i++){
              module.exports.getTagsname(result_tag_id[i]['tag_id'],i,function(result,j){
              tag_name.push(result[0]);
              if(result_tag_id.length-1 == j){
              tag_name.push(result[0]);
              tag_name = removeDuplicates(tag_name);
              res.status(201).json({ "status": "sucess","tag_name": tag_name,"product_id":product_buffer_id});
              }
              });
              }
              }
              });
              } else {
              res.status(201).json({ "status": "sucess","tag_name": [],"product_id":product_buffer_id});
              }
              });
              }


           
             exports.getTagsname = (tag_id,i,next) => {
             var tag = [];
             var tag_collection = db.get().collection("product_tags");
             tag_collection.find({"id":parseInt(tag_id)}).toArray( function(err, result_tag_id) {
             if (err) throw err;
             tag.push(result_tag_id[0]['tag_slug']);
              next(tag,i);
              });
              }

            function removeDuplicates(array) {
            return array.filter((a, b) => array.indexOf(a) === b)
            };

            exports.getProduct = (req,res,next)=>{
              var product_id = req.query.product_id;
              var product_collection = db.get().collection("product");
              var comp_collection = db.get().collection("prod_comp");
              var images_collection = db.get().collection("prod_images");
              var tag_collection = db.get().collection("product_tags");
              var text_collection = db.get().collection("prod_text");
              var colors_collection = db.get().collection("prod_colors");
              var subcategory_collection = db.get().collection("product_subcategory");
              var asset_array = [];
              var tag_array = [];
              var tag_data = {};
              var sub_cagetory_array = [];
               if(product_id === null || product_id === undefined || product_id === ""){
               product_collection.find({status:{"$in":[0,1]}}).toArray( function(err, result_pro) {
               if (err) throw err;
               res.status(201).json({ "status": "sucess","products":result_pro});
               });
               } else {
                product_collection.find({'_id':ObjectId(product_id)}).toArray( function(err, result_pro) {
               if (err) throw err;


               if(_.size(result_pro) > 0){
               tag_collection.find({'product_id':product_id}).toArray( function(err, result_tag) {
               if (err) throw err;

               result_tag.forEach(function (t) {
                tag_data[t.tag_id] = t.tag_id;
                 }),
                Object.entries(tag_data).forEach(function ([key]) {
                return tag_array.push(`${key}`);
                }),

               
               subcategory_collection.find({'product_id':product_id}).toArray( function(err, result_subcat) {
                if (err) throw err;
                var cot_data= {};
                var cat_array = [];

                result_subcat.forEach(function (t) {
                  cot_data[t.subcategory_id] = t.subcategory_id;
                   }),

               Object.entries(cot_data).forEach(function ([key]) {
                  return cat_array.push(`${key}`);
                  }),
                 
             images_collection.find({'product_id':ObjectId(product_id)}).sort({'name':1}).collation( { locale: "en_US", numericOrdering: true }).toArray( function(err, result_images) {
              if (err) throw err;   
                            
              text_collection.find({'product_id':ObjectId(product_id)}).sort({'name':1}).collation( { locale: "en_US", numericOrdering: true }).toArray( function(err, result_text) {
               if (err) throw err;
              
              colors_collection.find({'product_id':ObjectId(product_id)}).sort({'name':1}).collation( { locale: "en_US", numericOrdering: true }).toArray( function(err, result_colors) {
              if (err) throw err;
                           
         
              comp_collection.find({'prod_id':ObjectId(product_id)}).toArray( function(err, result_comp) {
               if (err) throw err;
               var comp_array = [];
               if(_.size(result_comp)>0){
               for(var j = 0; j < result_comp.length; j++){
                      var assets_array = {_id:ObjectId(result_comp[j]['_id']),
                                          comp_name:result_comp[j]['comp_name'],
                                          comp_method:result_comp[j]['comp_method'],
                                          prod_id:ObjectId(result_comp[j]['prod_id']),
                                          original_width: result_comp[j]['original_width'],
                                          original_height: result_comp[j]['original_height'],
                                          preview_width: result_comp[j]['preview_width'],
                                          preview_heigh: result_comp[j]['preview_heigh'],
                                          key_time:result_comp[j]['key_time'],
                                          render_time: result_comp[j]['render_time'],
                                          render_status: result_comp[j]['render_status'],
                                          status: result_comp[j]['status'],
                                          preview_image:result_comp[j]['preview_image'],
                                          preview_video: result_comp[j]['preview_video'],
                                          nexrender_status: result_comp[j]['nexrender_status'],
                                          uploadPercentage: result_comp[j]['uploadPercentage'],
                                          priority: 1,
                                          images:result_images,
                                          text:result_text,
                                          color_picker:result_colors,
                                          };
                        
                       comp_array.push(assets_array);
                 if(result_comp.length-1 == j){
                   var product_array = {pro_id:product_id,
                                   id:j+1,
                                   comp_data:comp_array,
                                   tag_id:tag_array,
                                   subcategory_id:cat_array,
                                   pro_name:result_pro[0]['pro_name'],
                                  producer_name:result_pro[0]['producer_name'],
                                  producer_fname:result_pro[0]['producer_fname'],
                                   pro_price: result_pro[0]['pro_price'],
                                   category: result_pro[0]['category'],
                                   status:result_pro[0]['status'],
                                   sd_download:result_pro[0]['sd_download'],
                                   filePath: result_pro[0]['filePath'],
                                   default_audio:result_pro[0]['default_audio'],
                                   pro_tax: result_pro[0]['pro_tax'],
                                   created_at: result_pro[0]['created_at'],
                                   updated_at: result_pro[0]['updated_at'],
                                   producer_p_id: result_pro[0]['producer_p_id'],
                                   version:result_pro[0]['version'] === undefined || result_pro[0]['version'] === NaN || result_pro[0]['version'] === ""  ? 0.1:result_pro[0]['version'],
                                   image_render_time: result_pro[0]['image_render_time'] === undefined ? 0 : result_pro[0]['image_render_time'],
                                   image_frame: result_pro[0]['image_frame'],
                                   wp_status:result_pro[0]['wp_status'],
                                   default_audio_url:result_pro[0]['default_audio_url'],
                                   pro_buffer_id: result_pro[0]['pro_buffer_id'],
                                   filename: result_pro[0]['filename'],
                                   random_number: result_pro[0]['random_number']};
                 return res.status(201).json({ "status": "sucess","products":product_array});
                  
                 }
                 }
                 } else {
                  return res.status(201).json({ "status": "sucess","products":[]});

                 }
                 });
                 })
                 });
                 });
                 });
                 });
                 }else {
                 return res.status(201).json({ "status": "sucess","products":[]});
                 }
                 });
                 }              
                 }

            exports.deleteBuffer = (req,res,next) => {
            var random_number = req.body.random_number;
            var collection = db.get().collection("product");
            var myquery = { random_number: random_number};
            var newvalues = { $set: {status: 4} };
            collection.updateOne(myquery,newvalues, function(err, result) {
            if (err) throw err;
            res.status(201).json({ "status": "sucess"});
            });
           }

          exports.bulkDelete = (req,res,next) => {
          var random_number = req.body.random_number;
          var collection = db.get().collection("product");
          random_number.forEach(function (officer,index) {
            var myquery = { random_number:officer};
            var newvalues = { $set: {status: 4} };
            collection.updateMany(myquery,newvalues, function(err, result) {
            if (err) throw err;
            if(index === random_number.length-1){
            res.status(201).json({ "status": "sucess"});
            }
            });
            });
         }


        exports.bulkapproval = (req,res,next) => {
          var random_number = req.body.random_number;
          var collection = db.get().collection("product");
          random_number.forEach(function (officer,index) {
            var myquery = { random_number:officer};
            var newvalues = { $set: {status: 0} };
            collection.updateMany(myquery,newvalues, function(err, result) {
            if (err) throw err;
            if(index === random_number.length-1){
            res.status(201).json({ "status": "sucess"});
            }
            });
            });
         }

     exports.disapprove = (req,res,next) => {
          var product_id = req.body.product_id;
          var collection = db.get().collection("product");
            var myquery = { _id:ObjectId(product_id)};
            var newvalues = { $set: {status: 0} };
            collection.updateMany(myquery,newvalues, function(err, result) {
            if (err) throw err;
            res.status(201).json({ "status": "sucess"});
            });
           
         }
        


         exports.relatedProduct = (req, res, next) => {
          const cat_id =  req.query.cat_id;
         const sub_cat_id =  req.query.sub_cat_id;
         if(cat_id === "" || cat_id === null || cat_id === undefined){
          res.status(200).json({ "status": "sucess","related_product":[]});
         } else {
          var collection = db.get().collection("product");
          // comp_collection.find({'prod_id':parseInt(product_id)}).toArray( function(err, result_comp) {
          //   if (err) throw err;
          var related_product_query= "SELECT `pro_name`,`preview_width`,`preview_heigh` FROM product LEFT JOIN prod_comp ON product.pro_id = prod_comp.prod_id WHERE product.category = '"+cat_id+"' and subcategory IN ('"+sub_cat_id+"') And product.status =1 AND product.wp_status = prod_comp.comp_method ORDER BY sd_download DESC";
          db.query(related_product_query, function (err, result) {
          if (err) throw err;
          if(_.size(result) > 0){
           res.status(200).json({ "status": "sucess","related_product":result});
           } else {
           res.status(200).json({ "status": "sucess","related_product":[]});
            }
           });
           }
          }

        exports.project_upload_audio = (req, res) => {
          var url_name ='prefix_'+ (new Date().getTime()).toString(36)+"_t.mp3";
          var png_name ='prefix_'+ (new Date().getTime()).toString(36)+".png";
          var file = 'project_uploads' + '/' + url_name;
          var test_number = (new Date().getTime()).toString(36);
          var file_name = req.body.file_name;
          var fineName = req.file.originalname
          var extpart = getFileExtension3(fineName);
          fs.rename(req.file.path, file, function(err) {
          if (err) {
           res.status(500);
           } else {
          fs.readFile('project_uploads' + '/' + url_name, function (err, data) {
          if (err) { throw err; }
          const s3Render = async () => {
               await s3.putObject({
                     Body: data,
                     Bucket: "consumerdata",
                     Key: 'uploaddata/'+test_number+'/'+url_name,
                     ACL: 'public-read',
                     CacheControl: 'public, max-age=50',
                     },(err, data) => {
                      if (err) {
                       console.log(err);
                      } else {
                       console.log("Successfully uploaded data ");
                      }
                      });
                      }
                      s3Render().catch(err => {
                           console.log(err);
                           res.status(500).json({
                            error: err
                            });
                            });
                            });
         fs.readFile('project_uploads' + '/' + url_name, function (err, data) {
         if (err) { throw err; }
         const child = exec('ffmpeg -i ./project_uploads/'+url_name+' -lavfi showwavespic=s=180x10:colors=B8B8B8 ./project_uploads/'+png_name, (error, stdout, stderr) => {
        if (error) {
        throw error;
         }
        fs.readFile('./project_uploads/'+png_name, function (err, data) {
        if (err) { throw err; }
         const s3Render = async () => {
         await s3.putObject({
                    Body: data,
                   Bucket: "consumerdata",
                   Key: 'uploaddata/'+test_number+'/'+png_name,
                   ACL: 'public-read',
                  CacheControl: 'public, max-age=50',
                  }, (err, data) => {
                 if (err) {
                 console.log(err);
                 } else {
                  res.status(200).json({ "status": "data found","upload_url": consumer_url +"uploaddata/"+ test_number +'/'+url_name,"waveFomr_url":consumer_url +"uploaddata/"+ test_number +'/'+png_name,"nodeName":req.body.nodeName});
                  fs.unlink('project_uploads' + '/' + req.file.originalname, (err) => {
                  if (err) {
                  console.log("failed to delete local mp3:"+err);
                  } else {
                  fs.unlink('project_uploads' + '/' + file_name+'.png', (err) => {
                 if (err) {
                  console.log("failed to delete local image:"+err);
                   } else {
                  console.log('successfully deleted local image');                                
                   }
                   });
                   }
                  });
                  }
                  });
                  }
               s3Render().catch(err => {
               console.log(err);
               res.status(500).json({
               error: err
               });
               });
               });
               });
               });
               }
               });
            }


        exports.groupproductspriority = (req, res, next) => {
         var cat_slider_collection = db.get().collection("category_slider");
         cat_slider_collection.find().toArray( function(err, result) {
         if (err) throw err;
          var officersIds = [];
           result.forEach(function (officer) {
           officersIds.push(officer.priority);
           });
          var generate_number = range(1, 11, 1);
          var sumArray = arrayDiff(generate_number,officersIds);  
          res.status(200).json({ "status": "sucess","priorities":sumArray});
           });
          }

         const range = (start, end, step) => {
         return Array.from(Array.from(Array(Math.ceil((end-start)/step)).keys()), x => start+ x*step);
          }

        function arrayDiff(a, b) {
          var arrays = Array.prototype.slice.call(arguments);
          var diff = [];
           arrays.forEach(function(arr, i) {
           var other = i === 1 ? a : b;
           arr.forEach(function(x) {
           if (other.indexOf(x) === -1) {
           diff.push(x);
           }
           });
           })
          return diff;
          }

          exports.getGroupproductsslider = (req,res,next) => {
          res.status(201).json({ "status": "error","comment":"sucess","groupDetails":groupData});
           }
                

          exports.getproductsslider = (req,res,next) => {
          res.status(201).json({ "status": "error","comment":"sucess","groupDetails":groupData});
           }

          exports.getGroupproductss = (result_cat_slider,i,next) => {
            var comp_method = result_cat_slider['comp_method'];
            var group_name = result_cat_slider['group_name'];
            var priority = result_cat_slider['priority'];
            var group_id = result_cat_slider['id'];
            var groupLog = [];
            var cat_slider_collection = db.get().collection("slider_product");
               
                var options = {
                  allowDiskUse: true
              };
              
              var pipeline = [
                  {
                      "$project": {
                          "_id": 0,
                          "slider_product": "$$ROOT"
                      }
                  }, 
                  {
                      "$lookup": {
                          "localField": "slider_product.product_name",
                          "from": "product",
                          "foreignField": "pro_buffer_id",
                          "as": "product"
                      }
                  }, 
                  {
                      "$unwind": {
                          "path": "$product",
                          "preserveNullAndEmptyArrays": true
                      }
                  }, 
                  {
                      "$lookup": {
                          "localField": "product.pro_id",
                          "from": "prod_comp",
                          "foreignField": "prod_id",
                          "as": "prod_comp"
                      }
                  }, 
                  {
                      "$unwind": {
                          "path": "$prod_comp",
                          "preserveNullAndEmptyArrays": true
                      }
                  }, 
                  {
                      "$match": {
                          "slider_product.group_id":parseInt(group_id),
                          "prod_comp.comp_method": comp_method
                      }
                  }
              ];

            cat_slider_collection.aggregate(pipeline, options).toArray( function(err, result_cat_product) {
            if (err) throw err;
            var groupDetails = {groupname:group_name,wp_status:comp_method,priority:priority,url:result_cat_product};
            groupLog.push(groupDetails);
            next(groupLog,i);
            });
            }


      exports.groupproductspriority = (req, res, next) => {
       var cat_slider_collection = db.get().collection("category_slider");
       cat_slider_collection.find().toArray( function(err, result) {
        if (err) throw err;
        var officersIds = [];
            result.forEach(function (officer) {
            officersIds.push(officer.priority);
         });
         var generate_number = range(1, 11, 1);
         var sumArray = arrayDiff(generate_number,officersIds);  
         res.status(200).json({ "status": "sucess","priorities":sumArray});
          });
         }


       exports.getRenderItem = (req, res, next) => {
         var render_result = [];
         var order_type ='';
       var order_item_collection = db.get().collection("order_item");
        order_item_collection.find().toArray( function(err, result_order) {
         if (err) throw err;
         if(_.size(result_order)>0){
          result_order.forEach(function (result,index) {
              var product_id = result['product_id'];
              
              var user_id = result['user_id'];
              db.get().collection("product").find(
              { 
                  "pro_buffer_id" : product_id
              }
              ).toArray( function(err, pro_result) {
              if (err) throw err;
              var category = pro_result[0]['category'];

              db.get().collection("product_category").find(
                { 
                    "_id" : ObjectId(category)
                }
                ).toArray( function(err, pro_category) {
                if (err) throw err;
                db.get().collection("order").find(
                { 
                "user_id" : ObjectId(user_id)
                }
               ).toArray( function(err, pro_order) {
               if (err) throw err;

               if(_.size(pro_order)>0){
                order_type= pro_order[0]['order_type'];
               } else {
                order_type = 1
               }
               
               var render_time = 0

              render_time = Math.floor(result['render_finalized'] - result['render_created']);
              render_time = Math.floor(render_time/1000);

              if(render_time<0){
                render_time = 0;
              } else {
                render_time = render_time;
              }



            
                var render_array = {order_item_id:index+1,
                                   product_id:(pro_result[0]['pro_name'] ? pro_result[0]['pro_name'] : ''),
                                   pro_price:(pro_result[0]['pro_price'] ? parseInt(pro_result[0]['pro_price']) :0),
                                   slug:pro_category[0]['slug'],
                                   order_type:order_type,
                                   render_status:result['render_status'],
                                   item_status:result['item_status'],
                                   render_type:result['render_type'],
                                   render_created:result['render_created'],
                                   jobs_failure:result['jobs_failure'],
                                   port:result['port_number'],
                                   item_buffer_id:result['item_buffer_id'],
                                   render_time:render_time
                                 
                                   }
                                  render_result.push(render_array);


                      if(index===result_order.length-1){
                      res.status(200).json({ "status": "sucess","render_item":render_result});
                      }            
                 });
                });
              });

           });
          } else {
          res.status(200).json({ "status": "sucess","render_item":[]});
          }
          });
         }


        exports.uploadRenderItem = (req, res, next) => {
          var uploader_queue_collection = db.get().collection("uploader_queue");
          var render_queue = [];
                
         uploader_queue_collection.find({}).toArray( function(err, result) {
          if (err) throw err;
          if(_.size(result)>0){
            result.forEach(function (officer,index) {
            var product_name = '';
            var render_json = {compositions: officer['compositions'],
                                created_status: officer['created_status'],
                                finalized_status:officer['finalized_status'],
                                picked_status: officer['picked_status'],
                                failure_status: officer['failure_status'],
                                port_number:officer['port_number'],
                                product_id: officer['prod_name'],
                                rendering_status:officer['rendering_status'],
                                server_id:officer['server_id'],
                                id:index+1
                              }
              render_queue.push(render_json);
              if(index===result.length-1){
                res.status(200).json({ "status": "sucess","render_item":render_queue});
               }     
             }); 
            } else {
              res.status(200).json({ "status": "sucess","render_item":[]});
            }
            });
          }


        exports.getRenderItemInfo = (req, res, next) => {

        var item_buffer_id = req.query.item_buffer_id;
        var result_text = [];
         var compostion_data = [];
         var collection = db.get().collection("order_item");

         var options = {
             allowDiskUse: true
         };
         
         var pipeline = [
             {
                 "$project": {
                     "_id": 0,
                     "order_item": "$$ROOT",
                    
                 }
             }, 
             {
                 "$lookup": {
                     "localField": "order_item.product_id",
                     "from": "product",
                     "foreignField": "pro_buffer_id",
                     "as": "product"
                 }
             }, 
             {
                 "$unwind": {
                     "path": "$product",
                     "preserveNullAndEmptyArrays": true
                 }
             }, 
             {
                 "$lookup": {
                     "localField": "product.category",
                     "from": "product_category",
                     "foreignField": "_id",
                     "as": "product_category"
                 }
             }, 
             {
                 "$unwind": {
                     "path": "$product_category",
                     "preserveNullAndEmptyArrays": true
                 }
             }, 
             {
                 "$lookup": {
                     "localField": "order_item.user_id",
                     "from": "order",
                     "foreignField": "user_id",
                     "as": "order"
                 }
             }, 
             {
                 "$unwind": {
                     "path": "$order",
                     "preserveNullAndEmptyArrays": true
                 }
             }, 
             {
                 "$match": {
                     "order_item.item_buffer_id": item_buffer_id
                
                 }
             }
         ];
         
         collection.aggregate(pipeline, options).toArray( function(err, result) {
          if (err) throw err;

          var item_id = ObjectId(result[0]['order_item']['_id']);
 
       db.get().collection("order_item_text").find({item_id:item_id }).toArray( function(err, result_text) {
       if (err) throw err;
     
       db.get().collection("order_item_image").find({item_id: item_id}).toArray( function(err, result_image) {
        if (err) throw err;
        db.get().collection("order_item_colors").find({item_id: item_id}).toArray( function(err, result_color) {
        if (err) throw err;
         
       var compostion_array = {image:result_image,colors:result_color,text:result_text};

       compostion_data.push(compostion_array);

       res.status(200).json({ "status": "sucess","render_basic_info":result,"item_info":compostion_data});
          });
         });
        });
       });
      }



   exports.addWorker = (req, res, next) => {
      var server_data = req.body.server_data;
      var worker_data = server_data.worker_data;
      var group_id = server_data.group_id;
      var worker_status,port;
      var server_group_collection = db.get().collection("server_group");
      var render_server_state_collection = db.get().collection("render_server_state");
      if(group_id === 0){
          var server_json = {server_name:server_data.server_name,
              server_status:server_data.server_status,
               group_id:parseInt(server_data.groups_id)};
          server_group_collection.insertOne(server_json, function(err, result) {
          if (err) throw err;
          var group_id = result.insertedId;
          if(worker_data && worker_data.length > 0){
          if (err) throw err;
          for (var i = 0; i < worker_data.length; i++) {
          if(server_data.groups_id === 1 || server_data.groups_id === 2){
          if(worker_data[i]['render_type'] === 'SD'){
          port = "400"+Math.floor(0+ parseInt(server_data.groups_id));
          } else if(worker_data[i]['render_type'] === 'HD'){
          port = "500"+Math.floor(0+ parseInt(server_data.groups_id));
          } else if (worker_data[i]['render_type'] === 'Project Uploader'){
          port = "600"+Math.floor(0+ parseInt(server_data.groups_id));
          } else {
          port ='4000'
          }
          } else {

          if(worker_data[i]['render_type'] === 'AWS'){
          port = "4041";
          } else if(worker_data[i]['render_type'] === 'Image Render'){
          port = "4000";
          } else if(worker_data[i]['render_type'] === 'Project Uploader'){
          port = "6041";
          } else {
          port = "5041";
          }
          }

          if(server_data.server_status  === 0){
          worker_status = "offline"
          } else {
          worker_status = worker_data[i]['status']
          }
          var worker_json = {worker_name:worker_data[i]['render_type'],
                          server_id:group_id.toString(),
                          group_id:parseInt(server_data.groups_id),
                          queue_slots:parseInt(server_data.worker_data[i]['queue_slots']),
                          priority:parseInt(worker_data[i]['priority']),
                          port:parseInt(port),
                          available_slots:parseInt(worker_data[i]['available_slots']),
                          render_type:worker_data[i]['render_type'],
                          status:worker_status};
          render_server_state_collection.insertOne(worker_json, function(err, result) {
          if (err) throw err;
          });  
          }
          }
          res.status(200).json({ "status": "sucess","comment":"server added sucessfully"});
          });



      } else {

        var server_group_collection = db.get().collection("server_group");
        var myquery = {_id: ObjectId(group_id)};
        var newvalues = { $set: {server_name:server_data.server_name,
                                 server_status:server_data.server_status,
                                 group_id:parseInt(server_data.groups_id)}};
        server_group_collection.updateOne(myquery,newvalues, function(err, result) {
        if (err) throw err;
        if(worker_data && worker_data.length > 0){
        var myquery = { server_id:group_id };
        render_server_state_collection.deleteMany(myquery, function(err, result) {
        if (err) throw err;
        for (var i = 0; i < worker_data.length; i++) {
        if(server_data.groups_id === 1 || server_data.groups_id === 2){
        if(worker_data[i]['render_type'] === 'SD'){
         port = "400"+Math.floor(0+ parseInt(server_data.groups_id));
         } else if(worker_data[i]['render_type'] === 'HD'){
         port = "500"+Math.floor(0+ parseInt(server_data.groups_id));
         } else if(worker_data[i]['render_type'] === 'Project Uploader'){
         port = "600"+Math.floor(0+ parseInt(server_data.groups_id));
         } else {
         port='4000';
          }
        
        } else {                  
         if(worker_data[i]['render_type'] === 'AWS') {
          port = "4041";
          } else if(worker_data[i]['render_type'] === 'Image Render') {
          port = "4000";
          } else if(worker_data[i]['render_type'] === 'Project Uploader') {
          port = "6041";
          } else {
          port = "5041";
          }
          }
            
          if(server_data.server_status  === 0){
           worker_status = "offline"
           } else {
           worker_status = worker_data[i]['status']
           }
          var worker_json = {worker_name:worker_data[i]['render_type'],
                              server_id:group_id,
                              group_id:parseInt(server_data.groups_id),
                              queue_slots:parseInt(server_data.worker_data[i]['queue_slots']),
                              priority:parseInt(worker_data[i]['priority']),
                              port:parseInt(port),
                              available_slots:parseInt(worker_data[i]['available_slots']),
                              render_type:worker_data[i]['render_type'],
                              status:worker_status};
           render_server_state_collection.insertOne(worker_json, function(err, result) {
            if (err) throw err;
            });  
            }
           });
          }
          res.status(200).json({ "status": "sucess","comment":"server updated sucessfully"});
         });
         } 
         }

      exports.getWorker = (req, res, next) => {
           var group_id = req.query.group_id;
           var worker_data = [];
           var server_data = [];
           var server_group_collection = db.get().collection("server_group");
           if(group_id === null || group_id === undefined || group_id === ''){
            server_group_collection.find().toArray( function(err, result_group) {
            if (err) throw err;
            if(_.size(result_group) > 0){
            for(var i=0;i < result_group.length;i++){
            if(i == result_group.length -1){
            var group_name;
            if(result_group[i]['group_id'] === 1){
             group_name = "Home"
            } else if(result_group[i]['group_id'] === 2){
             group_name = "OFFICE"
            } else {
             group_name = "AWS"
            }
           var server_json = {"server_id" :result_group[i]['_id'],"server_name":result_group[i]['server_name'],"server_status":result_group[i]['server_status'],"group_id":group_name};
           server_data.push(server_json);
           res.status(200).json({ "status": "sucess","server_data":server_data});
           } else {
           var group_name;
            if(result_group[i]['group_id'] === 1){
               group_name = "Home"
            } else if(result_group[i]['group_id'] === 2){
               group_name = "OFFICE"
            } else {
               group_name = "AWS"
           }

          var server_json = {"server_id" :result_group[i]['_id'],"server_name":result_group[i]['server_name'],"server_status":result_group[i]['server_status'],"group_id":group_name};
          server_data.push(server_json);
                }
                }
               }
              });
           } else {
          server_group_collection.find().toArray( function(err, result_group) {
          if (err) throw err;
          if(_.size(result_group) > 0){
          var server_json = {"server_id" :group_id,"server_name":result_group[0]['server_name'],"server_status":result_group[0]['server_status']};
          server_data.push(server_json);
          res.status(200).json({ "status": "sucess","server_data":server_data});
            }
            });
            }
           }


        exports.getWorkerDetails = (req, res, next) => {
          var group_id = req.query.server_id;
          var server_group_collection = db.get().collection("server_group");
          var render_server_state_collection = db.get().collection("render_server_state");
          server_group_collection.find({'_id':ObjectId(group_id)}).toArray( function(err, result_group) {
            if (err) throw err;
            render_server_state_collection.find({'server_id':group_id}).toArray( function(err, result_worker) {
            if (err) throw err;
          var server_json = {"server_name":result_group[0]['server_name'],"server_status":result_group[0]['server_status'],"groups_id":result_group[0]['group_id'],"worker_data":result_worker};
          res.status(200).json({ "status": "sucess","server_data":server_json});
           });
           });
          }


      exports.checkAvaiblecomputer = (req, res, next) => {
       var group_id = req.body.group_id;
       var server_group_collection = db.get().collection("server_group");
       var render_server_state_collection = db.get().collection("render_server_state");
       server_group_collection.find({'group_id':parseInt(group_id)}).toArray( function(err, result) {
       if (err) throw err;
       var officersIds = [];
           result.forEach(function (officer) {
           officersIds.push(officer.computer_id);
           });
          var generate_number = range(1,6, 1);
          var sumArray = arrayDiff(generate_number,officersIds);  
          var group_json,groupName;
          var group_array = []
            for(var i=0;i<sumArray.length;i++){
             groupName = 'Render ' + sumArray[i];
             group_json = {value:sumArray[i],text:groupName};
             group_array.push(group_json);
             if(i === sumArray.length-1){
              res.status(200).json({ "status": "sucess","computers":group_array});
             }
            }
          });
          }

      exports.checkRenderStatus = (req, res, next) => {
       var product_id = req.query.product_id;
       var comp_collection = db.get().collection("prod_comp");
       var query = {
        "prod_id": ObjectId(product_id)
       };
     
       var sort = [ ["priority", 1] ];
       comp_collection.find(query).sort(sort).toArray( function(err, result) {
       if (err) throw err;
       result.forEach(function (officer) {
        if(officer.preview_video === officer.final_upload_url){
          console.log("File already updated sucessfully")
         } else {
         if(officer.fileUpload === 0 && officer.uploadPercentage === 100 && officer.render_finalized === 0){
          var now = Date.now();
          var diff = Math.floor((now - officer.render_start_time) / 1000);
          if(diff > 18000){
         module.exports.checkProjectDownload(officer)
       }
         } else {
         console.log("File is uploading")
       }
       }
       })
       res.status(200).json({ "status": "sucess","comp_data":result });
       });
       }

       function sizeOf(key, bucket) {
       return s3.headObject({ Key: key, Bucket: bucket })
        .promise()
        .then(res => res.ContentLength);
        }

       exports.checkProjectDownload = (officer) => {
        var final_udate = officer.final_aftereffect;
        var Bucket = "projectsfiless";
        var Key= "uploaddata/"+final_udate.random_scene+"/test/"+final_udate.final_video;
        sizeOf(Key, Bucket).then(size => module.exports.checkSize(size,officer,final_udate));
      }


      
       exports.checkSize =(size,officer,final_udate) => {
          if(size > 20480 && officer.uploadPercentage === 100){
          module.exports.final_update(final_udate.final_video,final_udate.image_frame,final_udate.random_number,final_udate.product_id,final_udate.comp,final_udate.prestnt_time,final_udate.audio_file,final_udate.file_duration,final_udate.server_id,final_udate.random_scene)
         }
        }

     exports.final_update =(product_id,image_frame,random_number,id,comp,prestnt_time,audio_file,file_duration,server_id,random_scene) => {
              var now = Date.now();
              var uploader_queue_collection = db.get().collection("uploader_queue");
              var prod_comp_collection = db.get().collection("prod_comp");
              var render_server_state_collection = db.get().collection("render_server_state");
              var prod_collection = db.get().collection("product");
              var myquery = { product_id:ObjectId(id),compositions:comp};
              var newvalues = { $set: {finalized_status: 1} };
              uploader_queue_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
              if (err) throw err;
              });              

                        
               prod_comp_collection.find({'prod_id':ObjectId(id),'comp_name':comp}).toArray( function(err, item_select_result) {
                if (err) throw err;
              var diff = Math.floor((now - item_select_result[0]['render_start_time']) / 1000);
               diff = Math.floor(diff - 20);
                var myquery = { comp_name:comp,prod_id:ObjectId(id)};
                var newvalues = { $set: {render_end_time:now,render_time:diff,nexrender_status:"render is complete"} };
                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                if (err) throw err;
                });
                });
          var download = function(uri, filename, callback){
                   request.head(uri, function(err, res, body){
                    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
                   });
                };
            var imageFrame = Math.floor(image_frame/30);
            download(keys.upload_url+random_scene+'/test/'+product_id, product_id, function(){
            download(audio_file,"audio_"+random_number+"_.mp3", function(){
              const child = exec('ffmpeg -i '+product_id+' -ss '+imageFrame+' -vframes 1 -q:v 2 render_' + random_number +'.jpeg', (error, stdout, stderr) => {
              if (error) {throw error;}
              fs.readFile('render_' + random_number +'.jpeg', function (err, data) {
                if (err) { throw err; }
                   const s3Render = async () => {
                    await s3.putObject({
                      Body: data,
                      Bucket: "projectsfiless",
                      Key: "uploaddata/"+random_scene+"/render_"+ random_number +"_modified.jpeg",
                      ACL: 'public-read',
                      ContentType: 'binary',
                      CacheControl: 'max-age=172800'
                      }
                    ,(err, data) => {
                     if (err) {console.log(err);
                      } else {
                      fs.unlink('render_' + random_number +'.jpeg', (err) => {
                      if (err) {console.log("failed to delete local image:"+err);}
                       });
                      }
                    });
                   }

                    s3Render().catch(err => {
                        console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });

                  });

                  });

                var endkeyframe = Math.floor(file_duration/30);
                var endTime = endkeyframe -2;
                const mp3_child = exec('ffmpeg -i  '+product_id+' -stream_loop -1 -i audio_'+random_number+'_.mp3 -af "apad,afade=type=out:start_time="'+endTime+'":duration=2" -shortest -map 0:v:0 -map 1:a:0 -y modified_'+product_id, (error, stdout, stderr) => {
                if (error) {throw error;}
                fs.readFile('modified_'+product_id, function (err, data) {
                if (err) { throw err; }
                const s3Render = async () => {
                    await s3.putObject({
                      Body: data,
                      Bucket: "projectsfiless",
                      Key: "uploaddata/"+random_scene+"/modified_"+product_id,
                      ACL: 'public-read',
                      CacheControl: 'public, max-age=50',
                      }
                     ,(err, data) => {
                      if (err) {console.log(err);
                       } else {
                        setTimeout(function() {                        
                        fs.unlink("modified_"+product_id, (err) => {
                         if (err) {
                           console.log("failed to delete local image:"+err);
                          } else {
                            try {
                                  fs.unlinkSync('audio_'+random_number+'_.mp3')
                                  fs.unlinkSync(product_id)
                                  //file removed
                                   } catch(err) {
                                   console.error(err)
                                }
                                var image_url = keys.upload_url+random_scene+ "/render_" + random_number + "_modified.jpeg";
                               var myquery = { comp_name:comp,prod_id:ObjectId(id)};
                                var newvalues = { $set: {preview_video: keys.upload_url+random_scene+'/modified_'+product_id,render_status:3,preview_image: image_url,fileUpload:1} };
                                prod_comp_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                                if (err) throw err;
                                var myquery = { _id:ObjectId(id)};
                                    var newvalues = { $set: {product_preview:image_url}};
                                    prod_collection.updateOne(myquery,newvalues, function(err, result_order_item) {
                                    if (err) throw err;
                                    });
                                });
                                }
                              })
                            ,15000})
                              }
                            });
                           }
                  s3Render().catch(err => {
                       console.log(err);
                        res.status(500).json({
                          error: err
                        });
                      });

                  });
                })  //=== end of mp3


               });
               });
                  
            }


         
         
       exports.checkGroup = (req, res, next) => {
       var server_group_collection = db.get().collection("server_group");
       var options = {
        allowDiskUse: true
       };
      var pipeline = [

          {
              "$group": {
                  "_id": {
                      "group_id": "$group_id"
                  },
                  "COUNT(*)": {
                      "$sum": 1
                  }
              }
          }, 
          {
              "$project": {
                  "group_id": "$_id.group_id",
                  "COUNT(*)": "$COUNT(*)",
                  "_id": 0
              }
          }, 
          {
              "$match": {
                  "COUNT(*)": {
                      "$gt": 1
                  }
              }
          }, 
          {
              "$project": {
                  "_id": 0,
                  "group_id": "$group_id"
              }
          }
      ];


       server_group_collection.aggregate(pipeline, options).toArray( function(err, result) {
        if (err) throw err;
           var officersIds = [];
             result.forEach(function (officer) {
             officersIds.push(officer.group_id);
           });
          var generate_number = range(1,4, 1);
          var sumArray = arrayDiff(generate_number,officersIds);  
          var group_json,groupName;
          var group_array = []
            for(var i=0;i<sumArray.length;i++){
             if(sumArray[i] === 1){
              groupName = 'Home';
             } else if(sumArray[i] === 2){
               groupName = 'Office';
             } else {
              groupName = 'AWS';
             }
             group_json = {value:sumArray[i],text:groupName};
             group_array.push(group_json);
             if(i === sumArray.length-1){
              res.status(200).json({ "status": "sucess","computers":group_array});
             }
            }
            });
          }


            function getDateTime() {
                var now     = new Date(); 
                var year    = now.getFullYear();
                var month   = now.getMonth()+1; 
                var day     = now.getDate();
                var hour    = now.getHours();
                var minute  = now.getMinutes();
                var second  = now.getSeconds(); 
                if(month.toString().length == 1) {
                   month = '0'+month;
                 }
                if(day.toString().length == 1) {
                   day = '0'+day;
                 }   
                if(hour.toString().length == 1) {
                   hour = '0'+hour;
                 }
                if(minute.toString().length == 1) {
                   minute = '0'+minute;
                 }
                if(second.toString().length == 1) {
                   second = '0'+second;
                 }   
                 var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;   
                 return dateTime;
              }


      exports.addProducer = (req, res, next) => {
      var producer_data = req.body.producer_data;
      var producer_id = producer_data._id;
      var producer_collection = db.get().collection("producer");
      if(producer_id === 0){
       var producer_json = {fname:producer_data.fname,lname:producer_data.lname,fullname:producer_data.fullname,email:producer_data.email,paypalemail:producer_data.paypalemail,link:producer_data.link,status:1};
       producer_collection.insertOne(producer_json, function(err, result) {
        if (err) throw err;
         res.status(200).json({ "status": "sucess","comment":"producer added sucessfully"});
        });
       } else {
       var myquery = {_id: ObjectId(producer_id)};
       var newvalues = { $set: {fname:producer_data.fname,lname:producer_data.lname,fullname:producer_data.fullname,email:producer_data.email,paypalemail:producer_data.paypalemail,link:producer_data.link}};
        producer_collection.updateOne(myquery,newvalues, function(err, result) {
        if (err) throw err;
        res.status(200).json({ "status": "sucess","comment":"producer updated sucessfully"});
         });
         } 
         }


       exports.getProducer = (req, res, next) => {
       var producer_collection = db.get().collection("freelancer_user");
       producer_collection.find().toArray( function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data",result:result});
       });
       }

      exports.getProducerdetails = (req, res, next) => {
      var producer_id = req.query.producer_id;
      var producer_collection = db.get().collection("producer");
       producer_collection.find({_id:ObjectId(producer_id)}).toArray( function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data",producer_data:result});
       });
       }


     exports.getProducertrash = (req, res, next) => {
      var producer_id = req.body.producer_id;
      var producer_collection = db.get().collection("producer");
      var query = {_id:ObjectId(producer_id)}
      var update_value = {$set:{status:0}}
      producer_collection.updateOne(query,update_value, function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data deleted"});
       });
       }




     exports.finalProname = (req, res, next) => {
      var product_id = req.body.product_id;
      var price = req.body.price;
      var producer_collection = db.get().collection("product");
      var query = {_id:ObjectId(product_id)}
      var update_value = {$set:{pro_name:price}}
      producer_collection.updateOne(query,update_value, function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data deleted"});
       });
       }

    exports.finalPrice = (req, res, next) => {
      var product_id = req.body.product_id;
      var price = req.body.price;
      var producer_collection = db.get().collection("product");
      var query = {_id:ObjectId(product_id)}
      var update_value = {$set:{pro_price:parseInt(price)}}
      producer_collection.updateOne(query,update_value, function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data deleted"});
       });
       }


   exports.updateLifetime = (req, res, next) => {
      var userId = req.body.userId;
      var lefetime_status = req.body.lefetime_status;
      var order_collection = db.get().collection("order");
      var order_type = 5;
      if(lefetime_status == 0){
         order_type = 9; 
      } else {
        order_type = order_type
      }
      var query = {"user_id":ObjectId(userId),"order_type" : { 
                "$in" : [
                    1, 
                    2, 
                    3, 
                    5,
                    9
                ]
            }}
      order_collection.find(query).toArray( function(err, result) {
      if (err) throw err;
      if(_.size(result)>0){
      var update_value = {$set:{lifetime_status:parseInt(lefetime_status)}}
      order_collection.updateOne(query,update_value, function(err, result) {
       if (err) throw err;
       console.log(result)
       res.status(200).json({ "status": "sucess","comment":""});
       });
      } else {
      var update_value = {user_id:  ObjectId(req.body.userId),
                          sub_id: "",
                          status:1,
                          lifetime_status:parseInt(lefetime_status),
                          order_type:order_type
                                     
                                             }
      order_collection.insertOne(update_value, function(err, result) {
       if (err) throw err;
       res.status(200).json({ "status": "sucess","comment":"producer data deleted"});
       });
      }
      })
      }




       



       


