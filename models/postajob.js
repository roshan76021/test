const { createClient } = require('@nexrender/api');
const fs = require('fs');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);
const exec = require('child_process').exec;
const download = require('download');
const keys = require('../config/keys');
var _ = require('underscore');
var request = require('request');
const AWS = require('aws-sdk');
const nodemailer = require('nodemailer');
const s3= new AWS.S3({
    accessKeyId:keys.AWSaccessKeyId,
    secretAccessKey:keys.AWSsecretAccessKey
});
var db = require('../mdbConnection')
var ObjectId = require('mongodb').ObjectID;
const validateRegisterInput = require('../validation/register');
const validateLoginInput = require('../validation/login');
const isEmpty = require('../validation/is-empty');
const md5 = require('md5');
const jwt = require('jsonwebtoken');

exports.getcategory = (req, res, next) => {

  var category_sql = "Select * from freelancer_category where status = '1'"
	
  db.query(category_sql, function (err, result) {
    if (err) throw err;
    
    res.status(200).json({ "status": "sucess","category":result});
   });
 };

  exports.getsubcategory = (req, res, next) => {

    var sub_cat_id = req.query.cat_id

  var category_sql = "Select * from freelancer_subcategory where cat_id = '"+sub_cat_id+"'"
  
  db.query(category_sql, function (err, result) {
    if (err) throw err;
    
    res.status(200).json({ "status": "sucess","sub_category":result});
   });
 };


  exports.getcountry = (req, res, next) => {

  var category_sql = "SELECT country FROM `freelancer_product`"
  
  db.query(category_sql, function (err, result) {
    if (err) throw err;

    var filtered = function (array) {
        var o = {};
        return array.filter(function (a) {
            var k = a.x + '|' + a.y;
            if (!o[k]) {
                o[k] = true;
                return true;
            }
        });
    }(result);
    
    
    res.status(200).json({ "status": "sucess","country":filtered});
   });
 };



  exports.getlanguage = (req, res, next) => {

  var category_sql = "SELECT language FROM `freelancer_product`"
  
  db.query(category_sql, function (err, result) {
    if (err) throw err;

    var filtered = function (array) {
        var o = {};
        return array.filter(function (a) {
            var k = a.x + '|' + a.y;
            if (!o[k]) {
                o[k] = true;
                return true;
            }
        });
    }(result);
    
    
    res.status(200).json({ "status": "sucess","language":filtered});
   });
 }


  function getUnique(array){
        var uniqueArray = [];
        
        // Loop through array values
        for(var value of array){
            if(uniqueArray.indexOf(value) === -1){
                uniqueArray.push(value);
            }
        }
        return uniqueArray;
    }


  exports.signup = (req, res, next) => {
   var signindata = req.body;
   var ip = JSON.stringify(req.ip)
   var now = new Date();
  db.get().collection("freelancer_user").find({email:signindata.email}).toArray( function(err, result_item_email) {
     if (err) throw err;
      if(_.size(result_item_email)< 1)
     {
   var signin = {fname:signindata.fname,
                 lname:signindata.lname,
                 profileName:signindata.profileName,
                 email:signindata.email,
                 website_url:signindata.websiteUrl,
                 ip:ip,
                 password:md5(signindata.password),
                 signup_date:now,
                 composition:signindata.composition,
                 status:1}
    db.get().collection("freelancer_user").insertOne(signin, function(err, result_order_tracking_insert) {
    if (err) throw err;
     var userID = result_order_tracking_insert.insertedId;
      if(_.size(signindata.website_url)>0){
        var url = signindata.website_url;
          for (var i =0; i < url.length; i++){
           var product_name = url[i]['timestamp'];
           var tag_data = {url:product_name,user_id:ObjectId(userID)}
                  db.get().collection("freelancer_website_url").insertOne(tag_data, function(err, result_cat) {
                  if (err) throw err;
                  });
                 }
               }

               let message = '';
               message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left">Thank you for being a contributor. To update your account, please <a href="https://www.onemaker.io/contributor" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.onemaker.io/contributor&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">login here.</a></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"> Hi '+signindata.profileName+', <br><br>Welcome to the Onemaker.io author Portal. <br><br>Your application to become an author has been approved.: <br><br>Please join our authors discord channel <br><a href="https://discord.gg/tfQBNZ7gC2">https://discord.gg/tfQBNZ7gC2</a> <br><br>We look forward to having you on our site!<br><br>Regards, <br>Onemake.io Team </div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>'

                   let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });

                      var mailOptions = {
                      to:'vivekrana46@gmail.com',
                      from: 'no-reply@onemaker.io',
                      subject: 'New contributor user registred',
                      text: 'New user is registred,Email address is :'+signindata.email
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

                      var mailOptions = {
                      to:signindata.email,
                     from: 'no-reply@onemaker.io',
                      subject: 'Welcome to the Onemaker.io author Portal',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });


      res.status(200).json({ "status": "sucess"});
    });
  } else {
    res.status(200).json({ "status": "user is not registered due to email address"});
  }
})
   },


 exports.checkUsername = (req, res, next) => {
   var username = req.query.username;
   db.get().collection("freelancer_user").find({profileName:new RegExp(username, "i")}).toArray( function(err, result_item_email) {
     if (err) throw err;
     if(_.size(result_item_email)>0){
       res.status(201).json({ "status": "Username is already exists","user_status":"1"});
     } else {
       res.status(200).json({ "status": "Username available for create user","user_status":"2"});
     }
   });
  }

exports.checkEmail = (req, res, next) => {
   var email = req.query.email;
  if (/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))
  {
    db.get().collection("freelancer_user").find({email: new RegExp(email, "i")}).toArray( function(err, result_item_email) {
     if (err) throw err;
     if(_.size(result_item_email)>0)
     {
       res.status(201).json({ "status": "Email is already exists","email_status":"1"});
     } else {
       res.status(200).json({ "status": "Email available for create user","email_status":"2"});

     }
   })
   }else {
     res.status(200).json({ "status": "Email is not valid","email_status":"3"});
   }
 }

 exports.getLeadSource = (req, res, next) => {
  db.get().collection("leadsource").find({}).toArray( function(err, result_item_email) {
     if (err) throw err;
     if(_.size(result_item_email)>0)
     {
       res.status(201).json({ "status": "leadSource","result":result_item_email});
     } else {
       res.status(200).json({ "status": "lead source is not found","result":result_item_email});

     }
   })
 }


 exports.login = (req, res, next) => {
   var signindata = req.body;
    db.get().collection("freelancer_user").find({email:signindata.email}).toArray( function(err, result) {
     if (err) throw err;
       if(_.size(result) > 0){
        if(result[0]['password'] === md5(signindata.password)){
         const user_token = jwt.sign(
                                  {
                                user_id:result[0]._id,
                                email:result[0].email,
                                website_url:result[0].websiteUrl,
                                profilename:result[0].profileName,
                                fname:result[0].fname,
                                lname:result[0].lname,
                                    },
                                     keys.JWT_KEY,{algorithm:'HS512'},
                                    {
                                     expiresIn: 3600
                                    }
                                  );
                           
            res.status(200).json({ "status": "Success","login_status":"1","usedata":user_token});
           } else {
            res.status(201).json({ "status": "Password is not correct","login_status":"2"});
           }
      } else {
        db.get().collection("freelancer_user").find({profileName:signindata.email}).toArray( function(err, result) {
        if (err) throw err;
         if(_.size(result) > 0){
                  if(result[0]['password'] === md5(signindata.password)){

                   const user_token = jwt.sign(
                                  {
                                user_id:result[0]._id,
                                email:result[0].email,
                                // website_url:result[0].websiteUrl,
                                profilename:result[0].profileName,
                                fname:result[0].fname,
                                lname:result[0].lname,
                                    },
                                     keys.JWT_KEY,{algorithm:'HS512'},
                                    {
                                     expiresIn: 3600
                                    }
                                  );
                           
             res.status(200).json({ "status": "Success","login_status":"1","usedata":user_token});
           } else {
             res.status(201).json({ "status": "Password is not correct","login_status":"2"});
           }
          } else {
           res.status(201).json({ "status": "Email/username is not exists","login_status":"3"});
        }
        });
       }
      });
    };


  exports.getpaymentInfo = (req, res, next) => {
     var paymentdata = req.body;
    db.get().collection("freelancer_payment_info").find({user_id:ObjectId(paymentdata.user_id)}).toArray( function(err, result) {
    if (err) throw err;
     if(_.size(result)>0){
   res.status(201).json({ "status":"success","paypal_email":result[0].paypal_email});
   } else {
     res.status(201).json({ "status":"success","paypal_email":""});
   }
   });
  }

   exports.addpaymentInfo = (req, res, next) => {
      var paymentdata = req.body;
       db.get().collection("freelancer_payment_info").find({user_id:ObjectId(paymentdata.user_id)}).toArray( function(err, result) {
       if (err) throw err;
          if(_.size(result)>0){
            var payment_data = {$set:{paypal_email:paymentdata.paypal_email,status:1}}
            var payment_query = {user_id:ObjectId(paymentdata.user_id)}
              db.get().collection("freelancer_payment_info").updateOne(payment_query,payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
              res.status(201).json({ "status": "payment updated","paypal_email":paymentdata.paypal_email,"payment_status":"1"});
             })

          } else {
            var payment_data = {paypal_email:paymentdata.paypal_email,status:1,user_id:ObjectId(paymentdata.user_id)}
            db.get().collection("freelancer_payment_info").insertOne(payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
             res.status(201).json({ "status": "payment insert","paypal_email":paymentdata.paypal_email,"payment_status":"2"});

             })
           }
        })
     }


 exports.changepassword = (req, res, next) => {
    var signindata = req.body;
    db.get().collection("freelancer_user").find({_id:ObjectId(signindata.user_id)}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
        var payment_data = {$set:{password:md5(signindata.password)}}
        var payment_query = {_id:ObjectId(signindata.user_id)}
              db.get().collection("freelancer_user").updateOne(payment_query,payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
             res.status(201).json({ "status":"success","password_status":"1"});
           })
                  } else {
            res.status(201).json({ "status":"success","password_status":"2"});
   }
   });
  }

 exports.resetPassword = (req, res, next) => {
    var signindata = req.body;
    db.get().collection("freelancer_user").find({email:signindata.email}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){

        var userDetailsJoson = jwt.sign(
                                  {
                                user_id:result[0]._id,
                                email:result[0].email,
                                profilename:result[0].profileName,
                                fname:result[0].fname,
                                lname:result[0].lname,
                                    },
                                 keys.JWT_KEY,{algorithm:'HS512'},
                                 {
                                 expiresIn: 3600
                                 }
                                );
                  var url = "https//www.onemaker.io/contributor/pages/reset-password/"+userDetailsJoson

                  var message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left"></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"> Hi '+result[0].profileName+',<br><br>We heard you need a password reset. Click the link below and you will be redirected to a secure site from which you can set a new password. <br><br>Please click on the Link for reset password,<a href="'+url+'" target="_blank" data-saferedirecturl="https://www.google.com/url?q='+url+'&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">Click here.</a>.</div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>'

                   let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });

                 var mailOptions = {
                      to:result[0].email,
                      from: 'no-reply@onemaker.io',
                      subject: 'Reset Password of Onemaker',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

             res.status(201).json({ "status":"success","password_status":"1"});
        
          } else {
            res.status(201).json({ "status":"success","password_status":"2"});
         }
         });
         }




  exports.getuserInfo = (req, res, next) => {
    var signindata = req.body;
    db.get().collection("freelancer_user").find({_id:ObjectId(signindata.user_id)}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
        var payment_data = {fname:result[0]['fname'],
                            lname:result[0]['lname'],
                            phone:result[0]['phone']=== undefined ? '' : result[0]['phone'],
                            address:result[0]['address']=== undefined ? '' : result[0]['address'],
                            company:result[0]['company']=== undefined ? '' : result[0]['company'],
                           }
         res.status(201).json({ "status":"success","userInfo":payment_data});
         } else {
         res.status(201).json({ "status":"success","userInfo":{}});
   }
   });
  }


   exports.adduserInfo = (req, res, next) => {
      var signindata = req.body;
       db.get().collection("freelancer_user").find({_id:ObjectId(signindata.user_id)}).toArray( function(err, result) {
       if (err) throw err;
          if(_.size(result)>0){
            var payment_data = {$set:{fname:signindata.fname,lname:signindata.lname,phone:signindata.phone,company:signindata.company,address:signindata.address}}
            var payment_query = {_id:ObjectId(signindata.user_id)}
              db.get().collection("freelancer_user").updateOne(payment_query,payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;


              res.status(201).json({ "status": "personal Info updated","info_status":"1"});
             })

          } else {
            var payment_data = {fname:signindata.fname,lname:signindata.lname,phone:signindata.phone,company:signindata.company,address:signindata.address}
            db.get().collection("freelancer_user").insertOne(payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
             res.status(201).json({ "status": "personal Info updated","info_status":"2"});

             })
           }
        })
     }


exports.getbio = (req, res, next) => {
    var biodata = req.body;
    db.get().collection("freelancer_user").find({_id:ObjectId(biodata.user_id)}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
        var bio_data = {biodata:result[0]['biodata']=== undefined ? '' : result[0]['biodata'],
                            t_url:result[0]['t_url']=== undefined ? '' : result[0]['t_url'],
                            f_url:result[0]['f_url']=== undefined ? '' : result[0]['f_url'],
                            l_url:result[0]['l_url']=== undefined ? '' : result[0]['l_url'],
                            i_url:result[0]['i_url']=== undefined ? '' : result[0]['i_url'],
                           }
         res.status(201).json({ "status":"success","getbioInfo":bio_data});
         } else {
         res.status(201).json({ "status":"success","getbioInfo":{}});
   }
   });
  }


   exports.addbio = (req, res, next) => {
      var biodata = req.body;
       db.get().collection("freelancer_user").find({_id:ObjectId(biodata.user_id)}).toArray( function(err, result) {
       if (err) throw err;
          if(_.size(result)>0){
            var bio_data = {$set:{biodata:biodata.biodata,
                                  t_url:biodata.t_url,
                                  f_url:biodata.f_url,
                                  l_url:biodata.l_url,
                                  i_url:biodata.i_url}}
            var bio_query = {_id:ObjectId(biodata.user_id)}
              db.get().collection("freelancer_user").updateOne(bio_query,bio_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
              res.status(201).json({ "status": "Bio Info updated","bio_status":"1"});
             })

          } else {
            var payment_data = {  biodata:biodata.biodata,
                                  t_url:biodata.t_url,
                                  f_url:biodata.f_url,
                                  l_url:biodata.l_url,
                                  i_url:biodata.i_url}
            db.get().collection("freelancer_user").insertOne(payment_data, function(err, result_order_tracking_insert) {
             if (err) throw err;
             res.status(201).json({ "status": "Bio Info updated","bio_status":"2"});

             })
           }
        })
     }


 

    exports.addfreelancerProject = (req, res, next) => {
      var freelancerdata = req.body;
     var freelancer_data = { name:freelancerdata.title,
                                  desc:freelancerdata.desc,
                                  category:freelancerdata.category,
                                  subcategory:freelancerdata.subcategoryName,
                                  keywords:freelancerdata.keywords,
                                  file_url:freelancerdata.file_url,
                                  user_id:freelancerdata.user_id,
                                  status:0,
                                  file_created:freelancerdata.now
                                }
            db.get().collection("freelancer_projects").insertOne(freelancer_data, function(err, result_order_tracking_insert) {
             if (err) throw err;

             let message = '';
             message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left">Thank you for being a contributor. To update your account, please <a href="https://www.onemaker.io/contributor" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.onemaker.io/contributor&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">login here.</a></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"> <br><br>Hey '+freelancerdata.userName+', your template '+freelancerdata.title+' has been uploaded for approval! <br><br>If you have any questions please talk to your reviewer on our Discord <br><a href="https://discord.gg/tfQBNZ7gC2">https://discord.gg/tfQBNZ7gC2</a> <br><br>Regards, <br>Onemake.io Team </div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>';
                   
                     let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });

                      var mailOptions = {
                      to:'vivekrana46@gmail.com',
                      from: 'no-reply@onemaker.io',
                      subject: 'New Freelancer project submitted',
                      html: '<html><body><New Freelancer project submitted,user id is :'+freelancerdata.user_id+' and project url is : <a href="'+freelancerdata.file_url+'">preview</a></body></html>'
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

                      var mailOptions = {
                      to:freelancerdata.userEmail,
                     from: 'no-reply@onemaker.io',
                      subject: 'Congratulation! '+freelancerdata.title+' has been uploaded',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

             res.status(201).json({ "status": "Bio Info updated","bio_status":"2"});

             })
    
     }

      exports.getProectsstatus = (req, res, next) => {
      var status_data = req.body;
      var user_id = status_data.user_id;
      var sort = [ ["file_created", -1] ];

      db.get().collection("freelancer_projects").find({user_id:user_id}).sort(sort).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
      var buffer_pending_status = result.filter(function(person) {return person.status === 0;});
          buffer_pending_status = buffer_pending_status === undefined || buffer_pending_status === '' ? [] : buffer_pending_status
      var pending_status = buffer_pending_status === undefined || buffer_pending_status === '' ? 0 : buffer_pending_status.length;


      var buffer_approved_status = result.filter(function(person) {return person.status === 1;});
         buffer_approved_status = buffer_approved_status === undefined || buffer_approved_status === '' ? [] : buffer_approved_status
      var approved_status = buffer_approved_status === undefined || buffer_approved_status === '' ? 0 : buffer_approved_status.length;



      var buffer_rejected_status = result.filter(function(person) {return person.status === 4;});
      buffer_rejected_status = buffer_rejected_status === undefined || buffer_rejected_status === '' ? [] : buffer_rejected_status
      var rejected_status = buffer_rejected_status === undefined || buffer_rejected_status === '' ? 0 : buffer_rejected_status.length;


      var buffer_release_status = result.filter(function(person) {return person.status === 2;});
      buffer_release_status = buffer_release_status === undefined || buffer_release_status === '' ? [] : buffer_release_status
      var release_status = buffer_release_status === undefined || buffer_release_status === '' ? 0 : buffer_release_status.length;

     res.status(201).json({ "status": "data not found","pending_status":pending_status,"approved_status":approved_status,"rejected_status":rejected_status,"release_status":release_status,"pending_data":buffer_pending_status,"rejected_data":buffer_rejected_status,"approved_data":buffer_approved_status,"release_data":buffer_release_status});
    
    
    
     } else {

      res.status(201).json({ "status": "data not found","pending_status":"0","approved_status":"0","rejected_status":"0","release_status":"0","pending_data":[],"approved_data":[],"release_data":[],"rejected_data":[]});
      }
      })
     }
    

     exports.updateproectsStatus = (req, res, next) => {
      var status_data = req.body;
      var update_status_data = {$set:{status:status_data.datastatus,comment:status_data.comment}};
      var update_status_query = {_id:ObjectId(status_data.project_id)};
       db.get().collection("freelancer_projects").updateOne(update_status_query,update_status_data, function(err, result_order_tracking_insert) {
       if (err) throw err;
       var message = '';
       var comment = status_data.comment;
       comment = comment.replace("\n", "<pre>");
       if(status_data.datastatus === 2){
          message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left">Thank you for being a contributor. To update your account, please <a href="https://www.onemaker.io/contributor" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.onemaker.io/contributor&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">login here.</a></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"><br><br>Hey '+status_data.freelancer_name+', your template '+status_data.project_name+' has been approved for Sale. <br><br>Keep Your sale <a href="https://www.onemaker.io/contributor/pages/payout">here</a> <br> <br><br>If you have any questions please talk to your reviewer on our Discord Server. <br><a href="https://discord.gg/tfQBNZ7gC2">https://discord.gg/tfQBNZ7gC2</a> <br><br>Regards, <br>Onemake.io Team </div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>'
        } else if(status_data.datastatus === 4){
        message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left">Thank you for being a contributor. To update your account, please <a href="https://www.onemaker.io/contributor" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.onemaker.io/contributor&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">login here.</a></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"><br><p>Your template needs a bit more work. <p>The list of things you need to fix before approval: </p><p> '+comment+'</p><p>Once you are done with these changes please submit again</p> <p>If you have any questions please talk to your reviewer on our Discord Server. <br><a href="https://discord.gg/tfQBNZ7gC2">https://discord.gg/tfQBNZ7gC2</a> <br><br>Regards, <br>Onemake.io Team </div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>'
        } else {
            message = '<html><body><div marginwidth="0" marginheight="0" style="margin:0;padding:0;background-color:#fafafa;width:100%!important"><center> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="m_3667155798134930410m_-5006997813821785982backgroundTable" style="margin:0;padding:0;background-color:#fafafa;height:100%!important;width:100%!important"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templatePreheader" style="background-color:#fafafa"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="10" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:10px;line-height:100%;text-align:left">Thank you for being a contributor. To update your account, please <a href="https://www.onemaker.io/contributor" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.onemaker.io/contributor&amp;source=gmail&amp;ust=1605604069338000&amp;usg=AFQjCNEhD-bVBAxduJeT0NXiG0hHu7JO-A">login here.</a></div></td></tr></tbody></table> </td></tr></tbody></table> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateContainer" style="border:1px solid #dddddd;background-color:#ffffff"> <tbody><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateHeader" style="background-color:#ffffff;border-bottom:0"> <tbody><tr> <td style="border-collapse:collapse;color:#202020;font-family:Arial;font-size:34px;font-weight:bold;line-height:100%;text-align:center;vertical-align:middle"> <br><br><img src="https://d7l7evf8207pq.cloudfront.net/_assets/logo/logo_contribute.png" width="300" alt="Onemaker.io" border="0" class="CToWUd"> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <table border="0" cellpadding="0" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateBody"> <tbody><tr> <td valign="top" style="border-collapse:collapse;background-color:#ffffff"> <table border="0" cellpadding="30" cellspacing="0" width="100%"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <div style="color:#505050;font-family:Arial;font-size:14px;line-height:150%;text-align:left"><br><br>Hey '+status_data.freelancer_name+', your template '+status_data.project_name+' has been approved for Sale. <br><br>Keep Your sale <a href="https://www.onemaker.io/contributor/pages/payout">here</a> <br> <br><br>If you have any questions please talk to your reviewer on our Discord Server. <br><a href="https://discord.gg/tfQBNZ7gC2">https://discord.gg/tfQBNZ7gC2</a> <br><br>Regards, <br>Onemake.io Team </div></td></tr></tbody></table> </td></tr></tbody></table> </td></tr><tr> <td align="center" valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><table border="0" cellpadding="10" cellspacing="0" width="600" id="m_3667155798134930410m_-5006997813821785982templateFooter" style="background-color:#ffffff;border-top:0"> <tbody><tr> <td valign="top" style="border-collapse:collapse"> <font color="#888888"> </font><font color="#888888"> </font><font color="#888888"> <br></font></td></tr></tbody></table><font color="#888888"></font></center><font color="#888888"></font></div></body></html>'
       }

              let smtpTransport = nodemailer.createTransport({
                    host: "smtp.zoho.in",
                    port: 465,
                    secure: true, // true for 465, false for other ports
                    auth: {
                    user: 'no-reply@onemaker.io', // generated ethereal user
                    pass: 'iYoginet@1234' // generated ethereal password
                    },
                    });

                      var mailOptions = {
                      to:'srivastava.kushagra7@gmail.com',
                      from: 'no-reply@onemaker.io',
                      subject: 'Freelance project status changed',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });

console.log(status_data.email)
                     if(status_data.datastatus === 4){

                      var mailOptions = {
                      to:status_data.email,
                     from: 'no-reply@onemaker.io',
                      subject: 'Hey '+status_data.freelancer_name+', Unfortunately your template '+status_data.project_name+' has been rejected.',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });
                     } else {


                      var mailOptions = {
                      to:status_data.email,
                     from: 'no-reply@onemaker.io',
                      subject: 'Hey '+status_data.freelancer_name+', your template '+status_data.project_name+' has been Approved.',
                      html: message
                       };
                     smtpTransport.sendMail(mailOptions, function(err) {
                     });
                       
                     }
       



       res.status(201).json({ "status": "Project status updated","status_status":"1"});
       })
      }


  exports.getProjectsstatus = (req, res, next) => {
       var data = [];
      db.get().collection("freelancer_projects").find({}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
     result.forEach(function(elementss,j) {
     var freelancer_id = elementss.user_id;
     db.get().collection("freelancer_user").find({_id:ObjectId(freelancer_id)}).toArray( function(err, result_item_email) {
     if (err) throw err;
      if(_.size(result_item_email)>0)
     {
      var freelancer_data = {desc:elementss.desc,file_created:elementss.file_created,_id:elementss._id,file_url: elementss.file_url,keywords: elementss.keywords,name: elementss.name,status: elementss.status,category:elementss.category,freelancer_name:result_item_email[0]['profileName'],email:result_item_email[0]['email']};
    data.push(freelancer_data)
   
     }
    
     if(data.length === result.length){
       res.status(201).json({ "status": "data  found","data":data});
     }
   })
   })

    } else {

      res.status(201).json({ "status": "data not found","data":[]});
      }
      })
     }
    
    exports.getFreelanceruser = (req, res, next) => {
       var data = [];
       var user_id = req.query.user_id;
       if(user_id === "" || user_id === undefined || user_id === null){
      db.get().collection("freelancer_user").find({}).toArray( function(err, result) {
       if (err) throw err;
      res.status(201).json({ "status": "data  found","data":result});
     })
    } else {
     db.get().collection("freelancer_user").find({_id:ObjectId(user_id)}).toArray( function(err, result_user) {
     if (err) throw err;
         var sort = [ ["file_created", -1] ];

     db.get().collection("freelancer_projects").find({user_id:user_id}).sort(sort).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
      var buffer_pending_status = result.filter(function(person) {return person.status === 0;});
          buffer_pending_status = buffer_pending_status === undefined || buffer_pending_status === '' ? [] : buffer_pending_status
      var pending_status = buffer_pending_status === undefined || buffer_pending_status === '' ? 0 : buffer_pending_status.length;
      var buffer_approved_status = result.filter(function(person) {return person.status === 1;});
      buffer_approved_status = buffer_approved_status === undefined || buffer_approved_status === '' ? [] : buffer_approved_status
      var approved_status = buffer_approved_status === undefined || buffer_approved_status === '' ? 0 : buffer_approved_status.length;
      var buffer_rejected_status = result.filter(function(person) {return person.status === 4;});
      buffer_rejected_status = buffer_rejected_status === undefined || buffer_rejected_status === '' ? [] : buffer_rejected_status
      var rejected_status = buffer_rejected_status === undefined || buffer_rejected_status === '' ? 0 : buffer_rejected_status.length;
      var buffer_release_status = result.filter(function(person) {return person.status === 2;});
      buffer_release_status = buffer_release_status === undefined || buffer_release_status === '' ? [] : buffer_release_status
      var release_status = buffer_release_status === undefined || buffer_release_status === '' ? 0 : buffer_release_status.length;
       db.get().collection("freelancer_website_url").find({user_id:ObjectId(user_id)}).sort(sort).toArray( function(err, result_website_url) {
       if (err) throw err;


      res.status(201).json({ "status": "data not found","pending_status":pending_status,"approved_status":approved_status,"rejected_status":rejected_status,"release_status":release_status,"pending_data":buffer_pending_status,"rejected_data":buffer_rejected_status,"approved_data":buffer_approved_status,"release_data":buffer_release_status,"data":result_user,"website_url":result_website_url});
      })
      } else {
            db.get().collection("freelancer_website_url").find({user_id:ObjectId(user_id)}).sort(sort).toArray( function(err, result_website_url) {
       if (err) throw err;
      res.status(201).json({ "status": "data not found","pending_status":0,"approved_status":0,"rejected_status":0,"release_status":0,"pending_data":[],"approved_data":[],"release_data":[],"data":result_user,"website_url":result_website_url});
      })
    
      }
      })
      })
      }
     }
    


  exports.getFreelancerpstatus = (req, res, next) => {
       var data = [];
       var project_id = req.query.project_id;
        
      db.get().collection("freelancer_projects").find({_id:ObjectId(project_id)}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
     result.forEach(function(elementss,j) {
     var freelancer_id = elementss.user_id;
     var sub_category;
     if(elementss.subcategory === null || elementss.subcategory === undefined || elementss.subcategory === "")
     {
       sub_category = ""
     
     } else {
         sub_category =  _.pluck(elementss.subcategory, "cat_name"); 
     }

     db.get().collection("freelancer_user").find({_id:ObjectId(freelancer_id)}).toArray( function(err, result_item_email) {
     if (err) throw err;
      if(_.size(result_item_email)>0)
     {
    
      var freelancer_data = {desc:elementss.desc,file_created:elementss.file_created,_id:elementss._id,file_url: elementss.file_url,keywords: elementss.keywords,name: elementss.name,status: elementss.status,category:elementss.category,subcategory:sub_category,freelancer_name:result_item_email[0]['profileName']};
    data.push(freelancer_data)
   
     }
    
     if(data.length === result.length){
       res.status(201).json({ "status": "data  found","data":data});
     }
   })
   })

    } else {

      res.status(201).json({ "status": "data not found","data":[]});
      }
      })
     }
    


  exports.getfreeLancerProject = (req, res, next) => {
     var data = [];
     var user_id = req.body.freelancer_id;
     db.get().collection("freelancer_projects").find({user_id:user_id,status:1}).toArray( function(err, result) {
     if (err) throw err;
     if(_.size(result)>0){
     res.status(201).json({ "status": "data  found","data":result});
      } else {
     res.status(201).json({ "status": "data not found","data":[]});
      }
      })
      }
    



















