var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors=require('cors');
var Product = require('./routes/product');
var Postajob = require('./routes/postajob');
var mongoose = require('mongoose');
var Render = require('./routes/render');
const jwtKey = process.env['JWT_KEY'] || 'shared-secret'
var db = require('./mdbConnection');



var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(cors({
    origin: ['http://150.129.182.25:8080','http://localhost:3000','http://150.129.182.25:3000','https://onemaker.io','https://www.onemaker.io','http://localhost:8080','https://staging.onemaker.io'],
    credentials: true
  
}));

var allowedOrigins = ['http://localhost:3000','http://150.129.182.25:3000','http://150.129.182.25:8080','https://onemaker.io','https://www.onemaker.io','http://localhost:8080','https://staging.onemaker.io'];
app.options('*', cors({ credentials: true }))

app.all('', function(req, res, next) {
  var origin = req.headers.origin;
  
  if(allowedOrigins.indexOf(origin) > -1){
       res.setHeader('Access-Control-Allow-Origin', origin);
         }


app.all('', function(req, res, next) {
    res.header("Access-Control-Allow-Origin","*");

    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers','Origin' ,'Access-Control-Allow-Headers', 'Origin,Accept', 'X-Requested-With', 'Content-Type', 'Access-Control-Request-Method', 'Access-Control-Request-Headers','X-Access-Token','XKey','Authorization');
    res.Header('Allow', "*")
    res.Header('Connection', "keep-alive")
    res.Header("Date", Date())
    res.Header("Content-Type", "application/json; charset=utf-8","multipart/form-data")
    if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();

});
});

db.connect('mongodb+srv://admin:iYoginet$123@cluster0.brr5f.mongodb.net/live_clone?retryWrites=true&w=majority', function(err) {

  if (err) {
    console.log('Unable to connect to Mongo.')
    process.exit(1)
  } else {
    console.log("connect with mongodb");
  }
})

app.use(express.static('/uploads/'));

app.use("/render", Render);
app.use("/product", Product);
app.use("/postajob", Postajob);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
