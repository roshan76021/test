const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(order_data) {
  let errors = {};

     //  order_data.user_id = !isEmpty(order_data.user_id) ? order_data.user_id : '';
       order_data.sub_id = !isEmpty(order_data.sub_id) ? order_data.sub_id : '';
       order_data.agent_mode = !isEmpty(order_data.agent_mode) ? order_data.agent_mode : '';
       // order_data.transcation_type = !isEmpty(order_data.transcation_type) ? order_data.transcation_type : '';
       // order_data.ip_address = !isEmpty(order_data.ip_address) ? order_data.ip_address : '';

  // if (Validator.isEmpty(order_data.user_id)) {
  //   errors.user_id = 'User Id is Required for generating order';
  // }

  if (Validator.isEmpty(order_data.sub_id )) {
    errors.sub_id = 'Subscription Id is required for genrate order';
  }

  if (Validator.isEmpty(order_data.agent_mode)) {
    errors.agent_mode = 'Agent Mode is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
