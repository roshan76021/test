'use strict';


var passport = require('passport');
var User = require('./models/users.js');
var FacebookTokenStrategy = require('passport-facebook-token');
var GoogleTokenStrategy = require('passport-google-token').Strategy;
var config = require('./common/config');
const keys = require('./config/keys')

module.exports = function () {


    passport.use(new FacebookTokenStrategy({
            clientID: keys.facebookAuthClientId,
            clientSecret: keys.facebookAuthClientSecret,
            passReqToCallback: true
        },
        function (req,accessToken, refreshToken, profile, done, res) {

           
            User.upsertFbUser(req,accessToken, refreshToken, profile, function(err, user) {

                return done(err, user);
            });
        }));


    passport.use(new GoogleTokenStrategy({
            clientID: keys.googleAuthClientId,
            clientSecret: keys.googleAuthClientSecret,
            passReqToCallback: true
        },
        function (req, accessToken, refreshToken, profile, done) {
          
                User.upsertGoogleUser(req, accessToken, refreshToken, profile, function(err, user) {
                return done(err, user);
            });
        }));
};
