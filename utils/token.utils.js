const jwt = require('jsonwebtoken');
const isEmpty = require('../validation/is-empty');


var createToken = function(auth,user,url) {
  // console.log(user);
  // console.log(url);
  if(!isEmpty(user.facebookProvider)&&url==='/auth/facebook')
  return jwt.sign(
    {
      email: user.email,
      profilePic: user.profilePic,
      userId: auth.id,
      userName: user.facebookProvider.username,
      inviteEmails: user.inviteEmails
    },
    process.env.JWT_KEY,
    {
      expiresIn: 3600
    }
  );
  else if(!isEmpty(user.googleProvider&&url==='/auth/google')) {
    return jwt.sign(
      {
        userName: user.googleProvider.username,
        profilePic: user.profilePic,
        email: user.email,
        userId: auth.id,
        inviteEmails: user.inviteEmails
      },
      process.env.JWT_KEY,
      {
        expiresIn: 3600
      }
    );
  }
  else {
    {
      return jwt.sign(
        {
          userId: auth.id
        },
        'my-secret',
        {
          expiresIn: 3600
        }
      );
    }
  }
};

module.exports = {
  generateToken: function(req, res, next) {
    console.log(req.user);
    req.token = createToken(req.auth,req.user,req.url);
    return next();
  },
  sendToken: function(req, res) {
    res.setHeader('x-auth-token', req.token);
    return res.status(200).send(JSON.stringify(req.user));
  }
};
